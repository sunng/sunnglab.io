Title: What to expect for Jiangsu Suning's summer break
Date: 2018-06-13 17:45:33
Tags: football

This article is written for [Jiangsu Suning fan blog](https://www.jiangsuguoxinsainty.com/). This is the first time I write about football, in both English and Chinese. After confirmed from the site owner, I can repost it on my own blog.

The 2018 World Cup is coming in next week. Chinese Super League will be paused until early July. Yesterday, Suning team was heading off to Italy for a special summer training camp. They will be using their sister club, Inter's Suning Training Center during this summer.
 
11 weeks into 2018, Suning gained 19 points and 18 goals, ranked 4th with only 4 points away from top, which is a huge improvement if you compare it to 2017. But fans always expect more. With promising young star Huang Zichang, an extraordinary Ji Xiang and super reliable Teixeira, can we expect a return to Asia Champions League, or even a title for 2018? The summer break can really decide.


## Olaroiu's secret new formation

After taking over the team from Capello, Olaroiu soon switched formation from 4-3-3 to 4-4-2. Using Wu Xi and Tian Yinong holding the midfield, and recovered Boakye & Teixeira on the front, Olaroiu managed to get 16 points in 8 matches. 

However, the Romanian still received questions about his never-changed lineup and substitutions: 89' for Zhang Lingfeng and 91' for Gao Tianyi. These two U23s gave impressive performance during Capello era, but now were only for satisfying CFA's U23 policy, with no tactical meaning.

In an interview before victory over Dalian Yifang, Olaroiu told CCTV-5 journalist Wang Tao, picking up the team in mid-season, he had little time to experiment any new formation and it's what he will definitely do during the summer break. 

The current 4-4-2 relies heavily on our starting XI. But we don't have many good substitutions for the system. Meanwhile, talent players like Wang Song, Gao Tianyi, Abduhamit were wasting their time on the bench. So I really wish Olaroiu can find his new formation in this summer, and utilize our potential for a more flexible squad. For example, if we can start 2 U23 players, we will have two meaning substitutions.
 
## Ramires

If I come from 2012, I will never believe we have super star like Ramires in team, and super star cannot find a position in starting XI. But that's the truth for 2018. The new policy allows only 3 foreign players in squad. It seems we have no choice but using Teixeira, Boakye and Paletta.

Needless to say Teixeira is the soul of team on attack, Boakye provides Teixeira more room in the front. Without Boakye, the opponent can spend 3 defenders to deal with Teixeira. On the defense side, Paletta plays a fatal role in Jiangsu Suning's system. Not only for holding the back, but nowadays he is also responsible for ball controlling and passing through midfield. 

So the fact is, Wu Xi and Tian Yinong can do 70% that Ramires provides, but no one else can replace Teixeira, Boakye or Paletta. That's the reason Ramires just played 1 CFA Cup match. Even if he sent two assists, we were still not having him in the league.

I won't be surprise if Ramires leave us in this Summer. He needs playing time and totally deserves it. Otherwise, we need new formation to unleash his power.

## Newcomers

There are few good Chinese players to buy from the market, until Guangzhou Evergrand president Xu Jiayin stated that no one in his team is unavailable for sale. So can we take someone from Evergrand to enhance our squad? Rumor said Suning actually queried Zhang Linpeng, the Evergrand defender this winter. There are a few names in the Guangzhou team can benefit us, but I doubt if they really want to sell.

R&F's Xiao Zhi is on Suning's hunting list too. This 32y old forward became the best Chinese target man in his thirties. Quanjian's Yang Xu is my own top choice if we can't have Xiao Zhi.

If Ramires is going to leave, we will free a slot for new foreign player. After World Cup I think there will be some good players willing to come to China. A strong target forward is in top priority in my opinion. If we do it right, we can soon catch up in title race, like signing Roger Martinez in 2016. 

## Fitness

The fixtures after World Cup is really tough. From July 28th, we will play 6 matches in 3 weeks. For most cities in China, includes our home Nanjing, the weather at that time is simply, hell. Some of our key players includes Huang Zichang, Wu Xi, Abduhamit and Zhang Lingfeng, were called up in national team during the break. Huang Zichang, being called up by national team, U23 and U21 at the same time, must be tired. 

Tactically, our play relies on running very much, if we keeps using same squad for every match, it will be a huge challenge to the power of players.

Capello did a good job this winter, on building up a strong team in both tactical and fitness. During the summer camp, we need to keep up with it. Good news is probably we can utilize the facility in Inter's training center, to do it efficiently.


These are my cents on the summer expectation. Overall, I'm pretty optimistic to our second half of 2018.
