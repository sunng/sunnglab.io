---
layout: post
title: 'cljts: Java Topology Suite for Clojure'
categories:
- ANN
tags:
- clojure
- GIS
published: true
comments: true
---
<p>I almost forgot to announce this library I made half of a year ago. This library is aiming to bring Clojure to GIS. So you can manipulate geometry objects with a set of clojure functions.</p>

<p>The library covers :
<ul>
	<li>Geometries defined in Simple Feature Spec</li>
	<li>Spatial relationship test, based on DE-9IM.</li>
	<li>IO functions, WKT and WKB support</li>
	<li>some spatial analysis functions such as buffer, convex-hull</li>
</ul></p>

<p>Also, this week Alexey Pushkin sent pull request and added support for Perpared Geometry and Affine transformations.</p>

<p>The current release of cljts is 0.2.0-SNAPSHOT. You can find API document at <a href="http://sunng87.github.com/cljts/">here</a>. </p>

<p>If you are also interested in bringing clojure to GIS, feel free to get connected and hope I could help you.
</p>
