---
layout: post
title: The distribution of intelligence
categories:
- 手艺
tags:
- heatcanvas
- visualization
published: true
comments: true
---
<p>This could be a big title for the content.</p>

<p>I just work out a heat map based on <a href="http://maps.google.com/maps/ms?ie=UTF&msa=0&msid=203716810039202316490.0004aeb1b1a01b1d3b9af" target="_blank">the data</a> of users who enrolled online courses offered by Stanford.</p>

<p><a href="http://imgur.com/rWUkH"><img src="http://i.imgur.com/rWUkH.png" width="500" alt="" title="Hosted by imgur.com" /></a>
(Click to enlarge)</p>

<p>I think you already have your ideas about this map. New England, California and Central Europe has higher density than any other area of the world. Also you can find some light in South America, East Coast Australia, India and China. I think this could be an overview of how intelligence distributed around the globe.
</p>
