---
layout: post
title: Firefox addon for wms development
categories:
- 装备
tags:
- firefox
- GIS
- wms
published: true
comments: true
---
<p>今天一个叫做<a href="http://wiki.github.com/amercader/WMS-Inspector/">WMS Inspector</a>的Firefox扩展发布了。这个扩展用来查看页面上的WMS请求，还可以生成GetCapabilities的Report。</p>

<p><a href="http://www.flickr.com/photos/40741608@N08/4427008096/" title="Screenshot by 贝小塔, on Flickr"><img src="http://farm3.static.flickr.com/2678/4427008096_1e27a3caa8.jpg" width="500" height="116" alt="Screenshot" /></a>
<a href="http://www.flickr.com/photos/40741608@N08/4427007996/" title="Screenshot-GetCapabilities request by 贝小塔, on Flickr"><img src="http://farm3.static.flickr.com/2715/4427007996_30f5b42791_o.png" width="394" height="314" alt="Screenshot-GetCapabilities request" /></a>
<a href="http://www.flickr.com/photos/40741608@N08/4427007932/" title="Screenshot-Capabilities report - WMS Inspector - Mozilla Firefox by 贝小塔, on Flickr"><img src="http://farm3.static.flickr.com/2734/4427007932_8c3260710f.jpg" width="500" height="286" alt="Screenshot-Capabilities report - WMS Inspector - Mozilla Firefox" /></a></p>
