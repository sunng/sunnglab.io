---
layout: post
title: GeoHash
categories:
- 手艺
tags:
- GIS
- ruby
published: true
comments: true
---
<p>今天偶然看到一个Ruby的GeoHash库，功能非常简单，就是将经纬度坐标哈希成字符串，并且，利用这个字符串可以粗略地比较两点的距离。这个库的代码host在GitHub上，要安装，需要将github加入gem：
<em>sudo gem sources -a http://gems.github.com<br />
sudo gem install davotroy-geohash</em></p>

<p>用法，irb：
<em>irb(main):001:0&gt; require 'rubygems'<br />
=&gt; true<br />
irb(main):002:0&gt; require 'geohash'<br />
=&gt; true<br />
irb(main):003:0&gt; GeoHash.encode(32.168, 118.54)<br />
=&gt; "wtsr12n0nj"<br />
irb(main):004:0&gt; GeoHash.decode('wtsr12n0nj')<br />
=&gt; [32.168, 118.54]</em></p>

<p>代码可以从github上获得
<em>git clone git://github.com/davetroy/geohash.git</em></p>

<p>GeoHash的核心部分用C实现，仔细看一下可以发现是一个四叉树的结构：
<pre>[codesyntax lang="c"]
#define BASE32	"0123456789bcdefghjkmnpqrstuvwxyz"
static void encode_geohash(double latitude, double longitude, int precision, char *geohash) {
	int is_even=1, i=0;
	double lat[2], lon[2], mid;
	char bits[] = {16,8,4,2,1};
	int bit=0, ch=0;</pre></p>

<p>	lat[0] = -90.0;  lat[1] = 90.0;<br />
	lon[0] = -180.0; lon[1] = 180.0;</p>

<p>	while (i &lt; precision) {<br />
		if (is_even) {<br />
 			mid = (lon[0] + lon[1]) / 2;<br />
 			if (longitude &gt; mid) {<br />
				ch |= bits[bit];<br />
				lon[0] = mid;<br />
			} else<br />
				lon[1] = mid;<br />
		} else {<br />
			mid = (lat[0] + lat[1]) / 2;<br />
			if (latitude &gt; mid) {<br />
				ch |= bits[bit];<br />
				lat[0] = mid;<br />
			} else<br />
				lat[1] = mid;<br />
		}</p>

<p>		is_even = !is_even;<br />
		if (bit &lt; 4)<br />
			bit++;<br />
		else {<br />
			geohash[i++] = BASE32[ch];<br />
			bit = 0;<br />
			ch = 0;<br />
		}<br />
	}<br />
	geohash[i] = 0;<br />
}</p>

<p>[/codesyntax]

可惜在学校的时候没有看到这么精致的代码。</p>
