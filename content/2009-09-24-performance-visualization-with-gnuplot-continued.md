---
layout: post
title: Performance Visualization with Gnuplot, continued
categories:
- 装备
tags:
- foss
- gnuplot
- linux
- shell
- vmstat
published: true
comments: true
---
<p>After I post my command to <a href="http://www.commandlinefu.com/" target="_blank">commandlinefu.com</a>, there is an alternative command followed:
<em>(echo "set terminal png;plot '-' u 1:2 t 'cpu' w linespoints;"; sudo vmstat 2 10 | awk 'NR &gt; 2 {print NR, $13}') | gnuplot &gt; plot.png</em></p>

<p>This one is appreciated for using pipes and redirection. It works on Ubuntu Jaunty, on which, gnuplot doesn't provide a -e option. The author also showed an link to an open source project 'vmplot' on <a href="http://freshmeat.net/projects/vmplot/" target="_blank">freshmeat</a>. Vmplot is written in python, which combines parameters with scripts and end up with an os.system call. Grab the source from <a href="http://elvis.elte.hu/~fuji/stuff/vmplot.py" target="_blank">here</a>. (Why use python instead on a simple bash command? because it's difficult to draw a multiplot without temp file or variables, you cannot read '-' twice.)</p>

<p>There are several people vote me down for my command! It must be primers whose use ubuntu and knows the world via Synaptic only, because they failed to run the command on their dated machine (*_*)</p>
