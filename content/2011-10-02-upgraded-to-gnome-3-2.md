---
layout: post
title: Upgraded to GNOME 3.2
categories:
- 装备
tags:
- archilinux
- gnome
- linux
published: true
comments: true
---
<p>ArchLinux最大的魅力就在于Rolling Release，所有的悲喜剧你都比别人早一步见证。</p>

<p>升级到GNOME 3.2后，我的gnome-settings-daemon不能正常工作，导致gtk+的主题都无效。如果没有经验你可能不太容易注意到它的真实原因。最后找到了同病相怜的人，这个问题被报告在<a href="https://bugzilla.gnome.org/show_bug.cgi?id=660664" target="_blank">这里</a>。恰好是在我发现这个问题几个小时之前。在gnome解决这个问题之前，有一个简单的workaround：
<em>sudo mv /usr/lib/gnome-settings-daemon-3.0/libcolor.so /usr/lib/gnome-settings-daemon-3.0/libcolor.so~</em></p>

<p>库加载失败后gnome-settings-daemon会自动禁用这个插件，避免出现Segmentation fault。以上的操作，at your own risk。</p>

<p>此外，gnome-shell升级到3.2以后有些api的变化，我更新了exaile豆瓣电台的gnome-shell插件，你可以顺手git pull一下。
<a href="http://www.flickr.com/photos/40741608@N08/6203348334/" title="Screenshot at 2011-10-02 17:48:51 by 贝小塔, on Flickr"><img src="http://farm7.static.flickr.com/6162/6203348334_c71d757170.jpg" width="500" height="157" alt="Screenshot at 2011-10-02 17:48:51" /></a></p>

<p></p>
