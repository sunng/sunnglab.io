---
layout: post
title: "Archlinux 安全更换硬盘"
date: 2013-10-22 22:26
comments: true
categories: 
---

公司升级所有笔记本到SSD，本来是个好事，不过迁移是个难事。除了我，所有的同事都是OS X，他们有自动工具，毫无个性，不理他们。我们Linux要做这个事其实想想比较简单：首先，我们的分区和目录是抽象为两层的；其次，整个文件系统是透明的，只要拷贝就好了。

首先，拿到新硬盘要按需分区和格式化。用gpartd就可以，这一步千万不要忘记把/boot的目标分区设置为bootable，否则会导致无法引导。

分区完成之后，挂载各个分区，把当前硬盘里的所有文件用`cp -ar`拷贝到新硬盘上，这一步非常花时间。

搞定之后要再手动mkdir几个目录

* /run
* /sys
* /proc
* /dev

使系统启动的时候可以mount上特殊分区。

此外，要修改`/etc/fstab`，根据新的硬盘UUID来配置。UUID可以在`ll /dev/disks/by-uuid`里看到。

挂载一些特殊分区到新硬盘目录：

* `mount -o bind /dev /mnt/new/dev`
* `mount -t proc none /mnt/new/proc`
* `mount -t sys none /mnt/new/sys`

然后就可以chroot到新硬盘。

最后一步，在新硬盘上安装grub：

    grub-install /dev/sdb
    
之后可以检查一下生成的/boot/grub/grub.cfg，观察一下硬盘的uuid是否正确，如果不对可以手动改掉。

重启，见证奇迹。


