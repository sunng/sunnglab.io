---
layout: post
title: 'Maven recipe #0'
categories:
- 手艺
tags:
- java
- maven
- recipe
published: true
comments: true
---
<p>问题： 多个root pom的dependencyManagement有重复的内容，希望统一管理。<br />
解决：<br />
新建一个空pom.xml，在dependencyManagement中指定这些依赖，如<br />
[cc lang="xml"]
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"<br />
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd"></p>

<p>	<modelversion>4.0.0</modelversion>
	<groupid>info.sunng</groupid>
	<artifactid>root</artifactid>
	<packaging>pom</packaging>
	<version>0.0.1-SNAPSHOT</version>
	<dependencymanagement /></p>

<p>		<dependencies>
			<dependency>
				<groupid>info.sunng</groupid>
				<artifactid>X</artifactid>
				<version>0.0.1-SNAPSHOT</version>
			</dependency></dependencies></p>

<p>		
	</p>

<p>
[/cc]<br />
将这个pom.xml部署到你的Maven私服。</p>

<p>在其他root pom中添加这样的dependencyManagement<br />
[cc lang="xml"]<br />
....<br />
			<dependency>
				<groupid>info.sunng</groupid>
				<artifactid>root</artifactid>
				<version>0.0.1-SNAPSHOT</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
....<br />
[/cc]</p>
