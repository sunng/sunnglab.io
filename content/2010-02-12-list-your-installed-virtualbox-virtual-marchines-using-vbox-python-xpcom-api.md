---
layout: post
title: List your installed virtualbox virtual marchines using vbox python xpcom api
categories:
- 把戏
tags:
- linux
- python
- VirtualBox
published: true
comments: true
---
<p>I'm sorry for the long title. Due to lack of documentation, it's not easy to use python xpcom api from virtualbox sdk. The code below is just a sample that lists your installed virtual machines. It works on linux with VirtualBox OSE 2.0.8. Hope useful to you.<br />
[cc lang="python"]<br />
import vboxapi<br />
vmsg = vboxapi.VirtualBoxManager(None, None)<br />
vbox = vmsg.vbox<br />
vmsg.mgr.getSessionObject(vbox)<br />
machs = vbox.getMachines()<br />
names = map(lambda x: (x.name,x.id), machs)<br />
print names<br />
[/cc]<br />
Output:<br />
[cc lang="python" nowrap="false" line_numbers="false"]<br />
[(u'Ubuntu', u'aac3fd46-4f1e-4f6e-8abe-b1a5516abf8c'), (u'Windows', u'fa73952e-f0a6-4bf0-af20-259513d73bc1'), (u'OpenSolaris', u'a951542c-3178-4a6b-8087-f1b0314e283a'), (u'LinuxMint', u'8191f256-1b76-4ce7-a202-93730dae1b33'), (u'Fedora', u'ca250f45-d752-4baf-9281-fb0847dc8287'), (u'Freebsd', u'8322b62a-aed5-4f04-9815-9fadfeba9fe1'), (u'ArchLinux', u'efc1167a-4e06-4c14-aeb0-bfb303c8f5f7'), (u'openSUSE', u'02e18816-311d-420b-99b3-6b4b4cccf7b7'), (u'CentOS', u'ccf0421c-85c4-4123-8722-57abc494c633')]<br />
[/cc]</p>
