---
layout: post
title: You say goodbye and I say hello.
categories:
- 自话
tags: []
published: true
comments: true
---
<p><iframe width="560" height="315" src="http://www.youtube.com/embed/HBZ8ulc5NTg" frameborder="0" allowfullscreen></iframe></p>

<p>You say yes, I say no<br />
You say stop and I say go, go, go<br />
Oh, no<br />
You say goodbye and I say <strong>hello world</strong></p>

<p><img src="http://www.lysator.liu.se/c/dmr/dmr.gif" alt="Dennis Ritchie" /></p>

<p><img src="http://www.globalnerdy.com/wordpress/wp-content/uploads/2008/09/the_c_programming_language.jpg" alt="the C programming language" /></p>

<p>[cc lang="C"]<br />
#include<stdio.h /></p>

<p>int main(){<br />
    printf("Dennis Ritchie, %d-%d\n", 1941, 2011);<br />
    return 0;<br />
}
[/cc]</p>

<p>RIP.
</p>
