---
layout: post
title: Sunng's Canvas based Heatmap API
categories:
- ANN
- 手艺
tags:
- canvas
- html
- javascript
- visualization
- web
published: true
comments: true
---
<p>Glad to announce my works this morning: A simple heatmap API based on HTML5 canvas.
<a href="http://www.flickr.com/photos/40741608@N08/4347604245/" title="canvas by 贝小塔, on Flickr"><img src="http://farm5.static.flickr.com/4071/4347604245_486bd3a12e_o.jpg" width="300" height="215" alt="canvas" /></a></p>

<p>The programming interface is rather simple now. To create such a heatmap, you just new a heatmap object by:
<pre class="brush:html">
&lt;canvas width="300" height="215" id="canv"&gt;&lt;/canvas&gt;
</pre>
<pre class="brush:javascript">
heatmap = new HeatMap("canv");
</pre></p>

<p>Then read your dataset and push data into the heatmap:
<pre class="brush:javascript">
heatmap.push(x, y, value);
</pre></p>

<p>At last, spread the data and get the canvas rendered:
<pre class="brush:javascript">
heatmap.spread();
heatmap.render();
</pre>
Now you got it.</p>

<p>For advanced usage, you can specify the resolution of heatmap by px:
<pre class="brush:javascript">
heatmap = new HeatMap("canv", 2);
</pre>
Large value will gain performance with low image quality.</p>

<p>Also, you can specify a value to define the attenuation value of each px.
<pre class="brush:javascript">
heatmap.spread(5);
</pre></p>

<p>Finally, there is an option to use your own color schema for heatmap generation.
<pre class="brush:javascript">
heatmap.render(function(value, maxValue){
    var light = value / maxValue * 100;
    return "hsl(20, 75%, "+light+"%)";
});
</pre>
The parameters passed in are current pixel value and max value in whole context.</p>

<p>That's all the toolkit. More functionality and options might be added in future. Grab it from my bitbucket page:
<a href="http://bitbucket.org/sunng/daily-coding/src/tip/canvas-heatmap/">http://bitbucket.org/sunng/daily-coding/src/tip/canvas-heatmap/</a></p>

<p>There is a demo page which you can test the api by clicking canvas:
<a href="http://www.flickr.com/photos/40741608@N08/4347650163/" title="canvas-raw by 贝小塔, on Flickr"><img src="http://farm3.static.flickr.com/2733/4347650163_ea740356e4_o.jpg" width="326" height="284" alt="canvas-raw" /></a>
then click heatmap button!
<a href="http://www.flickr.com/photos/40741608@N08/4348397874/" title="canvas2 by 贝小塔, on Flickr"><img src="http://farm3.static.flickr.com/2796/4348397874_8ac80b0756_o.jpg" width="314" height="275" alt="canvas2" /></a></p>

<p>This is the new year gift for my readers and my dear friends !</p>
