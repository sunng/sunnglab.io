---
layout: post
title: Clojure Developers You Should Follow
categories:
- 他山
tags:
- clojure
- opensource
published: true
comments: true
---
<p>介绍一下Clojure社区的活跃人物，有助于大家更好地学习和了解clojure，以下罗列的前提是包含但不限于，排名亦不分先后。</p>

<p><h3>Rich Hickey (richhickey)</h3>
Clojure的创始人。</p>

<p><h3>Michael Fogus (fogus)</h3>
The joy of clojure一书的作者，clojure社区的活跃人物，Clojure/core的成员。参与和维护着很多clojure项目。</p>

<p><h3>James Reeves (weavejester)</h3>
Compojure等一系列web框架的作者。是Clojure web开发方面的先驱，还维护着大量的clojure项目。</p>

<p><h3>Mark McGranaghan (mmcgrana)</h3>
Clojure web规范Ring的创始人，还维护着clj-redis等项目。</p>

<p><h3>Stuart Hallowa (stuarthalloway)</h3>
Programming Clojure一书的作者，Clojure的核心开发人员之一。</p>

<p><h3>Christophe Grand (cgrand)</h3>
cgrand是clojure web开发方面的专家，维护了enlive moustache等库。</p>

<p><h3>Phil Hagelberg (technomancy)</h3>
Clojure构建工具leiningen的作者。</p>

<p><h3>Zach Tellman (ztellman)</h3>
Zach是框架aleph/lamina/gloss的作者，是clojure网络编程的专家。</p>

<p><h3>Nathan Marz (nathanmarz)</h3>
Nathan是twitter的clojure dev，维护着一些与hadoop相关的clojure项目。</p>

<p>以上列举的人都可以在github和twitter上找到。<br /><br /><div class="zemanta-pixie"><img class="zemanta-pixie-img" alt="" src="http://img.zemanta.com/pixy.gif?x-id=eafeb545-d70d-8011-8df5-66febda32431" /></div></p>
