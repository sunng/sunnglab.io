---
layout: post
title: 'My response to Spark: Visualize your mercurial commit history from commandline'
categories:
- ANN
tags:
- github
- hg
- opensource
- python
- visualization
published: true
comments: true
---
<p>标题长了些。还是用母语吧。</p>

<p>昨天HackerNews上一个<a href="http://news.ycombinator.com/item?id=3237478" target="_blank">小脚本</a>轰动了，所谓山不在高程序不在小。为了响应这个小脚本，我写了一个更加简单的Mercurial(hg)扩展，帮助你输出hg仓库的提交历史。这个feature和github的直方图有点像，在我看来github的直方图是他们最重要的feature之一，它鞭策着你不断地commit。</p>

<p><img src="http://i.imgur.com/ZcEfH.png" alt="hg summary" /></p>

<p><script src="https://gist.github.com/1366606.js"> </script></p>

<p>安装:将这个脚本放在任意一处，在你的hgrc中添加：<br />
[cc lang="text"]<br />
[extensions]<br />
summary = /path/to/your/script<br />
[/cc]</p>

<p>由于很简单，就不按照mercurial的规范发布了。</p>
