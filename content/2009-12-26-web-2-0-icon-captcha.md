---
layout: post
title: Web 2.0 Icon Captcha
categories:
- ANN
tags:
- captcha
- project
- Yan
published: true
comments: true
---
<p>Yan 新增了一种验证码类型，Web 2.0 图标验证码。用户根据图标的内容和提示的信息，提交验证码。验证码图片如下：</p>

<p><img class="alignnone" src="http://farm3.static.flickr.com/2556/4215434144_f589085acd_o.png" alt="" width="256" height="192" /></p>

<p>提示文字： Please figure out twitter icons.</p>

<p>用户输入Twitter图标左上角上的字母，即可进行验证。在Yan的测试界面上使用如图：</p>

<p><img class="alignnone" src="http://farm3.static.flickr.com/2565/4214663971_cdc2168f69_o.png" alt="" width="394" height="503" /></p>

<p>Web2.0 Icon实际上是Yan中新增的拼图验证码的一个实例，利用拼图验证码可以生成相似的更有创意的验证码。在我的开发环境中生成这样一张图片大约需要80ms。</p>

<p>项目中使用的图标均从互联网收集，遵循CC等协议或经作者授权，详情参考项目中README文件。</p>

<p>祝DAF同学生日快乐。</p>
