---
layout: post
title: Setting Environment Variables for NetBeans, Continued
categories:
- 把戏
tags:
- java
- netbeans
published: true
comments: true
---
<p>This is a feature introduced in NetBeans 6.7.</p>

<p><img class="alignnone" src="http://farm5.static.flickr.com/4072/4227520447_bccc557571.jpg" alt="" width="500" height="287" /></p>

<p>Set environment variables for specified life-cycle phases: Open project properties dialog, select action category. Select actions which depend on the variable, click add button to add such an environment variable.</p>

<p>However, this method doesn't work with Glassfish spawned the IDE. If you want use environment variables in the server spawned by NetBeans IDE, you still have to export the variables in netbeans.conf</p>
