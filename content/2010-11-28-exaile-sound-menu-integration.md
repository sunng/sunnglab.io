---
layout: post
title: Exaile sound menu integration
categories:
- 装备
tags:
- exaile
- opensource
- ubuntu
- work
published: true
comments: true
---
<p><a href="https://wiki.kubuntu.org/SoundMenu">Sound menu indicator</a> was introduced in Ubuntu Maverick(10.10).  User can control playback from this menu, and view current track information.</p>

<p>The control is based on mpris spec 2.0, which is a common specification of controlling media player with dbus. To enable sound menu support, media player should implement mpris 2.0, and register itself with libindicate.</p>

<p>So this week,  I have been working on an exaile plugin to support sound menu and finally got a workable version.</p>

<p><a href="http://www.flickr.com/photos/40741608@N08/5213528341/" title="Screenshot of exaile sound menu by 贝小塔, on Flickr"><img src="http://farm6.static.flickr.com/5004/5213528341_3587622fb5.jpg" width="500" height="313" alt="Screenshot of exaile sound menu" /></a></p>

<p>The code repo can be found at github:
<a href="https://github.com/sunng87/Exaile-Soundmenu-Indicator">https://github.com/sunng87/Exaile-Soundmenu-Indicator</a></p>
