---
layout: post
title: 'OpenStreetMap Nanjing: A Year of Edits, 2012'
categories:
- 装备
tags:
- Nanjing
- OpenStreetMap
published: true
comments: true
---
<p>又到了A Year of Edits节目时间了，去年的场景还<a href="http://sunng.info/blog/2012/01/openstreetmap-nanjing-a-year-of-edits/" target="_blank">历历在目</a>。2012年OSM上的南京地图，变化更加可观。</p>

<p><img src="http://i.imgur.com/S4OSv.png" alt="" /></p>

<p>脚本和mapnik依然在<a href="https://gist.github.com/1639915" target="_blank">原处</a>，mapnik升级到2.1.x，配置文件有了一些变化。</p>
