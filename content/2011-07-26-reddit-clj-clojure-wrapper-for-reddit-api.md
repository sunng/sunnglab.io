---
layout: post
title: 'Reddit.clj: clojure wrapper for Reddit API'
categories:
- ANN
tags:
- clojure
- project
- reddit
published: true
comments: false
---
<p>As mentioned in <a href="http://sunng.info/blog/2011/07/weekend-project-rageviewer/" target="_blank">Rage Viewer</a>, I have another clojure library to communicate with Reddit. Now it's public available as "<a href="https://github.com/sunng87/reddit.clj" target="_blank">reddit.clj</a>".</p>

<p>Reddit.clj is managed with leiningen. The current reddit.clj release is <del datetime="2011-07-26T13:29:58+00:00">0.1.3</del> 0.2.0. You can include it in your project.clj:<br />
[cc lang="clojure"]<br />
(defproject xxxx "1.0.0-SNAPSHOT"<br />
  :dependencies [[reddit.clj "0.2.0"]])<br />
[/cc]</p>

<p>This is a simple tutorial for you to getting started :<br />
[cc lang="clojure"]<br />
;; include reddit.clj<br />
(require '[reddit.clj.core :as reddit])<br />
;; create a reddit client with reddit/login.<br />
;; you can also ignore the arguments to use it<br />
;; anonymously<br />
(def rc (reddit/login "your-reddit-name" "your-reddit-passwd"))</p>

<p>;; load reddits from subreddit "clojure", and print titles<br />
(doseq<br />
 [title (map :title (reddit/reddits rc "clojure"))]<br />
  (println title))</p>

<p>;; enhance reddit client to enable writing, for login user only<br />
;; this operation will retrieve reddit user modhash under the hood<br />
(def rc (reddit/enhance rc))</p>

<p>;; vote-up a thing on reddit<br />
(reddit/vote-up rc "t3_iz61z")</p>

<p>;; you may also submit links to reddit.<br />
;; permalink will be returned when success.<br />
;; be careful to use this API because reddit may ask you for a<br />
;; captcha. But as a library, reddit.clj will return nil on this case.<br />
;; use it at your own risk.<br />
(reddit/submit-link rc "title" "url" "subreddit-name")</p>

<p>[/cc]</p>

<p>Detailed API documents are listed <a href="http://sunng87.github.com/reddit.clj/reddit.clj.core-api.html" target="_blank">here</a>.</p>

<p>This library should be useful to crawl data from reddit which is rich of information. Reddit.clj is hosted on <a href="https://github.com/sunng87/reddit.clj" target="_blank">github</a>.</p>
