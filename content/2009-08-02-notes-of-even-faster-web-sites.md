---
layout: post
title: Notes of Even Faster Web Sites
categories:
- 手艺
tags:
- javascript
- web
published: true
comments: true
---
<p><ol>
	<li>异步、模拟多线程式的JavaScript，防止界面锁死。例如Google Gears，Firefox3.5 WebWorker异步API。</li>
	<li>拆分JavaScript文件，即需即载入即运行，避免首次载入的高代价。类似dojo的分包机制。</li>
	<li>普通的script会阻塞页面的载入直至脚本下载执行完成，采用其它方法可以改变这种情况：XHR Evel, XHR Inject, Script DOM, Script Defer, Script in Iframe, Document.write Script Tag。</li>
	<li>由于外部script和内联代码异步载入造成的冲突，解决方法1. 在外部编码种硬编码回调，2. 在Window.onload中调用 3. 用timer监控载入情况 4.scripttag.onreadystatechange</li>
	<li>把异步载入外部脚本的方法编写为可复用的module</li>
	<li>处理内联脚本，解决方法：1.将内联脚本移至页面尾部 2.异步执行JavaScript回调(setTimeout(0)) 3.使用script defer属性 内联脚本执行会被</li>
	<li>语言级别上提高JavaScript执行效率的一些手段，Zakas</li>
	<li>Comet的前景和现阶段实现，以及WebSocket</li>
	<li>优化HTML文件</li>
	<li>优化图片的utilities和几种格式的特点</li>
	<li>sharding文件到不同的服务器上增加并行下载</li>
	<li>正确地使用flush提高部分页面的加载速度 影响因素1.是否开启了Output_Buffer，2.是否使用了chunked encoding， 3.是否启用了gzip并在apache228上运行 4. 是否被代理和AV软件影响了 5.是否因为相同的域名的文件下载被block了 6.是否是webkit阈值以下不能渲染</li>
	<li>慎用iframe，DOM操作代价过高，在html中设置src的iframe会阻塞onload，iframe与style script标签的位置</li>
	<li>高性能的CSS选择器，避免全局rule，避免div#sele，避免div.sele，避免使用长选择器，避免使用decendant选择器，避免使用tag-child选择器，查看所有child选择器，使用继承</li>
</ol></p>
