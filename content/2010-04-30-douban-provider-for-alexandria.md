---
layout: post
title: Douban provider for Alexandria
categories:
- ANN
tags:
- Douban
- foss
- gnome
- ruby
published: true
comments: true
---
<p><a href="http://alexandria.rubyforge.org/">Alexandra</a> is a desktop book collection manager on gnome written in ruby. With an extensible architecture, Alexandria uses different sources to retrieve book's data, including Amazon and many local sites.</p>

<p>Douban.com is considered to be most applicable data source for books published in Chinese. So I write this provider according to Alexandria's SPI. Now it's possible to add Chinese books and manage reading lists.</p>

<p><a href="http://www.flickr.com/photos/40741608@N08/4564768299/" title="alexandria by 贝小塔, on Flickr"><img src="http://farm5.static.flickr.com/4046/4564768299_f424dc2be9.jpg" width="500" height="388" alt="alexandria" /></a></p>

<p>Now the code can be found in Alexandria's bug tracker:
<a href="http://rubyforge.org/tracker/index.php?func=detail&aid=28160&group_id=205&atid=865">http://rubyforge.org/tracker/index.php?func=detail&aid=28160&group_id=205&atid=865</a></p>

<p>However, the patch file of book_providers.rb in that list is for svn trunk head version only. To use it with currently stable version of Ubuntu, first, make sure you have Alexandria version <strong>0.6.5-0ubuntu1</strong> and <strong>libjson-ruby1.8</strong> installed.</p>

<p>Download douban.rb from rubyforge:
<a href="http://rubyforge.org/tracker/download.php/205/865/28160/4923/douban.rb">http://rubyforge.org/tracker/download.php/205/865/28160/4923/douban.rb</a>
Copy the file to /usr/lib/ruby/1.8/alexandria/book_providers/ with super user privileges.<br />
Use this patch to /usr/lib/ruby/1.8/alexandria/book_providers.rb<br />
[cc lang="ruby"]<br />
310a311,323
<     begin
<       begin
<         require 'json'
<       rescue LoadError
<         require 'rubygems'
<         require 'json'
<       end
<       require 'alexandria/book_providers/douban'
<     rescue LoadError =< ex
<       log.error{ex}
<       log.warn {'Fail to load douban as provider'}
<     end
<<br />
[/cc]</p>

<p>Feel free to report issue here.</p>
