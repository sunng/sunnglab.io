---
layout: post
title: Next to Richard Stallman
categories:
- 自话
tags:
- gnome
- linux
published: true
comments: true
---
<p><p>我这个人一般不八卦，你知道的。但是个别时候实在是情难以堪。</p>
<p>Bug 626593 - Gnome ate my boyfriend! Help!<br />
<a href="https://bugzilla.gnome.org/show_bug.cgi?id=626593">https://bugzilla.gnome.org/show_bug.cgi?id=626593</a></p>
<p>这位女士在GNOME bugzilla上提交bug，bug提交在empathy项目下。（注 dict empathy: 心意相通）</p>
<p>先不看正文，备注信息就极具幽默感：<br />
Status:  	 RESOLVED INVALID<br />
Product: 	empathy<br />
Component: 	User Guide<br />
Version: 	unspecified<br />
OS: 	Windows<br />
Importance:  	Normal critical </p>
<p>是不是真的RESOLVED就不知道了。</p>
<p>事情的直接原因是上周海牙GUADEC，此男cancel了周末和这位reporter的约会。此女不堪长期忍受此男、Linux、Maemo、C等等，彻底爆发了。</p>
<p>He even tried to put Linux on my computer and I simply could not take it.  I came home from work one day and my computer said "UNIX" all over it!<br />
这句有力的证明了我曾经看到的一句箴言，给女生装linux，对她们、对电脑、对linux都是一种折磨。</p>
<p>接下来搞笑开始了。一楼就是一位极缺乏幽默感的大叔：<br />
Akhil Laddha      2010-08-11 06:45:46 UTC<br />
This is a GNOME bug tracking system, not any family consultancy.</p>
<p>二楼是位好心人，提出了一个Linux week计划，还一厢情愿整了个交易计划：<br />
for each bug you report, he has to spend one night with you without the computer.</p>
<p>八楼精华：<br />
 David Liang      2010-08-12 08:53:16 UTC<br />
不得不顶</p>
<p>九楼亮出一个家属俱乐部的邮件列表：<br />
gnome-women-list@gnome.org，说你要是在家属圈子里麻将打得无敌手，自然你的boyfriend就崇拜你了，到时你就是爷了</p>
<p>高潮在十楼出现了，“你不说这男的是谁我们怎么帮你？”</p>
<p>紧接着十一楼一位神秘人士笑而不语。</p>
<p>十四楼比较直接<br />
#apt-get remove boyfriend --purge</p>
<p>十九楼知道宁拆十座庙的道理，建议先查看一下情况再说：<br />
cat /dev/boyfriend | grep love</p>
<p>最冷的是十六楼说咱们有这么个项目的：<br />
<a href="http://projects.gnome.org/outreach/women/">http://projects.gnome.org/outreach/women/</a></p>
<p>你以为故事就这么结束了，你错了。<br />
没想到男主人公的网站被我不经意间人肉出来了：<br />
<a href="http://zachgoldberg.com">http://zachgoldberg.com</a></p>
<p>一上来第一篇就说这事：<br />
<a href="http://zachgoldberg.com/2010/08/12/help-my-girlfriend-learned-how-to-use-a-bug-tracker/">HELP! My Girlfriend Learned How To Use A Bug Tracker</a></p>
<p>I never thought it would happen.</p>
<p>此男字字珠玑：<br />
It all started one night when I got home very late from work (where I get to play with Linux all day… who would ever go home?).  I got the usual “you need to pay more attention to me” and “Linux will never have sex with you!”.  I sat through it all and when it was over she went to sleep and I…. opened up my Laptop (running Ubuntu Linux, of course) and started hacking.  All is right with the world.</p>
<p>于是他发现自己上了LWN的Quote of the Week，和RMS的新闻并列，然后又被转载到hacker news, geek.com, slashdot。他感叹道 I never imagined my entry into the “slashdot number of zero” (think Erdos or Kevin Bacon) club would happen in this way.</p>
<p>最后他说他要开一个bugs.zachgoldberg.com专门解决这类的问题，If you want your own component in the bugtracker to help you and your loved one vent your problems all you need to do is ask!</p>
<p>哈哈哈哈</p>
<p>补充个精华链接：http://www.reddit.com/r/linux/comments/d02j6/bug_626593_gnome_ate_my_boyfriend/</p>
<p>最后daf同学总结性地指出，她不会学习使用bug tracker。</p>
<p>The post is brought to you by <a href="http://fedorahosted.org/lekhonee">lekhonee</a> v0.7</p></p>
