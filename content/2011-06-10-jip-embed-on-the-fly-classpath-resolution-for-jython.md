---
layout: post
title: 'jip.embed: On-the-fly classpath resolution for Jython'
categories:
- ANN
tags:
- jip
- jython
- opensource
- project
published: true
comments: true
---
<p>jip 0.7 introduces a module called jip.embed, which allows you to add libraries to your code in the runtime as you declare them. With jip.embed, you don't have to download jars manually and append them to your -Dpython.path. You just pick your editor, import jip.embed, code your business, then save and run it.</p>

<p>Code example:</p>

<p>[cc lang="python"]<br />
import jip.embed<br />
jip.embed.require('commons-lang:commons-lang:2.6')<br />
from org.apache.commons.lang import StringUtils</p>

<p>print StringUtils.reverse('jip rocks')<br />
[/cc]</p>

<p>Output:<br />
[cc]<br />
skcor pij<br />
[/cc]<br />
(jip will print some log here if dependencies are included for first time)</p>

<p>So please check out my new released jip, 0.7:
<a href="https://github.com/sunng87/jip">https://github.com/sunng87/jip</a></p>

<p>For virtualenv user, you can install full-featured jip via pip:
<font face="monospace">$ pip install jip</font></p>

<p>To install jip globally, download the package from <a href="http://pypi.python.org/pypi/jip">python cheese shop</a> and run:
<font face="monospace">$ jython setup.py install </font><br /><br /><div class="zemanta-pixie"><img class="zemanta-pixie-img" alt="" src="http://img.zemanta.com/pixy.gif?x-id=36db589c-cfdd-8173-8d89-3b7aeb29cb5c" /></div></p>
