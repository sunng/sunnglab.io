---
layout: post
title: Visualize call tree of a C function
categories:
- 把戏
tags:
- c
- dot
- foss
- visualization
published: true
comments: true
---
<p><h3>Requirement</h3>
You want to visualize a call hierarchy of a C function.</p>

<p><h3>Solution</h3>
Utilities you need are listed below:
<ul>
<li><a href="http://www.gnu.org/software/cflow/">GNU cflow</a></li>
<li><a href="http://cflow2vcg.sourceforge.net/">cflow2vcg</a></li>
<li>graphviz</li>
</ul></p>

<p>Take 'rdbSaveBackground' (<a href="http://github.com/antirez/redis/blob/master/src/rdb.c">redis/rdb.c</a>) for example:<br />
[cc lang="bash"]<br />
cflow --format=posix --omit-arguments --level-indent='0=\t' --level-indent='1=\t' --level-indent=start='\t' -m 'rdbSaveBackground' ~/osprojects/redis/src/rdb.c | cflow2dot | dot -Tjpg -o rdb.jpg<br />
[/cc]</p>

<p>Output:
<a href="http://www.flickr.com/photos/40741608@N08/5020142591/" title="visualization of a call tree by 贝小塔, on Flickr"><img src="http://farm5.static.flickr.com/4111/5020142591_7b34a52e3c.jpg" width="500" height="98" alt="visualization of a call tree" /></a></p>

<p>Source: <a href="http://unixdiary.blogspot.com/2006/05/using-cflow.html">unix diary</a></p>

<p>The post is brought to you by <a href="http://fedorahosted.org/lekhonee">lekhonee</a> v0.7</p>
