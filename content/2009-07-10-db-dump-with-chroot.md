---
layout: post
title: DB dump with chroot
categories:
- 装备
tags:
- database
- linux
- mysql
- postgresql
- shell
- sqlite
published: true
comments: true
---
<p>I caught chroot first time when attempting to install gentoo linux. Of course, its a tool of great useful. Two months ago I had my laptop crashed, and all static files were recovery easily by copy while raw db data file seems to be difficult to handle.</p>

<p>The old file system is in a mobile disk, mounted at /media/disk-1/. Now I try to use chroot to rollback to that environment, so that all standard dump tools will be available.</p>

<p>Before you chroot to the system, don't forget to bind /dev to new location. Or you might get error prompt such as "/dev/null Permission Denied" (it's a common error). Just execute command below:
<em>$ sudo mount --bind /dev /media/disk-1/dev</em></p>

<p>Then change root to my old system:
<em>$ sudo chroot /media/disk-1</em></p>

<p>Now you become root user automatically.
<h3>MySQL Dump</h3>
In a standard ubuntu mysql installation, we should launch mysql db from init.d by:
<em># /etc/init.d/mysql start</em></p>

<p>But it's no longer available in such environment. Fortunately, there is a direct way:
<em># mysqld_safe &amp;</em></p>

<p>OK, go on to dump database with mysqldump:
<em># mysqldump --all-databases &gt; mysql_dump_file</em>
<h3>Postgresql Dump</h3>
We cannot use init.d to start pgsql either. Therefore, try to run it by:
<em># su postgres -c "/usr/lib/postgresql/8.3/bin/pg_ctl start -D /etc/postgresql/8.3/main"</em></p>

<p><em>/etc/postgresql/8.3/main</em> is the default data directory in standard installation(with apt-get). This directory is supposed to contain a file named with "postgesql.conf"</p>

<p>Now pgsql db is also running. As root user we have no privilege to run pgsql utilities. so take following steps:
<em># touch pgsql_dump_file<br />
# chmod a+w pgsql_dump_file<br />
# su postgres -c "pg_dumpall -f pgsql_dump_file"</em>
<h3>Conclusion</h3>
Personally, I found it's too complex and low efficiency to backup and restore data with mysql and postgresql. If there is no critical requirement(just like personal data management), file based db(Surely I mean sqlite) is no doubt better choice. Easy management(lots of gui tool now provided), smooth copy and move, standard db interface for programming, and enough functionality.</p>
