---
layout: post
title: 'OpenStreetMap Nanjing: A Year of Edits'
categories:
- 装备
tags:
- OpenStreetMap
published: true
comments: true
---
<p><img src="http://i.imgur.com/0mVFk.png" alt="osm nanjing" /></p>

<p>这是2011年OpenStreetMap上，南京的编辑情况。高亮的部分是2011年创建或更新的要素。从这张图上可以看出最近的这一年，南京的数据从无到有到逐渐的完善，这里面倾注了本地几位贡献者结结实实的心血。</p>

<p>对这个可视化感兴趣，可以参考<a href="https://gist.github.com/1639915" target="_blank">这里的代码和样式表</a>。</p>
