---
layout: post
title: Using dojo on Chrome/Chromium from local file system
categories:
- 手艺
tags:
- Chrome
- dojo
- web
published: true
comments: true
---
<p>dojo.require uses XMLHttpRequest to load additional components. On default Chrome settings, XMLHttpRequest on local file system is not allowed. This will break whole dojo system.</p>

<p>To change the default policy, you can start Chrome with a command line option <i>--allow-file-access-from-files</i>.</p>

<p>This issue is a well known one for Chromium project and still under active discussion:
<a href="http://code.google.com/p/chromium/issues/detail?id=40787">http://code.google.com/p/chromium/issues/detail?id=40787</a></p>
