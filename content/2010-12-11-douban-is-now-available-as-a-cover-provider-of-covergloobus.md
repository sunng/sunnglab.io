---
layout: post
title: Douban is now available as a cover provider of CoverGloobus
categories:
- 装备
tags:
- Douban
- foss
published: true
comments: true
---
<p>This is a feature requested by <a href="http://www.douban.com/people/4350866/">a douban user</a>, we need a provider for <a href="https://launchpad.net/covergloobus">CoverGloobus</a> to retrieve covers from douban.com</p>

<p>I just modify the code of exaile-douban-cover, and adapt to CoverGloobus 1.7 API. In 1.7, CoverGloobus has simplfied SPI for cover providers, which has much enhancement from 1.6 . So this provider is only compatible with CoverGloobus 1.7 .</p>

<p>The patch is now merged into the bazaar repository, you can find it at:<br />
http://bazaar.launchpad.net/~gloobus-dev/covergloobus/trunk/files/head%3A/src/covers/</p>

<p>Chinese user will benefit from this patch that CoverGloobus will try to download album art if it's not available from your music player.</p>
