---
layout: post
title: Exaile doubanfm plugin 0.0.6b
categories:
- ANN
tags:
- Douban
- exaile
- ubuntu
published: true
comments: true
---
<p>上周豆瓣电台增加了电影原声频道，这次的插件更新支持了这个新频道（OST）。</p>

<p>下载地址<br />
https://github.com/sunng87/exaile-doubanfm-plugin/downloads</p>

<p>安装指南<br />
https://github.com/sunng87/exaile-doubanfm-plugin/wiki/Installation</p>

<p>欢迎使用！</p>
