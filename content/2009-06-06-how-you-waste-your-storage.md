---
layout: post
title: How you waste your storage?
categories:
- 留影
tags:
- foss
- linux
- VirtualBox
published: true
comments: true
---
<p><img class="alignnone" title="some kinds of waste" src="http://pic.yupoo.com/classicning/2074478db967/epko3a17.jpg" alt="" width="200" height="381" /></p>

<p>250G硬盘是这样被使用的。一个K桌面的ArchLinux，一个俄语界面的LinuxMint，一个出于好奇心的OpenSolaris，一个用来测试的Ubuntu Server，以及，以及一个不能免俗的盗版Windows。</p>

<p><a href="http://blog.samsonis.me/2009/06/install-ubuntu-on-virtualbox-part-1/" target="_blank">Check here</a> to see a complete installation guide of <a href="http://www.ubuntu.com" target="_blank">Ubuntu Linux</a> on <a href="http://www.virtualbox.org" target="_blank">VirtualBox</a>.</p>
