---
layout: post
title: Reading GPS Data From EXIF Using Groovy
categories:
- 手艺
tags:
- GIS
- groovy
- java
published: true
comments: true
---
<p>废话不说了，直接上code吧：</p>

<p><pre class="brush:groovy">
import com.drew.imaging.jpeg.*;
import com.drew.metadata.*;
import com.drew.metadata.exif.*;</pre></p>

<p>file = new File("sample.jpg");<br />
meta = JpegMetadataReader.readMetadata(file);</p>

<p>gpsdir = meta.getDirectory(GpsDirectory.class);<br />
lat = gpsdir.getRationalArray(GpsDirectory.TAG_GPS_LATITUDE);<br />
lon = gpsdir.getRationalArray(GpsDirectory.TAG_GPS_LONGITUDE);<br />
lats = lat[0].doubleValue() + lat[1].doubleValue()/60 + lat[2].doubleValue()/3600;<br />
lons = lon[0].doubleValue() + lon[1].doubleValue()/60 + lon[2].doubleValue()/3600;<br />
println(lats);<br />
println(lons);
</p>

<p>metadata-extractor似乎是现在惟一的读取exif信息的java库。在ivy中可以添加这样一个依赖：</p>

<p><pre class="brush:xml">
&lt;dependency org=&quot;com.drewnoakes&quot; name=&quot;metadata-extractor&quot; rev=&quot;2.4.0-beta-1&quot; conf=&quot;runtime&quot;/&gt;
</pre></p>
