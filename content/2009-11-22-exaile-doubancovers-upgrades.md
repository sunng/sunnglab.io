---
layout: post
title: Exaile-DoubanCover 0.0.2
categories:
- ANN
tags:
- Douban
- exaile
- foss
- glade
- python
published: true
comments: true
---
<p>The Exaile plugin "Douban Covers" has been upgraded to 0.0.2. Now there is a new preference pane inside exaile preference dialog. This update provides you an optional choice to specify your own apikey when access douban.com open api. With an apikey, your request frequency will be raised to 40 times per minute.</p>

<p><img class="alignnone" src="http://farm3.static.flickr.com/2727/4124219684_c93af1dd03.jpg" alt="" width="500" height="414" /></p>

<p>Again, you can get the plugin from:
<a href="http://bitbucket.org/sunng/exailedoubancovers/downloads/">http://bitbucket.org/sunng/exailedoubancovers/downloads</a>/</p>

<p>Grab source with mercurial:
<em>hg close http://bitbucket.org/sunng/exailedoubancovers/</em></p>

<p>Also, exaile-cn has its first 0.3.0-compatible version released early today. As a part of Exaile-cn, Exaile-Doubancovers <strong>0.0.1</strong> has been packaged into the public release for the first time. Find the project at: <a href="http://code.google.com/p/exaile-cn/">http://code.google.com/p/exaile-cn/</a></p>
