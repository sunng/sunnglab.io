---
layout: post
title: 终于能用Unity和Gnome-shell了
categories:
- 装备
tags:
- ubuntu
published: true
comments: true
---
<p>自从升级到Maverick之后，有两个问题就一直如鲠在喉，GNOME3会话和Ubuntu Netbook(Unity)会话能不能用。前者启动之后出现Panel后全部僵死，鼠标可以移动但是任何操作都没有响应；后者桌面背景图片一闪就黑屏，没有任何响应。</p>

<p>查看unity的xsession-error日志，其中的报错比较含糊，搜索也没有找到相似的问题。查看GNOME-Shell的xsession-error之后发现很多JS报错，搜索了一下，终于找到了一个bug报告：
<a href="https://bugs.launchpad.net/ubuntu/+source/mutter/+bug/618907">https://bugs.launchpad.net/ubuntu/+source/mutter/+bug/618907</a></p>

<p>情况和我一样，都是之前曾经安装过PPA版本的GNOME-Shell尝鲜，升级之后出现了JS报错。按照说明打开synaptic，果然有两个包使用的是git版本，尽管PPA已经删除，但是已安装的软件包只会被标记为Local or Obsolete，恰好这个git版本是0.9.4比Maverick里的0.9.3还要高，这样更新的时候又不会提示。如果不细看还真的不容易发现。按下Ctrl+E， synaptic会提示Force Version对话框，选择仓库的旧版本，安装、重启。这下不仅GNOME-Shell正常工作了，同样依赖Mutter的Unity也正常了。</p>

<p>这次悲剧，说明使用PPA还是要慎重，尤其是跨版本升级之后，要小心PPA被禁用但软件包没有被升级的情况。</p>
