---
layout: post
title: 'Exaile cover plugin: Douban Covers'
categories:
- 装备
tags:
- Douban
- exaile
- foss
- python
published: true
comments: true
---
<p>I have created a plugin for <a href="http://exaile.org" target="_blank">exaile</a> which fetches album cover from <a href="http://douban.com" target="_blank">douban.com</a></p>

<p>You can download the plugin archive from:
<a href="You can download the plugin archive from: http://bitbucket.org/sunng/exailedoubancovers/downloads/">http://bitbucket.org/sunng/exailedoubancovers/downloads/</a></p>

<p>To install it, open exaile, click menu edit &gt;&gt; preference &gt;&gt; plugins, hit button "install plugin file", select "doubancovers.tgz"(if you cannot find the file, just change filter to "all files")</p>

<p>In some cases, the plugin could not be shown in the list at once, you can close the preference dialog and open it again, then active it.</p>

<p>To get album art, right click the album art icon, select "fetch cover" and wait for right cover downloaded.</p>

<p>The plugin is open sourced under GPL v2, you can grab the code from bitbucket using mercurial:
<em>hg clone http://bitbucket.org/sunng/exailedoubancovers/</em></p>

<p>Some screenshots for you:</p>

<p><img class="alignnone" src="http://farm3.static.flickr.com/2628/4074746549_2398351bef.jpg" alt="" width="500" height="414" /></p>

<p><img class="alignnone" src="http://farm4.static.flickr.com/3512/4075502050_325281d63a_o.png" alt="" width="358" height="375" /></p>
