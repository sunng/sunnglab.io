---
layout: post
title: Exaile-doubanfm-plugin 0.0.2
categories:
- ANN
tags:
- Douban
- exaile
- foss
- gtk
- opensource
- python
published: true
comments: true
---
<p>Exaile doubanfm plugin 0.0.2 预览版，目标是在Linux桌面提供豆瓣电台的完整体验。</p>

<p><strong>目前插件只能运行在Exaile 0.3.1版本上。</strong></p>

<p>0.0.2增加了豆瓣电台专用的视图：
<a href="http://www.flickr.com/photos/40741608@N08/4760493886/" title="screenshot_001 by 贝小塔, on Flickr"><img src="http://farm5.static.flickr.com/4075/4760493886_5e334726cf.jpg" width="408" height="156" alt="screenshot_001" /></a></p>

<p>相比第一版通过rating来实现豆瓣电台喜欢、跳过和删除的功能，在专用视图上有专门的按钮来操作。</p>

<p>其他细节更新：
<ul>
	<li>当剩余曲目超过15首时不再增加播放列表</li>
	<li>当播放到最后一首歌曲取新播放列表时增加重试机制</li>
	<li>修正libdbfm跳过曲目bug一个</li>
</ul></p>

<p>安装：<br />
打开Exaile Preference，Plugin页，点击按钮Install Plugin，选择doubanfm.exz即可。如果存在问题，可以执行以下命令：<br />
mv doubanfm.exz douban.tar.gz<br />
tar xf doubanfm.tar.gz<br />
mv doubanfm ~/.local/share/exaile/plugins/</p>

<p>转到doubanfm设置页面，填写用户名密码重启Exaile。
<a href="http://www.flickr.com/photos/40741608@N08/4754666940/" title="screenshot_002 by 贝小塔, on Flickr"><img src="http://farm5.static.flickr.com/4141/4754666940_b998d99c4b.jpg" width="500" height="412" alt="screenshot_002" /></a></p>

<p>打开File菜单，选择豆瓣电台频道
<a href="http://www.flickr.com/photos/40741608@N08/4754619030/" title="screenshot_001 by 贝小塔, on Flickr"><img src="http://farm5.static.flickr.com/4094/4754619030_933e00fcfa.jpg" width="500" height="359" alt="screenshot_001" /></a></p>

<p>选择曲目就可以开始播放了，选择视图中豆瓣电台视图可以切换到豆瓣电台视图。</p>

<p>项目地址：
<a href="http://github.com/sunng87/exaile-doubanfm-plugin">http://github.com/sunng87/exaile-doubanfm-plugin</a></p>

<p>下载：
<a href="http://github.com/sunng87/exaile-doubanfm-plugin/downloads">http://github.com/sunng87/exaile-doubanfm-plugin/downloads</a></p>

<p>另外，doubancovers插件也有一个针对豆瓣电台的更新可以快速获取豆瓣电台音乐的封面：
<a href="http://bitbucket.org/sunng/exailedoubancovers/downloads?highlight=9265">http://bitbucket.org/sunng/exailedoubancovers/downloads?highlight=9265</a></p>

<p>The post is brought to you by <a href="http://fedorahosted.org/lekhonee">lekhonee</a> v0.7</p>
