---
layout: post
title: OSM reporter
categories:
- 手艺
tags:
- leaflet
- OpenStreetMap
published: true
comments: true
---
<p>好久没有Weekend project了，礼拜天给一个叫作<a href="https://github.com/timlinux/osm-reporter">osm reporter</a>的小项目写了一点代码。这个小程序的功能很简单，显示指定区域里建筑物的贡献者情况。</p>

<p>我做了一些修改，增加了道路贡献情况。还有，利用自己的heatcanvas库显示用户个人编辑的分布情况。</p>

<p><a href=" http://reporter.fluv.io/?bbox=118.51638793945312,31.92943755974919,119.16183471679688,32.133175697091374&obj=highway">这里</a>有一个live demo，显示南京的编辑情况，不幸而又庆幸的是，离开南京大半年，我已经远远落后于sinopitt了。</p>

<p>这个项目的缘起：<a href="http://linfiniti.com/2012/12/holiday-openstreetmap-project-for-swellendam/">http://linfiniti.com/2012/12/holiday-openstreetmap-project-for-swellendam/</a></p>
