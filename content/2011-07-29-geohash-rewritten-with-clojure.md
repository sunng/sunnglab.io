---
layout: post
title: Geohash rewritten with Clojure
categories:
- ANN
tags:
- clojure
- geohash
- project
published: true
comments: true
---
<p>Sorry but this is another release announcement.</p>

<p>I just rewrite my <a href="https://github.com/sunng87/node-geohash" target="_blank">node-geohash</a> module with clojure. You can include it with leiningen according to the instructions shows <a href="http://clojars.org/org.clojars.sunng/geohash" target="_blank">here</a>. The project is hosted on <a href="https://bitbucket.org/sunng/clojure-geohash" target="_blank">bitbucket</a> this time. The API is rather simple, please just follow the README doc.</p>

<p>It's totally different experience to write such kind of algorithm in a functional language. You have to translate loops to recursions, also make sure they are available for tail recursion optimization. I really enjoy coding in clojure.</p>
