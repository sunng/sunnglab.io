---
layout: post
title: 'libchamplain: Map Widget of Gnome Desktop'
categories:
- 装备
tags:
- foss
- GIS
- gnome
- linux
published: true
comments: true
---
<p>续昨，虽然昨天升级到了gnome2.28，也安装了libchamplain，但是却没有发现这个库应用。Ubuntu上的Empathy似乎是没有把location support编译进去。看不到效果怎么办，好在<a href="http://www.pierlux.com/" target="_blank">libchamplain的作者</a>还提供了另一个应用。这是一个eog的插件，用于读取照片的exif信息中的gps latitude和longitude，进而将他显示在libchamplain的地图里。</p>

<p><img class="alignnone" src="http://pic.yupoo.com/classicning/2698882a6ac0/medium.jpg" alt="" width="500" height="343" /></p>

<p>安装这个插件可以follow<a href="http://live.gnome.org/EyeOfGnome/Plugins" target="_blank">官方网站上的做法</a>，当然开发包、头文件之类的东西要齐备。</p>

<p>libchamplain使用了<a href="http://www.clutter-project.org/" target="_blank">clutter</a>，在载入地图的时候还有fadein的效果。clutter是将要应用在下一代桌面gnome shell中的图形特效库。</p>

<p>插件是用C写的，可以看成是libchamplain的一个例子，作者说已经有了其他语言的binding，如此未来在gnome桌面上开发tile地图程序会变得简单。</p>
