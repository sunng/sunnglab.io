---
layout: post
title: Come back to see you
categories:
- 自话
tags: []
published: true
comments: true
---
<p>在彻查病毒之前这个网站的可访问性已经差到一定程度为此我甚至加了cron任务每天定时两次恢复文件但是怎奈病毒运行时间无常最后难免心力交瘁。</p>

<p>现在问题基本解决在可预见的将来网站的可访问性暂时不会再有问题了大家也不用担心打开本人网站的时候被执行广告代码或者重定向到什么世外桃源去了。</p>

<p>在清理病毒期间正好在<a href="http://linuxtoy.org/archives/douban-covers.html" target="_blank">linuxtoy上发了一篇文章</a>可惜因为清理病毒没有赚到点击量和知名度了。</p>
