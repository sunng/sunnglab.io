---
layout: post
title: Reset default cursor theme and web browser after installing kubuntu
categories:
- 把戏
tags:
- linux
- ubuntu
published: true
comments: true
---
<p>If you installed kubuntu parallel to ubuntu, you might find that the default browser is set to konqueror and cursor theme is oxygen which cannot modified by "gnome-default-applications-properties" and "gnome-appearance-properties".</p>

<p>To fix this, use Debian update-alternatives tool:
<em>update-alternatives --config x-www-browser</em>
<em>update-alternatives --config x-cursor-theme</em></p>

<p>All configurable options are listed at <em>/var/lib/dkpg/alternatives</em></p>
