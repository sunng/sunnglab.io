---
layout: post
title: Using Google closure library with ClojureScript
categories:
- 手艺
tags:
- clojure
- clojurescript
- javascript
published: true
comments: true
---
<p>Google closure library is shipped with ClojureScript, and could be compiled with ClojureScript into a minimized javascript file. So closure library is doubtlessly the first candidate when you are considering to use an external Javascript library in your cljs browser application.</p>

<p>However, different from clojure's interoperability with Java, ClojureScript has its own characteristics when you are interoperating with JavaScript and JavaScript based libraries.</p>

<p><strong>Clojure types are not fully compatible with JavaScript types</strong>
In ClojureScript, you can never treat a Clojure map as a JavaScript object although they have similar characteristics. You have to do some conversion before passing a clojure map to javascript functions. Matthew Gilliard made <a href="https://gist.github.com/1098417#file_ajax.cljs" target="_blank">a sample</a> of such conversion.</p>

<p><strong>JavaScript package is not Clojure namespace</strong>
This could be a common mistake for ClojureScript newbie. Actually, JavaScript doesn't have concept of "Package" or "Namespace". Many JavaScript libraries(dojo, Google Closure) made enhancement on this. ClojureScript also takes advantage of this mechanism. So before you start to coding with closure, you may browse <a href="http://closure-library.googlecode.com/svn/docs/index.html" target="_blank">closure library API document</a>, and find a module called <em>goog.net</em> which includes lots of types. Then you write this:<br />
[cc lang="clojure"]<br />
(ns myjs<br />
  (:require [goog.net :as gnet]))<br />
[/cc]</p>

<p>But compiler shows you "ERROR: JSC_MISSING_PROVIDE_ERROR. required "goog.net" namespace never<br />
provided at ... ". This is not a PATH issue. The root cause is that closure module has a lower granularity than Clojure ones. Types are often contained in their own modules. You can find closure source code in <em>clojurescript/closure/library/closure</em>. Modules are declare with <em>goog.provide</em> function. Thus, you should require this name instead of the logical module name.<br />
[cc lang="clojure"]<br />
(ns myjs<br />
  (:require [goog.net.XhrIo :as gxhr]))<br />
[/cc]</p>

<p>In addition, ClojureScript does not support 'use'.</p>

<p><strong>Just use full name for JavaScript class</strong>
For functions contains in some module, you can refer it with the clojure way:<br />
[cc lang="clojure"]<br />
(ns myjs<br />
  (:require [goog.dom :as dom]))<br />
(dom/$ "element-id")<br />
[/cc]</p>

<p>But for classes, just use the full name and ignore the module alias.<br />
[cc lang="clojure"]<br />
(ns myjs<br />
  (:require [goog.net.XhrIo :as gxhr]))<br />
(def xhr (goog.net.XhrIo.))<br />
[/cc]</p>

<p>These are basic tips before you start using Closure with ClojureScript. Leveraging on Google's closure library, you can create cross-browser JavaScript application with Clojure easily.</p>
