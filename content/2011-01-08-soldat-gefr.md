---
layout: post
title: soldat & gefr
categories:
- 手艺
tags:
- gefr
- java
- jython
- project
- python
- soldat
published: true
comments: true
---
<p>我的这一套stack正在走向完整。上次贴了一张soldat-http的图，现在基于soldat的wsgi服务器也已经有了一个基本可以运行的实现，名字叫做gefr（我的命名出处参考<a href="http://en.wikipedia.org/wiki/Rank_insignia_of_the_German_armed_forces">这里</a>）。现在gefr上已经可以跑基于bottle框架的wsgi程序了，也就是说一些基于python的web应用可能可以通过jython来运行在soldat上。为了搞定jython的环境，这几天我还花了不少时间做了jip帮我从maven仓库里自动下载依赖的jar包。</p>

<p>soldat和gefr的代码都放在我的bitbucket上：
<ul>
<li>soldat <a href="https://bitbucket.org/sunng/soldat">https://bitbucket.org/sunng/soldat</a></li>
<li>gefr <a href="https://bitbucket.org/sunng/gefr">https://bitbucket.org/sunng/gefr</a></li>
</ul></p>

<p>此外，这两个项目也分别发布到了<a href="https://oss.sonatype.org/content/repositories/snapshots/">sonatype oss仓库</a>和<a href="http://pypi.python.org/pypi/gefr">python cheese shop</a>。</p>

<p>现在还有几个问题：
<ul>
<li>soldat在读buffer的时候先获得buffer的limit，再去读相应长度的buffer有时会出现BufferUnderflowException。这个可能存在线程安全问题，现在还没发现。</li>
<li>gefr启动之后可以通过在jvisualvm里找到这个进程，但是绑定profiler之后很诡异的是gefr就不再处理请求了。</li>
<li>直接用soldat的处理http请求，吞吐量可以上万；但是在上面加上jython的gefr，再加上bottle框架，同样的功能吞吐量就剩下原来的十分之一了。就是因为没法做profile，所以还不知道时间花到哪里去了。</li>
</ul></p>

<p>简单地 announce 一下，这样我有更多的动力来继续把这两个小东西做好。</p>
