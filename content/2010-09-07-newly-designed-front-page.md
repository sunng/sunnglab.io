---
layout: post
title: Newly designed front page
categories:
- 手艺
tags:
- css
published: true
comments: true
---
<p>The last version of my home page was initialized at 2008.9. After two years, the page is completely obsolete,  some features were broken due to different reasons. (fanfou.com has been shutdown and twitter was banned).</p>

<p>So I just bring you this:
<a href="http://www.flickr.com/photos/40741608@N08/4967665544/" title="screenshot_001 by 贝小塔, on Flickr"><img src="http://farm5.static.flickr.com/4124/4967665544_1b607edbf5.jpg" width="500" height="294" alt="screenshot_001" /></a></p>

<p>The page is extremely simple, without any JavaScript or image. Some CSS3 features have been applied to it, including gradient background, css border radius (also web font will be added when I find a suitable font).</p>

<p>The photo is taken at Expo, outside the  Holland Pavilion.</p>
