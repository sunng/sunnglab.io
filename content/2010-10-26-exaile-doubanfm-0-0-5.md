---
layout: post
title: Exaile 豆瓣电台插件 0.0.5
categories:
- ANN
tags:
- exaile
- python
- ubuntu
published: true
comments: true
---
<p>趁热依旧打铁。Exaile 豆瓣电台插件 0.0.5</p>

<p><h4>Features</h4>
<ul>
<li>界面快捷键 加心f 跳过s 删除d</li>
<li>调整了按钮的顺序，和官方一致</li>
<li>增强的菜单，可以在豆瓣模式里切换频道、关闭Exaile，无须回到主界面</li>
<li><strong>推荐当前歌曲到豆瓣</strong>，可以在配置界面配置推荐语模板</li>
</ul></p>

<p><h4>Installation</h4>
<a href="http://github.com/sunng87/exaile-doubanfm-plugin/wiki/Installation">Wiki page</a></p>

<p><h4>Downloads</h4>
<a href="http://github.com/sunng87/exaile-doubanfm-plugin/downloads">Download from Github</a></p>

<p><h4>Screenshots</h4></p>

<p>Douban Mode
<a href="http://www.flickr.com/photos/40741608@N08/5117860492/" title="screenshot_002 by 贝小塔, on Flickr"><img src="http://farm2.static.flickr.com/1318/5117860492_8daa067abf.jpg" width="402" height="148" alt="screenshot_002" /></a></p>

<p>Enhanced Douban Mode Menu
<a href="http://www.flickr.com/photos/40741608@N08/5117860126/" title="screenshot_001 by 贝小塔, on Flickr"><img src="http://farm5.static.flickr.com/4106/5117860126_eb2ca09332.jpg" width="278" height="306" alt="screenshot_001" /></a></p>

<p><h4>More ?</h4>
<a href="http://sunng.info/doubantu/">豆瓣 on Ubuntu</a></p>
