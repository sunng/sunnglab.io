---
layout: post
title: What's new in slacker 0.7.0 ?
categories:
- ANN
tags:
- clojure
- slacker
published: true
comments: true
---
<p>I just released [slacker "0.7.0"] to clojars. This is the first release after my presentation on the Clojure China Meetup. <a href="https://github.com/lbt05" target="_blank">lbt05</a> contributed an ACL module to slacker, which is the most significant feature in this release. </p>

<p>The ACL module provides a simple DSL to define access rules.</p>

<p>[cc lang="clojure"]<br />
(use 'slacker.acl)<br />
(use 'slacker.server)</p>

<p>(defrules myrule<br />
  (allow ["10.60.15.*"]))</p>

<p>(start-slacker-server ...<br />
                      :acl myrule)<br />
[/cc]</p>

<p>"myrule" defines a limited access control list. Only clients from IP segment 10.60.15.* could access the slacker service.</p>

<p>And there are also minor enhancements in this release:</p>

<p><ul>
<li>Content compression, new content type :deflate-carb :deflate-json and :deflate-clj</li>
<li>In debug mode, server side stacktraces are printed on client</li>
<li>Zookeeper node path refined</li>
<li>New options in use-remote, :only and :exclude</li>
<li>Cheshire used as json library</li>
</ul></p>

<p>slacker 0.7.0 will be the last version on clojure 1.2 . As aleph 0.2.1 is coming near, we will migrate to clojure 1.3 as soon as possible. If you like to taste slacker on your 1.3 application now, there is a 0.7.1-SNAPSHOT available.</p>

<p></p>
