---
layout: post
title: New shake syntax
categories:
- ANN
tags:
- clojure
- project
published: true
comments: true
---
<p>As shake goes public, I received a lot of feedback. The top issue is about using clojure variables in shake macros. Now it has been fixed in 0.2.2. Let me show you the new syntax. </p>

<p>Using vars, local bindings in shake macros:<br />
[cc lang="clojure"]<br />
(require '[shake.core :as sh])</p>

<p>(let [x "/home/nsun"]<br />
  (sh/ls -l $x))<br />
[/cc]</p>

<p>So you have to prefix the clojure variable with a dollar sign. This is quite similar to what we did in shell programming.</p>

<p>And more interesting, you can also use a $ prefixed clojure form in shake:</p>

<p>[cc lang="clojure"]<br />
(sh/curl $(format "https://github.com/%s" "sunng87"))<br />
[/cc]</p>

<p>Thanks to Clojure macro system, it has great flexibility to manipulate symbols and code lists, making inventing new syntax much easier than other languages. Shake can be a great example in describing macro system.
</p>
