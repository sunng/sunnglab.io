---
layout: post
title: HTML5 Form in Opera 11
categories:
- 装备
tags:
- css
- html
- opera
published: true
comments: true
---
<p>严格意义上我现在已经不能算是个web guy了，不过托Web开放的特性，咱也能评论一下。最近做个小的界面，考虑到要面向未来，HTML5又提供了丰富的表单增强。这些特性中，Firefox 3.6基本上还没有支持，Chromium 8.0有少部分支持，而真正支持比较完整的是Opera 11.</p>

<p><strong>autofocus</strong>
页面载入后自动将焦点转到某个input，就如以前的onload时候xxx.focus()</p>

<p><strong>required</strong>
指定某个input为必填，提交时会有validation，在Opera11里还会给你这样的警告并且禁止提交：
<a href="http://www.flickr.com/photos/40741608@N08/5349345730/" title="Screenshot - 01122011 - 10:53:41 PM by 贝小塔, on Flickr"><img src="http://farm6.static.flickr.com/5083/5349345730_f6696fd07e.jpg" width="329" height="144" alt="Screenshot - 01122011 - 10:53:41 PM" /></a></p>

<p><strong>email / url</strong>
指定某个input的特殊类型，提交时也会有对应的格式检查：
<a href="http://www.flickr.com/photos/40741608@N08/5348736441/" title="Screenshot - 01122011 - 10:54:41 PM by 贝小塔, on Flickr"><img src="http://farm6.static.flickr.com/5081/5348736441_78accc61ac.jpg" width="344" height="140" alt="Screenshot - 01122011 - 10:54:41 PM" /></a></p>

<p>这样的提示可能满足不了你的替用户着想的产品经理，但是你清楚他帮你省了多少事情。而且，在DiveInfoHtml5里提到，iphone甚至为这个input做了优化，在软件盘上把空格键变得很小（因为输入邮件地址用不着），并且提供了一个@键；这就是工程师的用户体验啊。</p>

<p><strong>placeholder</strong>
在input里显示提示文本：
<a href="http://www.flickr.com/photos/40741608@N08/5349345562/" title="Screenshot - 01122011 - 10:52:18 PM by 贝小塔, on Flickr"><img src="http://farm6.static.flickr.com/5209/5349345562_d658665b92.jpg" width="441" height="63" alt="Screenshot - 01122011 - 10:52:18 PM" /></a></p>

<p>注意到后面的红色星号了吗。这本来是我设计的一个workaround，既然Firefox 3都还不支持required validation，那么我们通过CSS来显示一个必填的提示：<br />
[cc lang="css"]<br />
input[required="required"]:after {<br />
	content: '(*)';<br />
	color: #F00;<br />
}
[/cc]</p>

<p>结果这个伪类最后只有在Opera里实现了预期的效果，看<a href="http://stackoverflow.com/questions/2587669/css-after-pseudo-element-on-input-field">在Stackoverflow上的讨论</a>才得知input的:after和:before伪类只有Opera支持。</p>

<p>HTML5 Form这部分是一个真正务实的标准，你可以看到几乎每一个特性都是我们日常每时每刻都需要，几乎每个网站的JS都要实现的功能。标准是沟通上下游（浏览器和应用开发人员）的契约，一个真正反应相互需求的标准才是有实际价值的。</p>
