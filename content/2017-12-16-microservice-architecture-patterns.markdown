Title: 微服务设计套路
Date: 2017-12-26 15:52:00
Tags: architecture

有些名词第一次听到以为是噱头，或者根本没有在意，然后一两个月内又听到一次，再然后一周内又听到一次。三人成虎，发现真的是热门的概念了。比如 [Service mesh](https://buoyant.io/2017/04/25/whats-a-service-mesh-and-why-do-i-need-one/)，比如 sidecar。
第一次看到 sidecar 这个词是在[这本书里](http://shop.oreilly.com/product/0636920072768.do)，昨天又看到了这篇[总结性的文章 Design patterns for microservices](https://azure.microsoft.com/en-us/blog/design-patterns-for-microservices/)，都是出自微软背景的技术人员。
也许是出自微软的惯用语？

不过 Design patterns for microservices 倒是值得关注一下，介绍了 9 种微服务架构套路。

### Ambassador

[大使](https://docs.microsoft.com/en-us/azure/architecture/patterns/ambassador)。大使进程专门负责对外调用，解决一些服务可靠性、监控统计等方面的问题。也叫 service proxy（？）
大使可以作为一个 sidecar 和服务一起部署，也可以作为一个基础设施部署在宿主机上供上面的所有服务共用。

大使可以用在那些没有对微服务架构优化过的老应用上，将老应用的请求全部发到就近的大使上。大使提供一种可靠的，可控制的服务间调用。

### Anti-Corruption Layer pattern

[防腐层](https://docs.microsoft.com/en-us/azure/architecture/patterns/anti-corruption-layer)。俗话说没有什么问题不是加一层可以解决的，如果有，就再加一层。
防腐层用来专门处理新旧应用的集成，比如调用协议的不兼容，而又不想让这些确认会被抛弃的代码 ruin 新的应用。

### Backends for Frontends

[不翻译了](https://docs.microsoft.com/en-us/azure/architecture/patterns/backends-for-frontends)。这个估计会有些争议。
给每一种类型的客户端准备一个专门的后端服务，比如浏览器访问浏览器的后端，移动设备访问移动设备的后端，避免一套代码供全球导致的对前端特性的忽视。
当然和所有套路类似，也不需要教条地针对所有场景都使用这种套路，只是在确有痛点的时候找到“这么做有先例”的论据。

### Bulkhead

[隔墙](https://docs.microsoft.com/en-us/azure/architecture/patterns/bulkhead)。给每个任务分配固定的资源上限，当某个任务出现异常开始消耗资源时，能够控制资源的消耗上限。
比如，调用外部服务的线程池，到服务A响应变慢时，肯定会占用更多的线程资源，影响那些只依赖服务B的业务。如果为不同的依赖设计不同的线程池，就可以达到隔离的效果，既然挂不可避免，那么也只挂 A，不要影响 B。

### Gateway Aggregation

[网关聚合](https://docs.microsoft.com/en-us/azure/architecture/patterns/gateway-aggregation)。当应用依赖多个外部服务时，设计这么一种代理网关可以更好地并行请求后端依赖，并且把聚合的结果一次性返回给应用。
当应用访问多个服务时有明显的延迟时，一个统一的出入口可以节省调用的代价。对标准的调用协议，聚合网关也可以是一个标准的架构组件。

### Gateway Offloading

[网关卸载](https://docs.microsoft.com/en-us/azure/architecture/patterns/gateway-offloading)。在网关层面把 SSL、安全、认证等辅助任务终结掉，在应用层面保持轻量。

### Gateway Routing

[网关路由](https://docs.microsoft.com/en-us/azure/architecture/patterns/gateway-routing)。为所有微服务提供一个统一的入口网关，对外部服务隔离内部的部署、划分细节。部署时还可以通过路由规则施展一些小手段，比如蓝绿部署。
不过对网关的配置能力要求比较高，当网关后面有二十多个各色服务时，如果只是手动维护，代价和风险有点大。（说出了现状）

### Sidecar

[挂斗](https://docs.microsoft.com/en-us/azure/architecture/patterns/sidecar)。对应用中一些通用的需求，比如日志、网络请求等，把他们实现为一组独立的进程，有几方面的好处：

1. 避免对核心业务的资源争抢
2. 可重用在任意系统，不依赖特定的语言

比如 [Istio](https://istio.io/) 作为一个 sidecar 接管所有的远程调用，提供服务管控能力。

### Strangler

[扼杀模式](https://docs.microsoft.com/en-us/azure/architecture/patterns/strangler)。当新旧服务在过渡期时，根据策略将请求路由到旧服务或新服务，目标是逐步废弃遗留系统，平缓迁移到新系统。
这种机制用于平滑迁移服务，保持兼容性，甚至可回退的能力。直到所有用户都迁移到新版时，将这套机制和旧版服务一同退役下线。
