---
layout: post
title: Twitter talks about the LBS feature and implementation
categories:
- 未分类
tags: []
published: true
comments: true
---
<p>这是3月份OReilly Where 2.0会议上Twitter介绍自己LBS功能和实现的幻灯片
<a href="http://www.slideshare.net/raffikrikorian/handling-realtime-geostreams-3597425">http://www.slideshare.net/raffikrikorian/handling-realtime-geostreams-3597425</a></p>

<p><div style="width:425px" id="__ss_3597425"><strong style="display:block;margin:12px 0 4px"><a href="http://www.slideshare.net/raffikrikorian/handling-realtime-geostreams-3597425" title="Handling Real-time Geostreams">Handling Real-time Geostreams</a></strong><object id="__sse3597425" width="425" height="355"><param name="movie" value="http://static.slidesharecdn.com/swf/ssplayer2.swf?doc=real-time-geostreams-100330161823-phpapp02&stripped_title=handling-realtime-geostreams-3597425" /><param name="allowFullScreen" value="true" /><param name="allowScriptAccess" value="always" /><embed name="__sse3597425" src="http://static.slidesharecdn.com/swf/ssplayer2.swf?doc=real-time-geostreams-100330161823-phpapp02&stripped_title=handling-realtime-geostreams-3597425" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="425" height="355"></embed></object><div style="padding:5px 0 12px">View more <a href="http://www.slideshare.net/">presentations</a> from <a href="http://www.slideshare.net/raffikrikorian">Raffi Krikorian</a>.</div></div></p>

<p>都封了，越封越傻逼，封封更傻逼。</p>

<p>The post is brought to you by <a href="http://fedorahosted.org/lekhonee">lekhonee</a> v0.7</p>
