---
layout: post
title: XAuth.org & OpenAddresses.org
categories:
- 他山
tags:
- opensource
- web
published: true
comments: true
---
<p>XAuth.org 提供一个新的轻量级认证机制，他的机制可以参考：
<a href="http://xauth.org/spec/">http://xauth.org/spec/</a></p>

<p>OpenAddresses.org是上个月O'Reilly Where 2.0上发布的开放的Geocoding服务，类似OpenStreetMaps
<a href="http://openaddresses.org/">http://openaddresses.org/</a></p>
