---
layout: post
title: SQuirrel SQL Client 3发布了
categories:
- 装备
tags:
- foss
- java
- swing
- tools
published: true
comments: true
---
<p>SQuirrel SQL Client是一个用Java写的数据库客户端，用JDBC统一数据库访问接口以后，可以通过一个统一的用户界面来操作MySQL PostgreSQL MSSQL Oracle等等任何支持JDBC访问的数据库。使用起来非常方便。而且，SQuirrel SQL Client还是一个典型的Swing程序，也算是Swing的一个比较成功的应用了。</p>

<p>2.x版本的SQuirrel SQL Client一直是用JDesktopPane和JInternalFrame来组织界面。这次发布的3.0最大的变化就是改用现在更加流行的Dock方式组织窗口。</p>

<p>07年开始我一直用这个工具，后来在某处实习时，意外地发现组里以前也有人用这个工具（不过后来没有人再用了），一问，原来也是之前我们学校的同学。看来，一个学校的果然还是有一点相同，呵呵。</p>
