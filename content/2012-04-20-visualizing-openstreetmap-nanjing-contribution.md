---
layout: post
title: Visualizing OpenStreetMap Nanjing Contribution
categories:
- 把戏
tags:
- Nanjing
- OpenStreetMap
- visualization
published: true
comments: true
---
<p>早上在prismatic上看到mapbox的一篇<a href="http://mapbox.com/blog/how-to-map-contributions-openstreetmap/" target="_blank">博客</a>，介绍通过TileMill可视化OSM的贡献者，非常酷。于是我在南京的地图上也做了一个这样的可视化。</p>

<p><img src="http://i.imgur.com/U2yXK.png" alt="" /></p>

<p>一个详细的大图在<a href="http://i.imgur.com/YnULm.png" target="_blank">这里</a>。虽然只做了南京的五个主要贡献者，基本上涵盖了大部分数据。</p>

<p>图例就不专门输出了<br />
[user = 'Sunng'] { marker-fill: @magenta;} <br />
[user = 'fuwuyuan'] { marker-fill: @blue;}<br />
[user = 'sinopitt'] {marker-fill: @yellow;}<br />
[user = 'larryy'] {marker-fill: @green;}<br />
[user = 'zhengz'] {marker-fill: @red;}</p>

<p>MapBox家的东西真的非常酷，这家的技术以nodejs为主，围绕osm开发了不少产品。最近比较大的新闻，比如4sq转到osm上，其实就是转到这家的osm服务上。有兴趣你可以关注一下！
<img src="http://i.imgur.com/Hum2r.png" alt="" /></p>
