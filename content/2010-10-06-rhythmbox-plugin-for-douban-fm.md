---
layout: post
title: Rhythmbox plugin for Douban FM
categories:
- 他山
tags:
- Douban
- ubuntu
published: true
comments: true
---
<p><a href="http://code.google.com/p/dbfmplugin/">Rhythmbox 豆瓣电台插件</a>是由cdredfox基于<a href="http://github.com/sunng87/exaile-doubanfm-plugin/blob/master/libdoubanfm.py">libdoubanfm</a>开发的另一个Linux桌面豆瓣电台客户端。如果您是Rhythmbox的忠实用户，现在您有一个新的选择了。</p>

<p><image src="http://hnyangfei.appspot.com/media/aglobnlhbmdmZWlyDQsSBU1lZGlhGImbBAw/%E8%96%9B%E5%87%AF%E7%90%AA%20-%20%E5%A4%8D%E5%88%BB%E5%9B%9E%E5%BF%86_007.png" width="500" /></p>
