---
layout: post
title: Sunng.info in Mosaic
categories:
- 留影
tags:
- web
published: true
comments: true
---
<p>Mosaic:
<a href="http://en.wikipedia.org/wiki/Mosaic_browser">http://en.wikipedia.org/wiki/Mosaic_browser</a></p>

<p><a href="http://www.flickr.com/photos/40741608@N08/4420674473/" title="mosaic by 贝小塔, on Flickr"><img src="http://farm5.static.flickr.com/4058/4420674473_e14d48020e.jpg" width="457" height="500" alt="mosaic" /></a></p>

<p>这么一看，什么标准通通扯淡，还是图片管用，什么浏览器都可以显示。</p>
