---
layout: post
title: Keep-Alive
categories:
- 自话
tags:
- Life
published: true
comments: true
---
<p>史无前例，上个月只写了一篇blog，再加上网站遭遇认证很多地方都访问不了，故而有必要keep alive一下，该reconnect的请自觉重连。</p>

<p>从家里出来，再次跑到北京来，就做好了艰苦卓绝的思想准备。现在的工作节奏，和2010年春天在盛大在线的时候差不多。那时一腔热血，也没什么经验，做什么事都心里没底。当然也是那段时间学到的东西最多，人生就是这样，熬过一个最困难的时候，总会有巨大的收获（如果没有收获说明还没熬过去，下同）。时过境迁，现在的情况有了巨大的不同，比如看<a href="http://tech.sina.com.cn/i/2012-05-26/03317171784.shtml" target="_blank">这里</a>。曾经没日没夜的奋斗的项目，最后烟消云散仿佛都不存在过。</p>

<p>人最大的瓶颈是时间，时间不可逆，挥霍掉就没有了。为了能有口饭吃，大部分时间还得拿自己的时间去换别人的金钱。如果这个时间能有合理的回报或是各取所需最好，如果这个工作自己也认同倒也不错。如果是纯粹的时间换金钱（所谓工作毕竟是工作），那就难免悲伤了。所以看到<a href="http://www.gsb.stanford.edu/news/research/aaker_happiness_2011.html" target="_blank">这样</a>一篇文章，还是觉得你很有必要快速浏览一下：
<ul>
	<li>Spend time with the right people. </li>
	<li>Spend time on the right activities. </li>
	<li>Enjoy experiences without spending time actually doing them. </li>
	<li>Expand your time. </li>
	<li>Be aware that happiness changes over time.</li>
</ul>
时间自由是人生最高自由。为了合理的利用时间，请关掉你正在折腾配置文件的emacs。</p>

<p>做了一个brogrammer的<a href="http://www.aqee.net/docs/Quiz-Are-You-a-Brogrammer/" target="_blank">测验</a>，和公司同事一起做的。结果得分-105，算是标准nerd了，在公司也是仅次于老板的-120。这个分数基本让人满意，对于那些戴墨镜编程的brogrammer，我想说的是，我的emacs一直是深色背景。</p>

<p></p>
