---
layout: post
title: Moving to ArchLinux
categories:
- 装备
tags:
- ArchLinux
- linux
published: true
comments: true
---
<p><a href="http://www.flickr.com/photos/40741608@N08/6198252182/" title="DSC_0004 by 贝小塔, on Flickr"><img src="http://farm7.static.flickr.com/6022/6198252182_656282b489.jpg" width="335" height="500" alt="DSC_0004" /></a>
这是用新入的Nikon 55-300mm的长焦镜头300端排的远处的塔吊，它与本文没有直接关系。如果实在要计算间接关系，它是我等待fedora无尽的启动时间里消磨时光的手段之一。</p>

<p>这周开始fedora彻底崩溃了，现在在默认的run-level下NetworkManager根本无法启动，次次3分钟超时。还由于不明的原因，以越来越大的概率，gdm会僵死在启动启动之前。如果切换到terminal，输入完用户名居然连password都不会prompt出来。有时甚至在runlevel3里都无法启动。本想忍下去等到下个月f16发布，但是今天晚上已经彻底无法进入系统了，算了，其实也就是挥挥刀的事情。</p>

<p>Ubuntu的回头路是走不得的，索性切换到了Arch，从此享受rolling release，不再在每年的4月10月里蠢蠢欲动惊慌失措。重装的经验和上次一样：看准分区，/home留着/干掉；在创建用户的时候看准原先用户的uid，直接用这个id创建新用户，这样$HOME自动就归属新用户了。Arch仓库里的东西甚至要比fedora还多，没有那么多洁癖，甚至Skype和IDEA的社区版都直接进了仓库。我现在年纪大了，能从仓库安装我是不会自己再去下载了。</p>

<p>其实Arch也是老朋友了，直不过以前一直把它憋在VirtualBox里，现在它从VBox里爬出来了，Ubuntu和Windows这些当年的host们被踹进去乖乖作guest了，翻身农奴这就起来把歌唱。</p>

<p></p>
