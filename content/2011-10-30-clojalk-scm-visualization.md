---
layout: post
title: Clojalk SCM Visualization
categories:
- 装备
tags:
- clojalk
- visualization
published: true
comments: true
---
<p>最近有一个小工具非常流行（如果我没有火星的话），<a href="http://code.google.com/p/gource/" target="_blank">gource</a>，可以将你的代码历史可视化出来。<a href="http://www.youtube.com/watch?v=ELSsGXGHfZM" target="_blank">这里</a>有reddit的代码历史，fogus也把写the joy of clojure做成了<a href="http://blog.fogus.me/2011/10/27/joy-of-clojure-the-movie/" target="_blank">这样</a>的视频。</p>

<p>凑个热闹，来看看我的<a href="https://github.com/sunng87/clojalk">clojalk</a>项目可视化
<embed src="http://player.youku.com/player.php/sid/XMzE3NzU4NjE2/v.swf" quality="high" width="480" height="400" align="middle" allowscriptaccess="sameDomain" allowfullscreen="true" type="application/x-shockwave-flash"></embed>
And the video link on <a href="http://vimeo.com/31328528" target="_blank">vimeo</a>.</p>

<p>除了一名contributor，只有一个commiter。又是一幕Forever alone。
<img src="http://i.imgur.com/FXcNw.png" alt="forever alone" /></p>

<p>PS：<br />
gource上的wiki里，ffmpeg如果报错File for preset 'slow' not found的话，去掉ffmpeg的-vpre slow就OK了。
</p>
