---
layout: post
title: All new SUNNG.INFO
categories:
- 装备
tags:
- blog
- web
published: true
comments: true
---
<p>今早短路，在godaddy上注册了我的新域名sunng.info。更换域名主要出于几方面的考虑：
<ol>
	<li>新域名1年的注册费用相当于原先的一半；</li>
	<li>众所周知的原因，域名放在国内存在一定的风险；</li>
	<li>原先域名过长不方便记忆和交流；</li>
	<li>.com后缀和我一穷二白地状况不符</li>
	<li>classicning.com曾被加入stopbadware.org，至今没有从twitter的malcious数据库里移除</li>
</ol>
原先的classicning.com会在今年6月到期，如果没有意外情况我不准备续费了，届时这个使用了四年的域名就要废弃了（当时客服大嫂还在电话里娇嗔地问我为啥不续十年呢，你说为啥呢）。</p>

<p>麻烦大家利用这半年的时间逐步改变习惯，逐步用sunng.info了。</p>
