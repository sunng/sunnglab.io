---
layout: post
title: geb for browser functional testing
categories:
- 装备
tags:
- groovy
- Testing
- web
published: true
comments: true
---
<p>虽然现在不做前段了，但是发现好的工具还是很兴奋。今天在twitter上看到Grails in Action的作者 @pledbrook 转了一个geb 0.4的消息，顺带看了一下这个工具<br />
http://geb.codehaus.org</p>

<p>geb项目旨在创造一套groovy dsl帮助人们进行webapp的functional test。它是对selenium的封装，举例：<br />
[cc lang="groovy"]<br />
@Grapes([<br />
    @Grab('org.seleniumhq.selenium:selenium-firefox-driver:latest.release'),<br />
    @Grab('org.codehaus.geb:geb-core:latest.release')<br />
])<br />
import geb.*</p>

<p>println "Dependencies downloaded, ready for testing"<br />
Browser.drive('http://sunng.info:8000/Pacajus'){<br />
    assert title== 'Pacajus'</p>

<p>    assert $("p", 3).text() == 'Population: 41558'<br />
}
println "Tested, bye"<br />
[/cc]</p>

<p>打开页面，执行断言。如果断言失败，driver方法会报null：<br />
Caught: geb.error.DriveException: null</p>

<p>只要在命令行用groovy执行即可，grapes会搞定依赖关系。</p>

<p>很方便吧，文档上说还可以跟grails / junit等等集成，快去看看吧
<a href="http://geb.codehaus.org/manual/latest/index.html">http://geb.codehaus.org/manual/latest/index.html</a></p>

<p>The post is brought to you by <a href="http://fedorahosted.org/lekhonee">lekhonee</a> v0.7</p>
