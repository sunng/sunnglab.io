---
layout: post
title: Time Tracking and Task Management with TaskCoach
categories:
- 装备
tags:
- foss
published: true
comments: true
---
<p><a href="http://www.taskcoach.org/" target="_blank">TaskCoach</a>其实是老朋友了，wxpython的，跨平台，最初只是TODO LIST，后来又加进了Time Tracking的功能。早先给<a href="http://blog.frank05.net/?p=152" target="_blank">frank05</a>推荐过，不过在学校的时候，在电脑上的时间往往是支离破碎的，没有什么tracking的价值。</p>

<p>现在不同了，工作以后每天在电脑前面坐至少8个小时，如果没有点控制，时间一样是像以前一样莫名其妙地溜走了。去年刚开始实习的时候，我大概有个习惯是把每天的TODO写在本子上，一来当时清楚，二来时间长了也有数。今年工作以后一开始也是这样，但是时间长了发现只用笔来记录功能还是太有限，比如事情做了一半pending了，经常是画个圈，但是时间长了还是忘掉了。而进一步想做Time Tracking就更麻烦了。</p>

<p>上周想了一下确实需要个工具，这周一开始就重新开始在开发用的Windows机器上用TaskCoach了。有将近一年的时间没关注这个软件，版本号虽然没有什么大的突破，不过用起来倒是更顺手了。现在可以最小化到系统栏，开机自动启动。本身的功能除了支持多层的Task管理，Time Tracking，还可以顺手记一些Notes，另外还有定时的事件提醒，也不会错过总也想不起来的培训了。</p>

<p>推荐给大家在Windows上用。</p>
