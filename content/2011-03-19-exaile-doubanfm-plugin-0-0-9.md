---
layout: post
title: Exaile DoubanFM Plugin 0.0.9
categories:
- ANN
tags:
- Douban
- exaile
published: true
comments: true
---
<p>新版本的Exaile豆瓣电台插件。这个版本的变更：
<ul>
<li>新增分享菜单，分享当前音轨到豆瓣、新浪、开心001、人人</li>
<li>改进了推荐方式，采用豆瓣电台的永久链接</li>
<li>修改了0.0.8以来存在的登录bug</li>
<li>修改了0.0.8以来存在的加载播放列表bug</li>
</ul></p>

<p><a href="http://www.flickr.com/photos/40741608@N08/5539013314/" title="Doubanfm Exaile new by 贝小塔, on Flickr"><img src="http://farm6.static.flickr.com/5092/5539013314_be4d6ff6ee.jpg" width="500" height="243" alt="Doubanfm Exaile new" /></a></p>

<p>从github获得新版本：
<a href="https://github.com/sunng87/exaile-doubanfm-plugin/downloads">https://github.com/sunng87/exaile-doubanfm-plugin/downloads</a></p>
