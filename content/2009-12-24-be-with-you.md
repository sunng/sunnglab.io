---
layout: post
title: Be with you
categories:
- 自话
tags:
- Life
published: true
comments: true
---
<p>今天下班骑车到高斯路时，突然路边奔出一只小狗跟着我的自行车一起跑。小狗挺干净，出现在这种工业区人不多的路上倒是挺让人诧异。我开始放慢速度，让它跟着我不至于太辛苦。可是它还足够顽皮，一会跑在左边，一会在右边，好像是知道今天过节一样，欢实得很。不过我对路中间不时经过的汽车有所顾虑，有意得向路边靠，打算把它挤到路涯边，这样安全一些。</p>

<p>大约是嫌我速度太慢了，一会后面又上来一个骑自行车的人，小狗顿时起了劲，蹦达蹦达地追了上去。那速度大概是它向往的吧，我在后面看着，直到他们一前一后，过了马路，我从另一个方向回家了。</p>
