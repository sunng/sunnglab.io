---
layout: post
title: Visualize Reddit upvotes by subreddit
categories:
- 把戏
tags:
- reddit
- visualization
published: true
comments: true
---
<p>这是我reddit上所有的upvote在各个subreddit上的分布情况，这个情况还是可以说明我是个普通青年。
<img src="http://i.imgur.com/jfXyK.png" alt="Reddit upvotes visualization" width="500px" /></p>

<p>排在前几位的分别是
<ul>
	<li>Programming</li>
	<li>Linux</li>
	<li>Python</li>
	<li>f7u12</li>
	<li>Clojure</li>
	<li>Ubuntu</li>
</ul></p>

<p>如果你还不了解什么是Reddit：Reddit是一个巨大的社会书签+论坛网站，他的频道叫做subreddit，每个频道有一个相应的主题，涵盖了从IT技术到新闻到生活的各个角落。</p>

<p>这些数据是通过下面的Clojure程序获得（使用reddit.clj库），并通过jfreechart展现出来的。
<script src="https://gist.github.com/1315572.js"> </script>
</p>
