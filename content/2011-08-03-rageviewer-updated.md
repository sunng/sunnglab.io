---
layout: post
title: RageViewer updated
categories:
- ANN
tags:
- clojure
- cloudfoundry
- project
published: true
comments: true
---
<p>Weekend project "RageViewer"最近新增了一些功能：
<ul>
	<li>界面更新</li>
	<li>Rages现在持久化到redis中，支持permalink</li>
	<li>程序可以直接部署到cloudfoundry上</li></ul></p>

<p>RageViewer现在也有一个cloudfoundry上部署的版本：
<a href="http://rageviewer.cloudfoundry.com/index.html" target="_blank">http://rageviewer.cloudfoundry.com/index.html</a></p>

<p>对本地开发、调试，现在需要在本机上启动一个监听默认端口的redis，执行<em>lein ring server</em>即可。对部署在cloudfoundry的情况，受益于cloudfoundry基于环境变量的数据库配置机制，不需要做任何额外的配置和修改：执行<em>lein ring uberwar && vmc update your-app-name</em> 即可。</p>
