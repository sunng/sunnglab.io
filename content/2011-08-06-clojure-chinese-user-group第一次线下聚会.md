---
layout: post
title: Clojure Chinese User Group第一次线下聚会
categories:
- 当时
tags:
- clojure
published: true
comments: true
---
<p>今天下午赶到上海参加了clojure中文用户的第一次线下聚会，见到了国内clojure的的主要用户。感谢一直在努力组织这次活动的朋友，还有提供场地的朋友。</p>

<p>这是我关于clojure构建工具、生命周期管理的slides</p>

<p><div style="width:425px" id="__ss_8785604"> <strong style="display:block;margin:12px 0 4px"><a href="http://www.slideshare.net/sunng87/clojure-cnclojuremeetup" title="Clojure cnclojure-meetup" target="_blank">Clojure cnclojure-meetup</a></strong> <iframe src="http://www.slideshare.net/slideshow/embed_code/8785604" marginwidth="0" marginheight="0" frameborder="0" height="355" scrolling="no" width="425"></iframe> <div style="padding:5px 0 12px"> View more <a href="http://www.slideshare.net/" target="_blank">presentations</a> from <a href="http://www.slideshare.net/sunng87" target="_blank">sunng87</a> </div> </div></p>

<p>欢迎对clojure感兴趣的朋友加入<a href="http://cnlojure.org/" target="_blank">中文用户邮件列表</a>，希望这样的聚会活动可以长期的办下去。<br /><br /><div class="zemanta-pixie"><img class="zemanta-pixie-img" alt="" src="http://img.zemanta.com/pixy.gif?x-id=4f567446-b5a0-8664-be07-796569d5b7c1" /></div></p>
