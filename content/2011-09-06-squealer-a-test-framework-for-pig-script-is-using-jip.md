---
layout: post
title: Squealer (a test framework for Pig script) is using jip
categories:
- ANN
tags:
- jip
- jython
- opensource
published: true
comments: true
---
<p><a href="https://bitbucket.org/markroddy/squealer/overview">Squealer</a> is a framework written in Jython to test your Apache Pig scripts, by <a href="http://dibon.homelinux.org/">Mark Roddy</a>. It's now using <a href="http://pypi.python.org/pypi/jip">jip</a> to resolve Java dependencies. On huge dependencies of Hadoop, jip could be great helpful to setup Squealer.</p>

<p>To get started, it is recommended to create a standalone Jython environment for Squealer:
<em>virtualenv -p /usr/local/bin/jython --no-site-packages squealer-env</em></p>

<p>Activate the environment
<em>cd squealer-env</em>
<em>. bin/activate</em></p>

<p>Install jip with pip
<em>pip install jip</em></p>

<p>Download squealer from bitbucket project page, extract it to somewhere. Install it with jip:
<em>jython setup.py install</em></p>

<p>Dependencies will be downloaded from Maven Central. You just wait for it to finish.</p>

<p>Start a Jython interpreter with '<em>jython-all</em>' and now you can import squealer.
</p>
