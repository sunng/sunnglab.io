---
layout: post
title: Fix ogg/oga not play in Firefox 3.6
categories:
- 把戏
tags:
- apache
- firefox
- html
published: true
comments: true
---
<p>Native audio support was introduced in since Firefox 3.5 . Ogg is one of the media format supported by Firefox. However, sometimes you may find it doesn't work even if you set the right source path.  And you just check the network status of audio element:</p>

<p><i>document.getElementsByTagName("audio")[0].networkState</i></p>

<p>Then you get the constant of a 4, which is HTMLMediaElement.NETWORK_NO_SOURCE.</p>

<p>This is because firefox checks the Content-Type header to make sure it's a media file. (Webkit based browsers don't have this restriction.) However, ogg format is not configured on most http servers. You can check the content type by:
<i>curl -I &lt;url-to-media-file&gt;</i></p>

<p>Take Apache as example, you can add following content to your configuration file:
<i>AddType audio/ogg .oga</i>
<i>AddType video/ogg .ogv .ogg</i></p>

<p>For more, check this article:
<a href="https://developer.mozilla.org/en/Configuring_servers_for_Ogg_media">https://developer.mozilla.org/en/Configuring_servers_for_Ogg_media</a></p>
