---
layout: post
title: Slacker Cluster
categories:
- ANN
tags:
- clojure
- project
- slacker
published: true
comments: true
---
<p>Cluster support is one of the big thing in slacker 0.6.x. Cluster enables high-availability and load balancing on slacker client and server. </p>

<p>Slacker cluster has a centralized registry, a zookeeper node, stores information of all the namespaces and servers instances in the cluster. Once a client declared remote functions, by calling `defn-remote` or `use-remote`, it reads all available servers offering that namespace from the registry and create connection to each of them. We the user issues a request, the client randomly pick up a connection from them. So the load is eventually distributed to every instance of slacker servers. And thanks to zookeeper's notification feature, the client watches certain znode. It will be notified when 1. a connected server goes offline 2. a new server serving required namespace added into the cluster. Thus you don't have to change client code or restart client when server changes. </p>

<p>To start a slacker server and add it to a cluster, you have to provide cluster information using the new <strong>:cluster</strong> option:<br />
[cc lang="clojure"]<br />
(start-slacker-server (the-ns 'slacker.example.api)<br />
                      2104<br />
                      :cluster {:zk "127.0.0.1:2181"<br />
                                :name "example-cluster"})<br />
[/cc]</p>

<p><ul>
	<li>:zk is the address of zookeeper node</li>
	<li>:name is a znode qualified string, to identify the cluster</li>
</ul></p>

<p>On the client side, it's important to use APIs from `slacker.client.cluster` instead of `slacker.client`:</p>

<p>[cc lang="clojure"]<br />
(use 'slacker.client.cluster)<br />
;; arguments: cluster-name, zookeeper address<br />
(def sc (clustered-slackerc "example-cluster" "127.0.0.1:2181"))<br />
(use-remote 'sc 'slacker.example.api)</p>

<p>;; call the function from a random server<br />
(timestamp)<br />
[/cc]</p>

<p>If all servers provide 'slacker.example.api go offline, slacker client will raise a "not-found" exception.</p>

<p>Slacker cluster is also designed with simple and clean in mind. You don't have to change you business code to make it remote or cluster. Everything is transparent and non-invasive. Enjoy it.</p>

<p></p>
