---
layout: post
title: 'dosync+: dosync with event listener'
categories:
- 手艺
tags:
- clojure
published: true
comments: true
---
<p>Discussed in clojure-cn mailing list, we come up with an extensible dosync block with event listener: on-start, on-retry and on-committed.</p>

<p><script src="https://gist.github.com/1898383.js?file=dosync-plus.clj"></script></p>

<p>With these extension points, you can bind STM monitor to a transaction. And I believe there will more advanced use cases on this.</p>

<p><script src="https://gist.github.com/1898383.js?file=main.clj"></script></p>

<p></p>
