---
layout: post
title: We are using Hudson for continuous integration
categories:
- 装备
tags:
- CI
- dev
- hudson
- java
- maven
- software
- tool
published: true
comments: true
---
<p>经历了持续两周的人肉集成，今天上午抢得一台Linux机器，终于尝试用hudson来替代人肉构建。</p>

<p>Hudson的安装和配置远比想象的简单，只要下载发布的war包，在相应的目录执行<br />
nohup java -jar hudson.war > hudson.log 2>&1 &<br />
即可启动到后台</p>

<p>hudson的web图形界面可以胜任几乎全部工作。我们主要使用maven来构建项目，hudson提供了非常强大的功能：邮件提醒（通过插件支持twitter/jabber/irc提醒）；自动构建，除了基本的定时构建以外，hudson还会自动解析其管理的项目之间的依赖关系，从而实现级联的构建，这个功能非常震撼。</p>

<p>刚刚上手以后我还安装了两个插件。一个是build-timeout插件，可以之间一次构建的超时时间：我们的项目中有老大写的交互式的maven配置插件，一旦这个插件在自动构建时运行会阻塞构建的流程。另一个是scp发布插件，可以自动scp一个文件到远程服务器上，我用这个插件来把构建版本发布到运行环境中，只要在适当的时候重启一下运行环境的服务器就可以实现部署了。不过scp插件由于上游依赖的问题貌似不支持putty生成的privatekey，这是暂时的美中不足。</p>

<p>配置了整整一天，终于有了CI工具，我就彻底解放出来可以做其他事了。</p>
