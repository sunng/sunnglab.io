---
layout: post
title: Douban on Gwibber
categories:
- 装备
tags:
- Douban
- foss
- gnome
- gtk
- gwibber
- python
- ubuntu
published: true
comments: true
---
<p>Now it's possible to use Gwibber as a desktop Douban.</p>

<p>Setup a douban client in the account dialog:
<a href="http://www.flickr.com/photos/40741608@N08/5053311735/" title="GwibberDoubanSetup by 贝小塔, on Flickr"><img src="http://farm5.static.flickr.com/4130/5053311735_5fc69747a5.jpg" width="500" height="259" alt="GwibberDoubanSetup" /></a></p>

<p>Authorize Gwibber to access private data on Douban (OAuth):
<a href="http://www.flickr.com/photos/40741608@N08/5053311377/" title="DoubanOAuthAuthorize by 贝小塔, on Flickr"><img src="http://farm5.static.flickr.com/4147/5053311377_43b0a4788e.jpg" width="500" height="394" alt="DoubanOAuthAuthorize" /></a></p>

<p>Display Douban activities on Gwibber timeline:
<a href="http://www.flickr.com/photos/40741608@N08/5053311635/" title="GwibberDouban by 贝小塔, on Flickr"><img src="http://farm5.static.flickr.com/4103/5053311635_ab6716100b.jpg" width="422" height="500" alt="GwibberDouban" /></a></p>

<p>The code has been uploaded to bitbucket:
<a href="https://bitbucket.org/sunng/gwibber-douban/overview">https://bitbucket.org/sunng/gwibber-douban/overview</a></p>

<p>I am currently working on an installation script.</p>
