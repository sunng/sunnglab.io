---
layout: post
title: Top Github users in China
categories:
- 把戏
tags:
- github
published: true
comments: true
---
<p>有感于<a href="http://sofish.de/file/demo/github/" target="_blank">这个版本</a>的github中国用户排名，我觉得单纯根据用户的followers数量不能完全说明问题。因此，需要改进一下排名的分数，加入项目Watchers的数量，这样可以让优质项目多的用户排名到前面。</p>

<p>这个策略是：followers + 1.5 * watchers + 2 * forks</p>

<p>得到的排名如下：
<script src="https://gist.github.com/3141146.js?file=results"></script></p>

<p>另外，这前100名用户里，语言使用的情况如下：<br />
'JavaScript': 80<br />
'Ruby': 52<br />
'Python': 46<br />
'C': 34<br />
'C++': 30<br />
'Java': 28<br />
'VimL': 28<br />
'Shell': 24<br />
'Objective-C': 24<br />
'PHP': 21<br />
'CoffeeScript': 13<br />
'Perl': 10<br />
'Erlang': 9<br />
'Emacs Lisp': 9<br />
'Lua': 6<br />
'Haskell': 5<br />
'Clojure': 5<br />
'Go': 5<br />
'ActionScript': 4<br />
'C#': 3<br />
'Scheme': 2<br />
'Common Lisp': 2<br />
'Elixir': 2<br />
'Scala': 1<br />
'Objective-J': 1<br />
'Vala': 1<br />
'Nemerle': 1</p>

<p>明天我会补充一下城市的排名。</p>

<p></p>
