---
layout: post
title: Clojure-Control plugin for leiningen
categories:
- ANN
tags:
- clojure
- lein
- project
published: true
comments: true
---
<p><a href="https://github.com/killme2008/clojure-control/" target="_blank">Clojure-Control</a> is the clojure port of node-control, developed by <a href="http://www.blogjava.net/killme2008/" target="_blank">killme2008</a>. Clojure-Control allows you to define clusters and tasks in a DSL and execute them with simple command.</p>

<p><a href="https://github.com/sunng87/lein-control" target="_blank">Lein-control</a> is a leiningen(clojure build tool) plugin, with which you can integrate Clojure-Control into your build tasks. It could be helpful to execute deployment tasks on several machines.</p>

<p>To use lein-control, simply add this to your project.clj . And download dependencies with <em>lein deps</em>.<br />
:dev-dependencies [[lein-control "0.1.0"]]</p>

<p>Create a sample control file in your project home:
<em>lein control init</em></p>

<p>Check your cluster configuration:
<em>lein control show cluster-name</em></p>

<p>Execute tasks against particular cluster:
<em>lein control run cluster-name task-name</em></p>

<p>Enjoy your deployment !</p>
