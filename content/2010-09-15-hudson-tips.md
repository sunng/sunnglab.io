---
layout: post
title: Hudson tips
categories:
- 装备
tags:
- hudson
- java
- maven
published: true
comments: true
---
<p>Hudson的项目有三种状态，分别是failed, Success, Unstable。当单元测试未能通过时，Hudson不会fail掉整个build而是设置为ubstable，并且继续执行post build scripts和actions。这就为集成的版本带来了一些不可知因素。取消这个设置，可以通过在maven options中添加一个 <i>-Dmaven.test.failure.ignore=false</i>。或者在全局设置，manage hudson -> configure hudson -> Global MAVEN_OPTS。这个方法来自：<a href="http://stackoverflow.com/questions/1004540/fail-hudson-build-on-single-unit-test-failure">http://stackoverflow.com/questions/1004540/fail-hudson-build-on-single-unit-test-failure</a></p>

<p>此外，我们借助hudson来自动完成开发环境的部署。从hudson的插件列表中安装ssh plugin和scp plugin。对打包项目新增一个post build action，使用SCP插件把打包生成的压缩文件上传到开发机上。本想同时配置一个build script，在开发环境机器上执行一个自动化部署脚本，但是使用发现ssh plugin的操作居然先于scp操作，而且这个顺序无法配置！不过不要紧，新建一个freestyle项目，项目只利用ssh插件运行远程脚本，再在打包项目配置中新增一个post build action -> build other project，填写前者的项目名，使之成为打包项目的downstream project即可。</p>

<p>The post is brought to you by <a href="http://fedorahosted.org/lekhonee">lekhonee</a> v0.7</p>
