---
layout: post
title: 'ActiveMQ: UnknownHostException on startup'
categories:
- 手艺
tags:
- ActiveMQ
- java
- JMS
- linux
published: true
comments: true
---
<p>在ArchLinux上使用ActiveMQ，执行bin/activemq，报错UnknownHostException，Transport Connection无法建立，可以取到/etc/ec.conf中设置的hostname（默认myhost）</p>

<p>解决方法，编辑/etc/hosts，添加127.0.0.1 myhost myhost。再次启动即可。Ubuntu上hosts自动把计算机名解析到127.0.0.1，ArchLinux上需要你手动做这件事了。</p>
