---
layout: post
title: New composite based captcha image
categories:
- ANN
tags:
- captcha
- java
- project
- Yan
published: true
comments: true
---
<p>recaptcha的验证码新增了alpha composite的新机制取代干扰线，今天用了一些时间在<a href="http://bitbucket.org/sunng/yan/" target="_blank">YAN</a>上也实现了这种绘图机制。</p>

<p><a title="30a2512d899641a8ab79a7c86946ff71 by 贝小塔, on Flickr" href="http://www.flickr.com/photos/40741608@N08/4308533479/"><img src="http://farm3.static.flickr.com/2752/4308533479_42337f4525_o.png" alt="30a2512d899641a8ab79a7c86946ff71" width="300" height="100" /></a>
<a title="f03ec596b91b4ce985c5b5af4a79e961 by 贝小塔, on Flickr" href="http://www.flickr.com/photos/40741608@N08/4308533483/"><img src="http://farm5.static.flickr.com/4068/4308533483_a9e687b74c_o.png" alt="f03ec596b91b4ce985c5b5af4a79e961" width="300" height="100" /></a></p>

<p>使用Java2D的AlphaComposite实现，选用的Rule为alpha 1.0的SrcOut，即通过公式</p>

<p>Ar = As * (1 – Ad )<br />
Cr = Cs * (1 – Ad )<br />
用语言描述就是叠加区域的透明度为0. 使用这种机制必须采用BufferedImage.TYPE_INT_ARGB的图像，并且输出支持alpha通道的格式。</p>
