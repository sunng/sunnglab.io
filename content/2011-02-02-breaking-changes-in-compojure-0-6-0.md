---
layout: post
title: Breaking changes in compojure 0.6.0
categories:
- 手艺
tags:
- clojure
- compojure
- web
published: true
comments: true
---
<p>在compojure 0.6.0里，默认的middleware被移除了。因为还没有正式发布，所以网上几乎没有相关的文档说明，而0.5.x的例子已经没能正常工作了。</p>

<p>[cc lang="clojure"]<br />
;...<br />
(defroutes root<br />
    (GET "/" {params :params} (str params)))</p>

<p>(run-jetty root {:port 8080}))<br />
[/cc]</p>

<p>这样在0.5.x中可以正确运行的代码，在0.6.0中params变成了空的map。</p>

<p>在0.6.0中，compojure引入了一个新的ns叫做compojure.handler，其中包含两个function， api和site，它们包含了一些默认的middleware，适合相应的开发场景。为了让代码能够工作，在新版本中：<br />
[cc lang="clojure"]<br />
;...<br />
(use 'compojure.handler)<br />
(defroutes root<br />
    (GET "/" {params :params} (str params)))</p>

<p>(def app (site root))</p>

<p>(run-jetty root {:port 8080}))<br />
[/cc]</p>

<p>详细可以在这里找到
<a href="https://groups.google.com/group/compojure/browse_thread/thread/4f8574d808ddf53e">https://groups.google.com/group/compojure/browse_thread/thread/4f8574d808ddf53e</a></p>
