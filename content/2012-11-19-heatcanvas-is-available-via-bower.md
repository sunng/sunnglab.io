---
layout: post
title: heatcanvas is available via bower
categories:
- ANN
tags:
- heatcanvas
- javascript
published: true
comments: true
---
<p>The heat map toolkit, heatcanvas, is now available via twitter's package management tool, bower.</p>

<p>You can now install heatcanvas with a single command:</p>

<p>bower install heatcanvas</p>

<p>Enjoy this library.</p>
