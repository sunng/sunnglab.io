---
layout: post
title: 听豆瓣电台的最佳姿势
categories:
- 装备
tags:
- Douban
- firefox
- foss
published: true
comments: true
---
<p>豆瓣电台，如果你是豆瓣用户我不多说了；如果不是豆瓣用户，嗯，算了，您换台吧。</p>

<p>总算找到Prism的用处了。如果你在用豆瓣电台，并且你使用Firefox，那么只要安装Prism，就可以把豆瓣电台变成一个任意大小的窗口，和你的多tab的浏览器分离。如果非常巧合你是一位少数派的Windows用户，嗯，Prism在Windows上还可以最小化到系统栏。</p>

<p>安装Prism扩展，嗯，最好确定你的Firefox是最新的（这个下雨的晚上最新的是3.5.5），当然也别新到3.6去。OK没有问题的话，就猛击下面这个链接：
<a href="https://addons.mozilla.org/en-US/firefox/addon/6665">https://addons.mozilla.org/en-US/firefox/addon/6665</a></p>

<p>注册都不需要了，直接点安装，然后等着重开Firefox就行了。安装完成，可以看到Tools菜单下面多了一个"Convert Website to application..." 这就是Prism，点开设置如下参数：</p>

<p><img class="alignnone" src="http://farm3.static.flickr.com/2489/4105923552_6c51a64048.jpg" alt="" width="415" height="500" /></p>

<p>Windows用户的选项会多一些，记得勾上那个show in the notification area（如果是这个名字的话），确定即可。</p>

<p>Windows用户（注意升级到3.5.5的Firefox）这时直接点桌面上的图标就可以了。由于cookie不能共享，你需要重登录一下，有必要的话最好选上“记住我”。等豆瓣电台的flash播放器出现的时候，把窗口拖到合适的大小，Prism会记住每次正常关闭时的窗口大小，下次打开就那么大。然后你把它最小化，于是就在右下角了（不好意思，至今我都不知道那个区域到底中文名叫什么，直译是提示区，但是从来没听人用普通话这么说过）。</p>

<p>Linux的用户稍微麻烦一些，第一次执行桌面生成的文件时会需要confirm，mark as trusted即可。因为和你的Firefox不共享数据，所以首次执行Prism程序会创建一套新的Firefox profile，然后检查插件更新，显示欢迎等等。没关系，关掉它，重新再执行一次，就会看到正常的登录框了，其他和Windows差不多。</p>

<p>但是Linux上的Prism还没有提供最小化到右上角(GNOME)或者右下角(KDE)或者其他什么角里的功能，怎么办？咱们有Window Manager，我是菜鸟，我刚会用Compiz，开启Scale Window吧，然后可以把窗口按比例缩小，所到不影响桌面工作，设不设置Always on top就看自己的想法了。</p>

<p>好的，就罗嗦到这里。</p>
