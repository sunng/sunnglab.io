---
layout: post
title: Alexander Sukhanov
categories:
- 自话
tags:
- Music
- Russia
published: true
comments: true
---
<p><img class="alignnone" title="亚历山大 舒克汉诺夫" src="http://www.math.msu.su/~sukh/im11.jpg" alt="" width="448" height="336" /></p>

<p>Alexander Alekseevich Sukhanov，亚历山大.舒克汉诺夫1952年出生，莫斯科大学Mechanics and mathematics faculty, Department of computational mathematics, Laboratory of computing methods 任教，数学博士。他的另一个重要身份是歌手和诗人。从1969年开始创作，拥有近150首作品。</p>

<p>这里可以找到大量作品：
<a href="http://bards.pp.ru/Suhanov/" target="_blank">http://bards.pp.ru/Suhanov/</a></p>

<p>个人主页：
<a href="http://www.math.msu.su/~sukh/" target="_blank">http://www.math.msu.su/~sukh/</a></p>

<p>Wiki Page：
<a href="http://en.wikipedia.org/wiki/Alexander_Sukhanov" target="_blank">http://en.wikipedia.org/wiki/Alexander_Sukhanov</a></p>

<p>魅力啊～
<div id="_mcePaste" style="overflow: hidden; position: absolute; left: -10000px; top: 348px; width: 1px; height: 1px;">个人主页：</div></p>
