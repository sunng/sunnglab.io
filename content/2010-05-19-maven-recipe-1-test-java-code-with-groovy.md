---
layout: post
title: 'Maven recipe #1: Test Java code with groovy'
categories:
- 把戏
tags:
- groovy
- java
- maven
published: true
comments: true
---
<p>问题： 受够了，不想写Java了，写个含有数据的map还要new出来一个一个put进去，想用groovy解决单元测试<br />
解决： gmaven+groovy eclipse插件可以解决这个需求<br />
在pom.xml中添加gmaven的依赖，注意，仅用来测试。老大不让生产代码里有不可靠的东西。<br />
[cc lang="xml"]<br />
...<br />
        <dependency>
            <groupid>org.codehaus.groovy.maven.runtime</groupid>
            <artifactid>gmaven-runtime-default</artifactid>
            <version>1.0-rc-3</version>
            <scope>test</scope>
        </dependency>
...<br />
            <plugin>
                <groupid>org.codehaus.groovy.maven</groupid>
                <artifactid>gmaven-plugin</artifactid>
                <version>1.0-rc-3</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>generateStubs</goal>
                            <goal>compile</goal>
                            <goal>generateTestStubs</goal>
                            <goal>testCompile</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
[/cc]</p>

<p>在eclipse中新建source folder, <em>src/test/groovy</em>，在其中创建groovy test case即可</p>
