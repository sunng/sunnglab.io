---
layout: post
title: jip 0.2 released
categories:
- ANN
tags:
- java
- jip
- jython
- maven
- project
- python
published: true
comments: true
---
<p>As you may know, jip is a dependency management tool for Jython/Java development. It resolves and downloads Java packages from maven-compatible repositories. jip also follows some best practices, encouraging you to use a portable and standalone environment (virtualenv) for your Jython development.</p>

<p>It has been four months since the initial release. In version 0.2, I made following changes for you:
<ul>
<li>Improved console output format</li>
<li>Correct scope dependency management inheritance</li>
<li>Snapshot management, alpha</li>
<li>Environment independent configuration</li>
<li>Bug fixes</li>
</ul></p>

<p>You can find the typical usage doc on github:
<a href="https://github.com/sunng87/jip">https://github.com/sunng87/jip</a></p>

<p>jip-0.2 has been published to pypi so you can install or upgrade it with:<br />
easy_install -U jip</p>

<p>Please do remember to use it inside virtualenv!<br /><br /><div class="zemanta-pixie"><img class="zemanta-pixie-img" alt="" src="http://img.zemanta.com/pixy.gif?x-id=6d854cec-aede-8421-9daa-99288bc44ead" /></div></p>
