---
layout: post
title: Patch for Fantasdic Google Source
categories:
- ANN
tags:
- foss
- gnome
- ruby
published: true
comments: true
---
<p>I found fantasdic a great powerful dictionary tool with friendly UI and supports multiply sources. Unfortunately, this project seems to be no longer maintained. The latest version is 1.0-beta7.
<a title="fantasdic-google by 贝小塔, on Flickr" href="http://www.flickr.com/photos/40741608@N08/4324570353/"><img src="http://farm5.static.flickr.com/4049/4324570353_c1f79ac6dc.jpg" alt="fantasdic-google" width="500" height="362" /></a></p>

<p>The google translate source was created in 2007. As the google translate service page changed, this module doesn't work any more, so I just picked up my vim then created this patch for those still want to use it.</p>

<p>This patch has dependency with ruby's json module, so before you apply it, run:
<em>sudo gem install json</em>
or
<em>sudo apt-get install libjson-ruby1.8</em></p>

<p>Download the patch at:
<a href="https://bugzilla.gnome.org/attachment.cgi?id=152835">https://bugzilla.gnome.org/attachment.cgi?id=152835</a></p>

<p>Patch it (Ubuntu installation as example):
<em>cd /usr/lib/ruby/1.8/fantasdic/sources/<br />
sudo patch google_translate.rb /home/sun/google_translate.rb.diff</em></p>

<p>Restart fantasdic, then your get it works! And this bug is tracked at:
<a href="https://bugzilla.gnome.org/show_bug.cgi?id=605641">https://bugzilla.gnome.org/show_bug.cgi?id=605641</a></p>
