---
layout: post
title: Inside Grails Flash Scope
categories:
- 手艺
tags:
- Grails
- java
- web
published: true
comments: true
---
<p><a href="http://www.grails.org" target="_blank">Grails</a>的在Servlet API的基础上增加了一个非常实用的FlashScope，FlashScope的生命周期为两次请求（也就是在一次重定向）。它的典型应用是POST方式提交后显示服务器端发给用户的提示信息，在平时的应用中会经常使用。</p>

<p>Grails的FlashScope接口定义在org.codehaus.groovy.grails.web.servlet包中，这个接口继承了Map接口，并定义了一个方法next()。</p>

<p>Grails的默认实现在org.codehaus.groovy.grails.web.servlet包中，GrailsFlashScope。这个实现内部定义了两个Map（生命周期为两个请求），current和next，这两个Map不断滚动，保持在一个请求中可以且仅可以访问到当前和前一次请求的上下文。<br />
[codesyntax lang="java"]<br />
public void next() {<br />
current.clear();<br />
current = (HashMap)next.clone();<br />
next.clear();<br />
reassociateObjectsWithErrors(current);<br />
}
[/codesyntax]</p>

<p>put的时候，Grails只把新制放到next表中，因为next将在下一次请求时继续保存<br />
[codesyntax lang="java"]<br />
public Object put(Object key, Object value) {<br />
// create the session if it doesn't exist<br />
registerWithSessionIfNecessary();<br />
if(current.containsKey(key)) {<br />
current.remove(key);<br />
}
storeErrorsIfPossible(next,value);</p>

<p>return next.put(key,value);<br />
}
[/codesyntax]</p>

<p>get的时候，Grails在两个Map中查找<br />
[codesyntax lang="java"]<br />
public Object get(Object key) {<br />
if(next.containsKey(key))<br />
return next.get(key);<br />
return current.get(key);<br />
}
[/codesyntax]</p>

<p>此外，FlashScope本身还是被放在session中<br />
[codesyntax lang="java"]<br />
private void registerWithSessionIfNecessary() {<br />
GrailsWebRequest webRequest = (GrailsWebRequest) RequestContextHolder.currentRequestAttributes();<br />
HttpSession session = webRequest.getCurrentRequest().getSession(true);<br />
if(session.getAttribute(GrailsApplicationAttributes.FLASH_SCOPE) == null)<br />
session.setAttribute(GrailsApplicationAttributes.FLASH_SCOPE, this);<br />
}
[/codesyntax]</p>

<p>然后，滚动FlashScope的行为在GrailsWebRequestFilter中被调用，GrailsWebRequestFilter继承自spring-web的OncePerRequestFilter<br />
[codesyntax lang="java"]<br />
protected void doFilterInternal(<br />
HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)<br />
throws ServletException, IOException {<br />
...</p>

<p>// Set the flash scope instance to its next state. We do<br />
// this here so that the flash is available from Grails<br />
// filters in a valid state.<br />
FlashScope fs = webRequest.getAttributes().getFlashScope(request);<br />
fs.next();<br />
...<br />
}
[/codesyntax]</p>
