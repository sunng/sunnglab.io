---
layout: post
title: Setting Environment Variables for NetBeans
categories:
- 把戏
tags:
- java
- netbeans
published: true
comments: true
---
<p>If you use NetBeans IDE to build an application which reads environment variable, you will have to do these settings in your IDE. There is no GUI to set such variables in NetBeans while Eclipse provides you a "Run Configuration Dialog". So just add the declaration in your netbeans.conf.</p>

<p>It is highly recommended to copy the configuration file to your home directory before you edit it.</p>

<p><em>cp -r /usr/local/netbeans-6.8/etc ~/.netbeans/6.8/</em></p>

<p>And append this line:</p>

<p><em>export YAN_RESOURCE=/home/sun/work/yan-resource</em></p>

<p>Then restart your IDE, it works. You can test/build/run your application with the environment variable <em>$YAN_RESOURCE</em> inside NetBeans IDE,</p>
