---
layout: post
title: RageViewer updated2
categories:
- ANN
tags:
- clojure
- project
published: true
comments: true
---
<p>I just added some new features to <a href="http://rageviewer.cloudfoundry.com/index.html" target="_blank">RageViewer</a>, a rage comic viewer written in Clojure and ClojureScript. Issue on a recent update of CloudFoundry has been fixed. So I can update this application as normal.</p>

<p>Since there are a lot of over-18 content on rage comics, RageViewer now prompts you to confirm if it is suitable to display NSFW (Not Suit For Work, marked by author) content. If rejected, a placeholder comic (made by me) will be displayed to you.
<img alt="" src="http://rageviewer.cloudfoundry.com/nsfw-placeholder.jpg" title="nsfw placeholder" class="alignnone" width="500" height="375" /></p>

<p>Also, a lot of rage comic related subreddits are now supported by RageViewer. You can find an indicator on the top bar for current channel. Click it and you can switch to a different channel. </p>

<p>Bugs are fixed when there is a ? on the end of image url. "Show most recent hot comics" button also works now.</p>

<p>Let me know if you have any ideas or questions about this site.</p>

<p></p>
