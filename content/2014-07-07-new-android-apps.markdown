---
layout: post
title: "Mapzei 和 观察者"
date: 2014-07-07 21:17:47 +0800
comments: true
tags: android
---

写了两个 Android 应用一致没有在博客上提，那么就一起提一下吧。

其一是3月份写的 [Mapzei](https://play.google.com/store/apps/details?id=info.sunng.muzei.maps)，是[Roman Nurik](https://plus.google.com/+RomanNurik/posts)的live wallpaper [Muzei](https://play.google.com/store/apps/details?id=net.nurik.roman.muzei)的扩展。 Roman Nurik 之前做过 Dash Clock，本身也是个开放架构，允许第三方增加扩展。 Mapzei是Muzei的扩展，每天取一张随机城市的地图作为桌面。用户可以配置地图的数据源：Google,OpenStreetMap或者Mapbox。

另一个是[观察者网](http://guancha.cn)的[客户端](https://play.google.com/store/apps/details?id=info.sunng.guanchazhe)，看新闻用。应用本身倒没有什么，用了一些典型的开源库：

* UniversalImageLoader
* SuperListView
* ButterKnife
* Eventbus

另外这个应用还发布在了[小米商店](http://app.mi.com/detail/65794).
