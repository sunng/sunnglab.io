---
layout: post
title: Take my coffee
categories:
- 当时
- 留影
tags:
- coffee
- Life
published: true
comments: true
---
<p>是coffee不是Java也不是CoffeeScript。买了两个月的咖啡机，到今天才终于把磨豆机和拉花杯都制备齐了。我上周用搪瓷缸打牛奶场面真是残不忍睹。不过我发现其实设备区别不大，蒸汽的掌握还是很重要的；牛奶的选择也很重要，鉴于家里没有其他牛奶可选，这部分跳过。蒸汽喷嘴与牛奶的举例很重要，我的咖啡机上还有个橡胶嘴我今天把它彻底扔掉了，直接用金属的喷嘴即可。按照某视频介绍，蒸汽嘴距离牛奶表面3毫米。这个距离如果太大就会导致牛奶乱喷，如果直接把喷嘴伸进牛奶我还没有尝试。</p>

<p>打好的牛奶就是这个样子，对不住大家的是，焦点跑了。
<a href="http://www.flickr.com/photos/40741608@N08/5795245711/" title="IMG_0647 by 贝小塔, on Flickr"><img src="http://farm3.static.flickr.com/2368/5795245711_d40c2afed9.jpg" alt="IMG_0647" height="500" width="375" /></a></p>

<p>加上我们刚喷好的咖啡
<a href="http://www.flickr.com/photos/40741608@N08/5795806680/" title="IMG_0650 by 贝小塔, on Flickr"><img src="http://farm4.static.flickr.com/3622/5795806680_6bcb9c4cab.jpg" alt="IMG_0650" height="375" width="500" /></a></p>

<p>就变成了这个样子
<a href="http://www.flickr.com/photos/40741608@N08/5795248727/" title="IMG_0651 by 贝小塔, on Flickr"><img src="http://farm4.static.flickr.com/3647/5795248727_8d7a99fd7d.jpg" alt="IMG_0651" height="375" width="500" /></a>
（我晕，焦点又跑了）</p>

<p>这个算是cappuccino吗。。。</p>

<p>最后还有一点咖啡，不要浪费了，搞点艺术创作：
<a href="http://www.flickr.com/photos/40741608@N08/5795810074/" title="IMG_0652 by 贝小塔, on Flickr"><img src="http://farm4.static.flickr.com/3608/5795810074_a7061ca08e.jpg" alt="IMG_0652" height="375" width="500" /></a>
焦点你最后终于来了。<br /><br /><div class="zemanta-pixie"><img class="zemanta-pixie-img" alt="" src="http://img.zemanta.com/pixy.gif?x-id=5c368ad7-7cee-8bbf-a47f-4c2001f11dd1" /></div></p>
