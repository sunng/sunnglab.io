---
layout: post
title: 'Slacker 0.6: Exposing multiple namespaces'
categories:
- ANN
tags:
- clojure
- project
- slacker
published: true
comments: true
---
<p>After <a href="https://github.com/sunng87/slacker/compare/8b4abdb62a...3e41e035b9" title="Changes">98 commits</a> in about one month, I'm glad to announce [slacker "0.6.1"].</p>

<p>One thing in slacker 0.6.x is you can expose multiple namespaces from a single server.</p>

<p>Suppose you have two namespaces `redday.stats` and `redday.api`, both contains functions you want to expose.</p>

<p>[cc lang="clojure"]<br />
  (start-slacker-server [(the-ns 'redday.stats)<br />
                         (the-ns 'redday.api)]<br />
                        6565)<br />
[/cc]</p>

<p>This will expose `redday.stats` and `redday.api` on port 6565.</p>

<p>On the client side, we have a new `use-remote` behaviors like clojure's use. Instead of local one, it imports functions from a remote namespace to your current namespace. </p>

<p>[cc lang="clojure"]<br />
(use 'slacker.client)<br />
;; create a slacker client<br />
(def scp (slackerc "127.0.0.1:6565")) </p>

<p>(use-remote 'scp 'redday.api) ;; caution, use the symbol of 'scp here <br />
(use-remote 'scp 'redday.stats)</p>

<p>;;top-titles is a function in redday.api<br />
;;now you can use the remote function transparently<br />
(top-titles "programming") </p>

<p>;;check function metadata you can find more slacker properties<br />
(meta top-titles)<br />
[/cc]</p>

<p>If you need to configure callback to a particular function, you can still use `defn-remote` to specify the callback function. In slacker 0.6.0, a `:remote-ns` is required when you define such a remote function.<br />
[cc lang="clojure"]<br />
(defn-remote top-titles :remote-ns "redday.api" :callback #(println %))<br />
[/cc]</p>

<p>The complete code example (both server and client) can be found <a href="https://bitbucket.org/sunng/slacker-demo/overview">here</a>. </p>

<p>In next post, I will explain another big new feature of 0.6.x, cluster support.</p>

<p></p>
