---
layout: post
title: Introduction to Amoeba
categories:
- ANN
tags:
- amoeba
- mysql
published: true
comments: true
---
<p>Amoeba is a distributed database middleware works as mysql proxy, provides sharding and high availability support for large scale applications using multiple mysql servers as backend.</p>

<p>Compatible with mysql protocol, Amoeba is fully transparent to any client using standard mysql drivers, which means, to use Amoeba you don't have to modify any code of database connector. You just configure rules for amoeba, then the client request will automatically route to certain mysql instance. Amoeba has a flexible set of configuration rules that can satisfy your requirements.</p>

<p>Amoeba is written in Java, and deployed on Linux server in most cases. The IO module is built on the top of Java nonblocking IO, which keeps communication between client, Amoeba and mysql at a high performance.</p>

<p>The project is initialized in 2008. Stable release 1.2.1-GA have been available since July, 2010 . It is now serving on the production environment of the social networks <a href="http://t.sdo.com/">http://t.sdo.com/</a></p>

<p>Project home:
<a href="http://code.google.com/p/amoeba/">http://code.google.com/p/amoeba/</a></p>

<p>Development logs:
<a href="http://amoeba.meidusa.com/wordpress/">http://amoeba.meidusa.com/wordpress/</a></p>

<p>The post is brought to you by <a href="http://fedorahosted.org/lekhonee">lekhonee</a> v0.7</p>
