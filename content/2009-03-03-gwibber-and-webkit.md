---
layout: post
title: Gwibber and WebKit
categories:
- 手艺
tags:
- css
- foss
- gnome
- twitter
- web
- webkit
published: true
comments: true
---
<p>Gwibber是一个用来接收流行的web2.0应用的消息的客户端。（靠，这句绝望了。）今天LinuxToy才刚刚介绍，不过我前几天就发现了，哦hohohoho。</p>

<p>Gwibber的架构很容易扩展，稍微看一看就清楚了，我就不描述了。正好饭否和twitter的接口是保持一致的，拷拷改改很容易就可以给Gwibber增加饭否的支持。如此一来就可以同时看饭否看twitter发饭否发twitter了。</p>

<p>你以为这是一张截图，其实它是分隔线。</p>

<p><img class="alignnone" title="Gwibber with fanfou support" src="http://pic.yupoo.com/classicning/15491710661a/xaq77l24.jpg" alt="" width="338" height="528" /></p>

<p>注意上图，gwibber是一个简单的pyGtk程序加上Webkit，它的核心部分就是html网页。这个网页除了头像以外可是没有图片的，那几个效果是怎么实现的呢。一个一个说。
<ul>
	<li>圆角 -webkit-border-radius: 7px;</li>
	<li>文字阴影 text-shadow: 1px 1px black;</li>
	<li>渐变 background:  -webkit-gradient(linear, left top, left 220%, from(rgba(254, 240, 152, 0.9)), to(black));</li>
</ul>
比较神奇的就是这三个属性。所以也难怪现在用WebKit核心的程序（不完全是浏览器）越来越多。没有记错的话，这三个丰富特性除了text-shadow在Firefox3.1开始支持其他的Gecko都还不支持。</p>

<p>总而言之，用CSS就可以绘制出精致的图形，而且完全可以生长在桌面。</p>
