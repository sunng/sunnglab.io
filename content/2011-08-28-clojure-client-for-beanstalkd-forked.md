---
layout: post
title: Clojure client for Beanstalkd forked
categories:
- ANN
tags:
- beanstalkd
- clojure
- project
published: true
comments: true
---
<p><a href="https://github.com/kr/beanstalkd">Beanstalkd</a> is a distributed task queue written by Keith Rarick. I just forked the clojure client as the original version is no long active. Some features are added:
<ul>
	<li>Add commands: stats-tube, stats-job, pause-tube, list-tubes, list-tube-used and list-tubes-watched</li>
	<li>Use yaml library to decode stats output into clojure data structure.</li>
</ul></p>

<p>Use it in your leiningen project:<br />
[org.clojars.sunng/beanstalk "1.0.5"]</p>
