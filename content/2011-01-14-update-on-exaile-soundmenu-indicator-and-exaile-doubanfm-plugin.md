---
layout: post
title: Update on exaile-soundmenu-indicator and exaile-doubanfm-plugin
categories:
- ANN
tags:
- Douban
- exaile
- project
- ubuntu
published: true
comments: true
---
<p><h3>Exaile-soundmenu-indicator</h3>
As many users complaint about the "minimise to sound menu" issue, I updated exaile-soundmenu-indicator plugin. Now it is basically compliant with specification of <a href="https://wiki.ubuntu.com/SoundMenu">SoundMenu</a>: it will keep playing if you click the close button while the player is playing, and will exit if not playing. However, to be able to complete the functionality, you have to commentify the line <strong>1506</strong> of <strong>/usr/lib/exaile/xlgui/main.py</strong>, which is "<strong>return true</strong>" of method "<strong>delete_event</strong>" (On Ubuntu 10.10, exaile 0.3.2.0-ubuntu3). <strong>Otherwise</strong>, whenever you close the window it won't exit.</p>

<p>I know this is a bad idea to require user to modify the source code, but it‘s not possible to override the behavior of a GTK callback, especially when you do not have the handler_id of the callback. If you do not mind the incompliant of closing behavior, you can just keep it as is, and exiting by menu and CRTL+Q.</p>

<p>Grab the snapshot of github repository to get the up-to-date version:
<a href="https://github.com/sunng87/Exaile-Soundmenu-Indicator">https://github.com/sunng87/Exaile-Soundmenu-Indicator</a></p>

<p><h3>Exaile-doubanfm-plugin</h3>
Exaile豆瓣电台插件更新。豆瓣最近调整了登录的策略：
<ul><li>用户在首次访问豆瓣时被设置cookie bid</li>
<li>用户提交登陆表单时被要求提交cookie bid，否则不予通过。</li></ul></p>

<p>此外，这次更新开始使用HTTPS提交用户登录信息。</p>

<p><a href="https://github.com/sunng87/exaile-doubanfm-plugin">https://github.com/sunng87/exaile-doubanfm-plugin</a>，请下载最新0.0.6c。</p>
