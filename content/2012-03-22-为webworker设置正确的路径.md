---
layout: post
title: 为WebWorker设置正确的路径
categories:
- 把戏
tags:
- heatcanvas
- javascript
published: true
comments: true
---
<p>WebWorker的路径通常是写在代码源文件中，而且这个路径并非其相对父js文件的相对路径，而似乎是相对页面的路径。所以指定一个正确的可随处部署的路径变得有些麻烦。昨天有人给HeatCanvas提了这个问题我才想到上网搜索了一下，有一个还算挺不错的办法。</p>

<p>写一个getPath函数，从document里找到父js的路径，拼到Worker的名字上。对heatcanvas.js这个文件来说就是：</p>

<p>[cc lang="javascript"]<br />
HeatCanvas.getPath = function() {<br />
    var scriptTags = document.getElementsByTagName("script");<br />
    for (var i=0; i<scriptTags.length; i++) {<br />
        var src = scriptTags[i].src;<br />
        var pos = src.indexOf("heatcanvas.js");<br />
        if (pos > 0) {<br />
            return src.substring(0, pos);<br />
        }<br />
    }<br />
    return "";<br />
};<br />
[/cc]</p>

<p>因此现在HeatCanvas已经解决了这个路径问题，现在这个库应该更好用了。当然如果你改了我的文件名我就无话可说了。
</p>
