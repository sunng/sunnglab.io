---
layout: post
title: OpenLayers extension for HeatCanvas
categories:
- ANN
tags:
- heatcanvas
- javascript
- OpenStreetMap
- project
published: true
comments: true
---
<p><img src="http://i.imgur.com/rtEuI.png" alt="" title="Hosted by imgur.com" /></p>

<p>As the screenshot shows, now you can embed HeatCanavs in your OpenLayers application, as well as OpenSteetMap.</p>

<p>Three steps to create such kind of map:</p>

<p>1. Create HeatCanvas layer:<br />
[cc lang="javascript"]<br />
// constructor params:<br />
// name, OpenLayers map instance, OpenLayers layer options, HeatCanvas options<br />
var heatmap = new OpenLayers.Layer.HeatCanvas("HeatCanvas", map, {},<br />
        {'step':0.3, 'degree':HeatCanvas.QUAD, 'opacity':0.8});<br />
[/cc]</p>

<p>2. Feed some data on layer:<br />
[cc lang="javascript"]<br />
heatmap.pushData(latitude, longitude, value);<br />
[/cc]</p>

<p>3. Add layer to map:<br />
[cc lang="javascript"]<br />
map.addLayer(heatmap);<br />
[/cc]</p>

<p>The live demo is hosted on github:
<a href="http://sunng87.github.com/heatcanvas/openstreetmap.html">http://sunng87.github.com/heatcanvas/openstreetmap.html</a></p>

<p><a href="https://github.com/sunng87/heatcanvas">HeatCanavs</a> is a heat map implementation on HTML5 canvas and WebWorker API.<br /><br /><div class="zemanta-pixie"><img class="zemanta-pixie-img" alt="" src="http://img.zemanta.com/pixy.gif?x-id=c938a108-0a36-8a66-b89a-693e8cf88e05" /></div></p>
