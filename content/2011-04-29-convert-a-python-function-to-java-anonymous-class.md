---
layout: post
title: Convert a Python function to Java anonymous class
categories:
- 手艺
tags:
- java
- jython
- python
published: true
comments: true
---
<p>When calling Java with Jython, anonymous inner class might be an issue because there is no such equivalent in Python.</p>

<p>In GUI programming, jython made additional effort on AWT event processing. You can pass a python function as some types of event listener.<br />
[cc lang="python"]<br />
def change_text(event):<br />
    print 'Clicked!'</p>

<p>button = JButton('Click Me!', actionPerformed=change_text)<br />
frame.add(button)<br />
[/cc]</p>

<p>Described in the <a href="http://www.jython.org/jythonbook/en/1.0/GUIApplications.html">Definitive Guide of Jython</a>:</p>

<p><blockquote>This works because Jython is able to automatically recognize events in Java code if they have corresponding addEvent()* and *removeEvent() methods. Jython takes the name of the event and makes it accessible using the nice Python syntax as long as the event methods are public. </blockquote></p>

<p>However, it does not work with Runnable, Callable and many other interfaces designed for anonymous usage.</p>

<p>To solve the incompatibility, I created a small decorator that converts Python function to a Java object.
<script src="https://gist.github.com/947926.js"> </script></p>

<p>Thanks to python's dynamic magic, we can create class in runtime and assign modified method to a particular instance. The conversion is performed once the function loaded. Also, you can pass something as arguments to constructor.</p>

<p>With anonymous_class decorator, the example above can be written as:<br />
[cc lang="python"]<br />
from javax.swing import JButton, JFrame<br />
from java.awt.event import ActionListener</p>

<p>frame = JFrame('Hello, Jython!',<br />
            defaultCloseOperation = JFrame.EXIT_ON_CLOSE,<br />
            size = (300, 300)<br />
        )</p>

<p>@anonymous_class(ActionListener, "actionPerformed")<br />
def change_text(dummy, event):<br />
    print 'Clicked!'</p>

<p>button = JButton('Click Me!', actionPerformed=change_text)<br />
frame.add(button)<br />
frame.visible = True<br />
[/cc]</p>
