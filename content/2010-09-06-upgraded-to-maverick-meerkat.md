---
layout: post
title: Upgraded to Maverick Meerkat
categories:
- 装备
tags:
- linux
- ubuntu
published: true
comments: true
---
<p>上周Ubuntu 10.10 Maverick Meerkat beta发布了，本人向来是在Beta版发布的时候更新，然后经历一个月的黑白颠倒最终走向光明走向stable。因为我安装的规模有点大，这次升级总共要下载两千八百多个包，花了我两天的时间。</p>

<p>升级到Meerkat后，由于内核版本变成了2.6.35，原先ath5k网卡的驱动madwifi不能用了。这点一定要有心理准备，好在madwifi已经有了针对2.6.35的分支。可以从madwifi-project网站上下载：
<a href="http://snapshots.madwifi-project.org/madwifi-0.9.4-current.tar.gz">http://snapshots.madwifi-project.org/madwifi-0.9.4-current.tar.gz</a>
然后您可以根据UserGuide上的步骤进行操作：
<a href="http://madwifi-project.org/wiki/UserDocs/FirstTimeHowTo">http://madwifi-project.org/wiki/UserDocs/FirstTimeHowTo</a>
话说我升级了网卡驱动之后无线网路稳定性、速度都有了很明显的提升，对一个饱受无线掉线折磨的人来说，这个太重要了。（我先前使用的驱动是 madwifi-hal-0.10.5.6-r4119）</p>

<p>其他变化包括：
<ul>
	<li>主题多了橙色元素</li>
<li>indicator applet增加了几个新的图标</li>
<li>tracker applet可以显示出来了 -_-|||</li>
<li>Evolution 2.30 工具栏有点变化</li>
<li>UpdateManager UI有点变化</li>
<li>多了一个Input Method Switch的图形界面设置</li>
<li>Indicator多了network manager</li>
</ul></p>

<p>其他更新还要慢慢体会，不过总得来说这次10.10更新不多，恐怕也和GNOME3跳票有关吧。</p>

<p>The post is brought to you by <a href="http://fedorahosted.org/lekhonee">lekhonee</a> v0.7</p>
