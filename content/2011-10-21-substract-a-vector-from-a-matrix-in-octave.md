---
layout: post
title: Substract a vector from a matrix in Octave
categories:
- 把戏
tags:
- math
- octave
published: true
comments: true
---
<p>假设你有一个矩阵：</p>

<p>[cc lang="text"]<br />
A = [1,2;3,4;5,6]<br />
[/cc]</p>

<p>以及一个矢量：</p>

<p>[cc lang="text"]<br />
B = [2,5]<br />
[/cc]</p>

<p>你希望对A的每一行元素对元素地减B，例如第一行<br />
[cc lang="text"]<br />
[1-2,2-5]<br />
[/cc]</p>

<p>但你不希望用循环完成这个工作，那么你可能想到利用B创建一个和A一样维度的矩阵然后进行.-：<br />
[cc lang="text"]<br />
C = [2,5;2,5;2,5]<br />
A .- C<br />
[/cc]</p>

<p>在Octave中可以利用repmat这个函数获得C：<br />
[cc lang="text"]<br />
C = repmat(B, length(A), 1)<br />
[/cc]<br />
但是对于大矩阵来说这是一种对内存的浪费。</p>

<p>更好的方法是利用bsxfun：<br />
[cc lang="text"]<br />
bsxfun(@minus, A, B)<br />
[/cc]</p>

<p>bsxfun在octave的文档中似乎鲜有提及。
</p>
