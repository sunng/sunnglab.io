---
layout: post
title: Open for a new job
categories:
- ANN
tags:
- job
published: true
comments: true
---
<p>I am about to leave current company and looking for a new challenge for future. If you are interested, and also looking for a Java or Python developer, please check the hot baked resume:
<ul>
<li><a href="http://sunng.info/resume_en.html">en</a></li>
<li><a href="http://sunng.info/resume_cn.html">zh</a></li>
</ul></p>

<p>Actually, I am not good at documenting myself. So if you need more details, feel free to get connected with email: classicning[at]gmail.com</p>

<p>The post is brought to you by <a href="http://fedorahosted.org/lekhonee">lekhonee</a> v0.7</p>
