---
layout: post
title: New features in JTS 1.11
categories:
- 装备
tags:
- GIS
- java
published: true
comments: true
---
<p><a href="http://lin-ear-th-inking.blogspot.com/2010/03/jts-version-111-released.html">JTS最近发布了1.11版本</a>，新增了：
<ul>
<li>对<a href="http://en.wikipedia.org/wiki/Delaunay_triangulation">Delaunay三角网</a>、<a href="http://en.wikipedia.org/wiki/Voronoi">Voronoi多边形</a>的支持；</li>
<li>把Geometry对象转换为AWT的Shape对象的功能</li>
<li>对几何对象进行densify的操作（增加结点）；</li>
<li>计算<a href="http://en.wikipedia.org/wiki/Hausdorff_distance">Hausdorff相似度</a>和Area相似度的支持</li>
</ul>
计算Delaunay三角网和Voronoi多边形：<br />
{% raw %}
``` java
import java.util.ArrayList;
import java.util.Collection;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.triangulate.DelaunayTriangulationBuilder;
import com.vividsolutions.jts.triangulate.VoronoiDiagramBuilder;

/**
 *
 * @author Sun Ning/SNDA
 * @since 2010-3-3
 */
public class DelaunayAndVoronoiApp {

	/**
	 * create some predefined sites
	 * @return
	 */
	public static Collection<coordinate> getPredefinedSites(){
		double[][] coords =  {{100,27},{28, 50},{29,
	 40},{32, 90}, {12, 26  }};
		ArrayList<coordinate> coordinates = new ArrayList<coordinate>(coords.length);</coordinate></coordinate></coordinate>

		for(int i=0; i<coords.length; i++){
			coordinates.add(new Coordinate(coords[i][0], coords[i][1]));
		}

		return coordinates;
	}

	/**
	 *
	 * @param coords
	 * @return a geometry collection of triangulations
	 */
	public static Geometry buildDelaunayTriangulation(Collection<coordinate> coords){
		DelaunayTriangulationBuilder builder = new DelaunayTriangulationBuilder();
		builder.setSites(coords);
		return builder.getTriangles(new GeometryFactory());
	}
	/**
	 *
	 * @param coords
	 * @return a collection of polygons
	 */
	public static Geometry buildVoronoiDiagram(Collection<coordinate> coords){
		VoronoiDiagramBuilder builder = new VoronoiDiagramBuilder();
		builder.setSites(coords);
		return builder.getDiagram(new GeometryFactory());
	}</coordinate></coordinate>

	/**
	 *
	 * @param args
	 */
	public static void main(String[] args){
		Collection<coordinate> coordinates = getPredefinedSites();</coordinate>

		/**
		 * Delauny
		 */
		GeometryCollection triangulations
				= (GeometryCollection)buildDelaunayTriangulation(coordinates);

		int total = triangulations.getNumGeometries();
		System.out.printf("Total triangulations: %d\n", total);
		for(int i=0; i<total; i++){
			Geometry g = triangulations.getGeometryN(i);
			Coordinate[] coords = g.getCoordinates();
			System.out.printf("Triangulation %d: ", i);
			for(Coordinate c : coords){
				System.out.printf("(%.3f, %.3f) ", c.x, c.y);
			}
			System.out.println();
		}

		/**
		 * Voronoi
		 */
		GeometryCollection diagram = (GeometryCollection)
				buildVoronoiDiagram(coordinates);
		int totalDia = diagram.getNumGeometries();
		for(int i=0; i<totalDia; i++){
			Geometry g = diagram.getGeometryN(i);
			Coordinate[] coords = g.getCoordinates();
			System.out.printf("Diagram %d: ", i);
			for(Coordinate c : coords){
				System.out.printf("(%.3f, %.3f) ", c.x, c.y);
			}
			System.out.println();
		}
	}

}

```
{% endraw %}
输出：<br />
[cc lang="text"]<br />
Total triangulations: 5<br />
Triangulation 0: (32.000, 90.000) (12.000, 26.000) (28.000, 50.000) (32.000, 90.000)<br />
Triangulation 1: (32.000, 90.000) (28.000, 50.000) (100.000, 27.000) (32.000, 90.000)<br />
Triangulation 2: (100.000, 27.000) (28.000, 50.000) (29.000, 40.000) (100.000, 27.000)<br />
Triangulation 3: (100.000, 27.000) (29.000, 40.000) (12.000, 26.000) (100.000, 27.000)<br />
Triangulation 4: (12.000, 26.000) (29.000, 40.000) (28.000, 50.000) (12.000, 26.000)<br />
Diagram 0: (-76.000, 88.625) (-76.000, 178.000) (176.713, 178.000) (72.699, 65.730) (-38.235, 76.824) (-76.000, 88.625)<br />
Diagram 1: (-76.000, -62.000) (-76.000, 88.625) (-38.235, 76.824) (11.978, 43.348) (56.422, -10.619) (57.006, -62.000) (-76.000, -62.000)<br />
Diagram 2: (11.978, 43.348) (-38.235, 76.824) (72.699, 65.730) (67.316, 48.882) (11.978, 43.348)<br />
Diagram 3: (176.713, 178.000) (188.000, 178.000) (188.000, -62.000) (57.006, -62.000) (56.422, -10.619) (67.316, 48.882) (72.699, 65.730) (176.713, 178.000)<br />
Diagram 4: (11.978, 43.348) (67.316, 48.882) (56.422, -10.619) (11.978, 43.348)<br />
[/cc]</p>

<p>将Geometry对象转换成Shape对象，绘制在JPanel上：<br />
[cc lang="java"]<br />
import java.awt.Color;<br />
import java.awt.Dimension;<br />
import java.awt.Graphics;<br />
import java.awt.Graphics2D;<br />
import java.awt.Shape;<br />
import java.util.Collection;<br />
import javax.swing.JFrame;<br />
import javax.swing.JPanel;</p>

<p>import com.vividsolutions.jts.awt.ShapeWriter;<br />
import com.vividsolutions.jts.geom.Coordinate;<br />
import com.vividsolutions.jts.geom.Geometry;<br />
import java.util.Random;</p>

<p>/**<br />
 *<br />
 * @author Sun Ning/SNDA<br />
 * @since 2010-3-3<br />
 */<br />
public class JTS2Awt {</p>

<p>	public static void showUI(final Shape... shape){<br />
		JFrame jframe = new JFrame("JTS Geometry to AWT Shape");</p>

<p>		JPanel jp = new JPanel(){<br />
			@Override<br />
			public void paint(Graphics g){<br />
				super.paint(g);<br />
				Graphics2D g2d = (Graphics2D)g;<br />
				if(shape != null){<br />
					for(Shape s: shape){<br />
						g2d.setColor(new Color(Color.HSBtoRGB(new Random().nextFloat(), 1f, 0.6f)));<br />
						g2d.draw(s);<br />
					}<br />
				}<br />
			}<br />
		};<br />
		jp.setPreferredSize(new Dimension(150, 150));</p>

<p>		jframe.getContentPane().add(jp);<br />
		jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);<br />
		jframe.pack();<br />
		jframe.setVisible(true);<br />
	}</p>

<p>	public static Shape toShape(Geometry geom){<br />
		ShapeWriter writer = new ShapeWriter();<br />
		return writer.toShape(geom);<br />
	}</p>

<p>	public static void main(String[] args){<br />
		Collection<coordinate> coords = DelaunayAndVoronoiApp.getPredefinedSites();<br />
		Geometry geomT = DelaunayAndVoronoiApp.buildDelaunayTriangulation(coords);<br />
		Geometry geomD = DelaunayAndVoronoiApp.buildVoronoiDiagram(coords);</coordinate></p>

<p>		showUI(toShape(geomT), toShape(geomD));<br />
	}</p>

<p>}</p>

<p>[/cc]
<a href="http://www.flickr.com/photos/40741608@N08/4403349474/" title="jts by 贝小塔, on Flickr"><img src="http://farm5.static.flickr.com/4010/4403349474_cc3db44369_o.png" width="158" height="177" alt="jts" /></a></p>
