---
layout: post
title: 使用defrecord与defprotocol的注意事项
categories:
- 手艺
tags:
- clojure
- programming
published: true
comments: true
---
<p>简单地说，protocol是clojure中的接口，record是clojure中的数据类型。</p>

<p>可以通过这样的code定义一个protocol<br />
[cc lang="clojure"]<br />
(defprotoco DummyProtocol<br />
  "doc string..."<br />
  (method-one [self x] "doc string..."))<br />
[/cc]</p>

<p>需要注意的是，protocol里所有方法的第一个参数都是self/this参数（类似python），从第二个开始才是调用时传入的参数。如果方法要重载呢？</p>

<p>[cc lang="clojure"]<br />
(defprotocol DummyProtocol<br />
  "doc string..."<br />
  (method-one [self x] [self x y] "doc string")<br />
)
[/cc]</p>

<p>Apress的 Practical Clojure 书里的例子，给重载的参数表加上了括号，这样会导致编译错误（<a href="http://book.douban.com/annotation/13819191/" target="_blank">注记</a>）。</p>

<p>定义一个record实现protocol</p>

<p>[cc lang＝"clojure"]<br />
(defrecord DummyRecord [a b c]<br />
  DummyProtocol<br />
  (method-one [self x] (+ a x))<br />
  (method-one [self x y] (+ a x y)))<br />
[/cc]<br />
Practical Clojure里关于这部分的代码，又丢掉了self参数（<a href="http://book.douban.com/annotation/13819264/" target="_blank">注记</a>）。</p>

<p>最后还有一个问题，如果直接use你的ns，你会发现调用record时出现：<br />
java.lang.IllegalArgumentException: Unable to resolve classname: DummyRecord</p>

<p>怎么回事，不是都use了吗？原因是record被编译成了java对象，所以引用时要用java对象的引用方式，import之。</p>
