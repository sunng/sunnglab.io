---
layout: post
title: Another simple micro-blogging tool initialized
categories:
- 把戏
tags:
- ibatis
- java
- spring
- strust2
- web
published: true
comments: true
---
<p>A simple micro blogging tool based on Java web framework stack (Struts2/Spring/iBatis). It costs me five days to develop such a prototype version which supports basic functions(view, post, follow and tag). And I will try to deploy it on GAE later(I hope it is possible). More improvements will also come up in next severals day.</p>

<p><img class="alignnone" title="Screenshot for YAMB" src="http://pic.yupoo.com/classicning/837997da6200/medium.jpg" alt="" width="500" height="407" /></p>

<p>The prototype is just simple and plain which, i think, might be a good instance for books that titled with "Teach Yourself Java Web in 7 Days",(hough it's not a popular topic any more):). I'm also thinking of the table of content:
<ol>
	<li>Introduction to 3-Tier Java Web Develop</li>
	<li>Setup Your Maven Environment</li>
	<li>Setup Your Eclipse IDE</li>
	<li>Creating Domain Objects and Tables</li>
	<li>Writing Down Your First iBatis DAO</li>
	<li>Spring for Bean Wiring</li>
	<li>Unit Test with JUnit in Spring</li>
	<li>Struts2 in Action</li>
	<li>Integrating Everything with Spring</li>
	<li>Create Ajax enabled Frontend</li>
	<li>Easy Deployment</li>
</ol>
This is my first maven managed web project. When using maven with hsqldb, there is no extra configuration needed if you switch develop environment. Really effective!</p>
