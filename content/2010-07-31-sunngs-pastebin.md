---
layout: post
title: sunng's pastebin
categories:
- ANN
tags:
- gae
- python
published: true
comments: true
---
<p>写了一个基本的pastebin放在appengine上：
<a href="http://sunoffline.appspot.com/pb/">http://sunoffline.appspot.com/pb/</a></p>

<p>支持纯文本、Markdown和代码高亮。数据永久保留，推荐大家收藏以备不时之需。</p>

<p>The post is brought to you by <a href="http://fedorahosted.org/lekhonee">lekhonee</a> v0.7</p>
