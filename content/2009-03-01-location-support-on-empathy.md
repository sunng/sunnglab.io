---
layout: post
title: Location Support on Empathy
categories:
- 装备
tags:
- foss
- gnome
- linux
- location
published: true
comments: true
---
<p>好久不写了 ||</p>

<p>昨天看到了这么一篇介绍Gnome桌面上对地理位置的支持的文章（汗这句子）。地址在<a href="http://bergie.iki.fi/blog/making_the_gnome_desktop_location-aware/" target="_blank">这里</a>。</p>

<p>我印象中最早的就是Gnome 2.24开始那个日期applet增加的一个地图，可以选择和设置当前的location，于是有了比较好的时区支持和天气服务。文章里还提到居然Tracker搜索也支持这么一个地理元信息，可以搜出在某个地方（是说地球上的某个地方）编辑的文件。这个很强。</p>

<p>然后就是Empathy对location的支持了。这个看起来很强大，截图和介绍可以看这里看这里看<a href="http://blog.pierlux.com/2009/01/22/empathy-where-are-you/en/" target="_blank">这里</a>。Empathy可以用来发布你evolution的联系人地址。话说，很容易就想起来了，XMPP协议有一个地理位置的扩展。不过文中说几乎除了GoogleTalk其他都支持地理位置扩展。好家伙，这个又只能遥远地YY了。</p>

<p>文中提到的提供LBS的库叫做Geoclue，而在Gnome上显示地图是一个叫做libchamplain的widget。而地图，当然毫无疑问是OSM了。有兴趣的朋友可以去关注。</p>

<p>没了，静静等待这个Empathy变得越来越强大吧。</p>
