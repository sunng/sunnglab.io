---
layout: post
title: Groovy http-builder tips
categories:
- 把戏
tags:
- groovy
- java
published: true
comments: true
---
<p>最近用Groovy的HttpBuilder模块开发RESTful服务的客户端，遇到不少问题，浪费不少时间</p>

<p>网上的例子都是从Grape开始的，但是按照例子上运行，无论是通过grape install还是在代码里@Grab都没有办法把http builder加到classpath里，甚至依赖关系已经下载到~/.groovy/grapes里了，但是还是在import的时候报错。用Grails的时候也是一样，如果通过ivy定义依赖，通过ant下载到lib目录中，在grails shell里还是没有办法引用。不理解了，难道还需要手动再指定path吗。最后用grails的rest插件（grails install-plugin rest）总算是下载到了path中了。</p>

<p>第二是http builder的报错太智能了，它会把服务器端50x的报错输出在客户端。在查看错误的时候一定要分清楚。</p>

<p>第三是net.sf.json-lib这个库在转json的时候有一点问题，比如这样的js对象
<em>{"result":"success"}</em></p>

<p>会因为key上有引号而无法解析，必须是这样的格式：
<em>{result: "success"}</em></p>

<p>至于那种是正确的json我也没有兴趣了解了，只是非常遗憾的是couchdb返回的都是前一种形式。</p>

<p>当然了，瑕不掩瑜，http builder还是提供了非常方便的封装，比直接用HttpURLConnection或是HttpClient更加Groovy</p>
