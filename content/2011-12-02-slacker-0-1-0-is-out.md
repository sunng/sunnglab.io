---
layout: post
title: Slacker 0.1.0 is out
categories:
- ANN
tags:
- clojure
- project
published: true
comments: true
---
<p>Glad to roll out the first release of the <strong>slacker</strong> framework. Slacker is a clojure RPC framework on top of a TCP binary protocol. It provides a set of non-invasive APIs for both server and client. The remote invocation is completely transparent to user.</p>

<p>In addition to APIs introduced in <a href="http://sunng.info/blog/2011/11/clojure-rpc-prototyping-and-early-thoughts/">last post</a>, asynchronous approach is supported in client API :<br />
[cc lang="clojure"]<br />
(defremote remote-func :async true)<br />
@(remote-func)<br />
[/cc]<br />
If you add option `:async` to defremote, then the function facade will return a promise. You have to deref it by yourself. Also you can use the `:callback` option in defremote to specify a callback function.<br />
[cc lang="clojure"]<br />
(defremote remote-func :callback #(println %))<br />
(remote-func)<br />
[/cc]</p>

<p>This gives you much more flexibility of using remote function. But be careful it will break consistency between local and remote mode. </p>

<p>To use slacker, add it to your project.clj<br />
[cc lang="clojure"]<br />
:dependencies [[info.sunng/slacker "0.1.0"]]<br />
[/cc]</p>

<p>You can find examples on the <a href="https://github.com/sunng87/slacker" target="_blank">github page</a>. </p>
