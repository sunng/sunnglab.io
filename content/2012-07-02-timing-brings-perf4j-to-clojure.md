---
layout: post
title: Timing brings perf4j to clojure
categories:
- ANN
tags:
- clojure
- github
- java
published: true
comments: true
---
<p><a href="https://github.com/sunng87/timing" target="_blank">Timing</a> is a dead simple clojure library wraps perf4j, while perf4j is an advanced library to log call time. It's like log4j to System.out.println, that to System.currentTimeMillies. </p>

<p>The core part of Timing library is a `timed` macro. You can put any forms in it and it will log the call time.</p>

<p>[cc lang="clojure"]<br />
(timed "demo"<br />
  (look-for-something-from-the-moon)<br />
  ...)<br />
[/cc]</p>

<p>And it logs:<br />
start[1341215254682] time[1000] tag[demo]</p>

<p>Timing looks up your classpath at startup for a logging backend: slf4j or log4j. If neither of them found, it fails back to log to stderr. By the way, log4j is the recommended logging backend. There are some predefined appender in perf4j to generate semi-realtime summary and charts. Anyway, Timing, as a library, doesn't depend on any library other than perf4j.</p>

<p>The project is hosted at <a href="https://github.com/sunng87/timing" target="_blank">github</a> and available on clojars of version "0.1.0".</p>
