---
layout: post
title: Pylons Tips
categories:
- 手艺
tags:
- pylons
- python
- web
published: true
comments: true
---
<p>All scripts created by paster(paster create, paster controller and etc.) uses 4 spaces as indent while many editors(vim) uses tabs automatically. Different indent in one file will cause fatal error.</p>

<p><em>h.url_for</em> is a popular helper function in many books and tutorials which to solve url mapping. This tool is no longer available by default since 0.97. If you got error message like "AttributeError: 'module' object has no attribute 'url_for'", just add</p>

<p>[codesyntax lang="python"]from routes import url_for[/codesyntax]<br />
to<em> yourapp/libs/helper.py</em> .</p>
