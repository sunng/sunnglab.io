---
layout: post
title: DoubanCovers has been merged into Exaile-cn
categories:
- ANN
tags:
- exaile
- foss
published: true
comments: true
---
<p>既然是exaile-cn，就不扯蹩脚的英语了。</p>

<p>前天和Exaile-cn的作者进行了一些沟通，现在DoubanCovers插件已经合并到Exaile-CN项目里了。<a href="http://code.google.com/p/exaile-cn/">Exaile-CN</a>为Exaile提供符合中文用户习惯的一系列插件，包括歌词插件、迷你模式等。</p>

<p>As exaile-cn has not been adapted to exaile 0.3, the plugin will not be packaged into public release at once. if you use svn, you can checkout the plugin from branch:
<em>svn checkout http://exaile-cn.googlecode.com/svn/branches/0.3/</em></p>

<p>For a ready to use package, download from here as mentioned before:
<a href="http://bitbucket.org/sunng/exailedoubancovers/downloads/">http://bitbucket.org/sunng/exailedoubancovers/downloads/</a></p>
