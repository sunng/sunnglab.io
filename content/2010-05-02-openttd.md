---
layout: post
title: 交通游戏OpenTTD
categories:
- 把戏
tags:
- foss
- opensource
published: true
comments: true
---
<p>我总是对这种城市建设、交通规划一类的游戏欲罢不能，以前在Linux上一直只是玩<a href="http://lincity-ng.berlios.de/">Lincity</a>，后来发现了纯交通类的<a href="http://www.simutrans.com/">simutrans</a>，最近升级到Ubuntu10.04以后，终于可以玩这个六年磨一剑，精雕细琢的<a href="http://openttd.org">OpenTTD</a>了。</p>

<p><a href="http://www.flickr.com/photos/40741608@N08/4571371260/" title="Chindhattan Springs Transport, 19th Dec 1953 by 贝小塔, on Flickr"><img src="http://farm4.static.flickr.com/3521/4571371260_ce28e57779.jpg" width="500" height="284" alt="Chindhattan Springs Transport, 19th Dec 1953" /></a></p>

<p>可以建造各种客运货运的汽车站、火车站，机场，码头等等，可以建设道路、铁路，可以购买公交车、卡车、火车头、火车车厢等等。可惜我玩了大半天还不知道怎么盈利。。。</p>
