---
layout: post
title: Adding -var-missing to Clojure Namespace
categories:
- 手艺
tags:
- clojure
published: true
comments: true
---
<p><h3>Motivation</h3></p>

<p>I Just need some mechanism like "methondMissing" in Ruby. When a nonexistent var is called, the lookup system will try to call a "-var-missing" function in the namespace. This function should return a var and clojure compiler assumes this var as the one it was looking for.</p>

<p>For example, in the shake library:<br />
[cc lang="clojure"]<br />
(ns shake.core)</p>

<p>(defn -var-missing [sym]<br />
  (create-executable-var sym))<br />
[/cc]</p>

<p>To support lazy loading, shake 0.3.0 won't read your path. It will now create vars on demand. So on calling sh/uname, a var named `uname` will be created. And to create a var, just use intern or eval.</p>

<p>[cc lang="clojure"]<br />
(require '[shake.core :as sh])<br />
(sh/uname -a)<br />
[/cc]</p>

<p><h3>How to</h3></p>

<p>I'm sorry there is no way to implement this except hacking into Clojure's compiler. Fortunately, it's not too difficult to find out the injection point. </p>

<p>As you may know, there are two phases in Clojure compiler: expanding macros and evaluating forms. Both phases will look up vars to find macros or values. So we should take care both of them.</p>

<p>All code diff is here:
<script src="https://gist.github.com/3962750.js?file=Compiler.java"></script></p>

<p><h3>Conclusion</h3>
Adding -var-missing is just an attempt to implement lazy loading of vars. And it provides another smooth syntax for writing DSLs. But actually, Clojure's macro system provides a great metaprogramming mechanism. So in most case, you don't have to hack into the Compiler like this. Just put your DSL into a top level macro, and you can get them done all in clojure scripts. </p>

<p></p>
