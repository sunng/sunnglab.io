---
layout: post
title: Use pipe in subprocess
categories:
- 把戏
tags:
- linux
- python
published: true
comments: true
---
<p><h3>问题：</h3>
您要通过python的subprocess模块执行一些命令，要在程序中获得命令的标准输出，此外，命令中还有管道操作。
<h3>解决：</h3>
以这个命令为例：
<em>cat /home/admin/deploy/log/2009-11-11.log ｜grep example.com ｜wc -l</em>
<pre class="brush:python">
import subprocess</pre></p>

<p>cmd1 = 'cat /home/admin/deploy/log/2009-11-11.log'<br />
cmd2 = 'grep example.com'<br />
cmd3 = 'wc -l'</p>

<p>pipe1 = subprocess.Popen(cmd1.split(), stdout=subprocess.PIPE)<br />
pipe2 = subprocess.Popen(cmd2.split(), stdin=pipe1, stdout=subprocess.PIPE)<br />
pipe3 = subprocess.Popen(cmd3.split(), stdin=pipe2, stdout=subprocess.PIPE)</p>

<p>result = pipe3.communicate()[0]
</p>
