---
layout: post
title: Happy New Year!
categories:
- 留影
tags:
- boardgame
published: true
comments: true
---
<p><a href="http://www.flickr.com/photos/40741608@N08/5311404087/" title="IMG_0276 by 贝小塔, on Flickr"><img src="http://farm6.static.flickr.com/5081/5311404087_fea4651daf.jpg" width="500" height="375" alt="IMG_0276" /></a>
今天基本上学会了农场主的基本规则，在我看来虽然复杂，但是多少也和波多黎各什么的类似，这大概就是这类桌游的pattern吧。负责任地说，绝对没有两个人玩的晨昏对峙复杂（又是太空又是核武又是战争指数还有全球战场搞神马啊）。这点从桌游店就能看出来，去拿农场主的时候老板还会对你表示一下惊叹和欣赏；可是要玩晨昏对峙呢，根本店里根本找不到。高出不胜寒啊！你一生要是能找到一个棋逢对手的朋友一起玩晨昏对峙，也是一件幸事 -___-。</p>

<p>Anyway，农场主也很好玩，我就喜欢这种卡牌多、道具多的桌游。桌游的道具模型都可以复用了，它的猪和卡卡颂商人扩展里的一模一样。</p>

<p>专心休假，停止劳作。</p>
