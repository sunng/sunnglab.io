---
layout: post
title: "slacker library updated"
date: 2014-03-04 23:40
comments: true
tags: clojure, programming
---

After almost two years idle in commit log, I restarted development of my slacker frameworks recently. It will be used in our AVOS Cloud production as integration solution between clojure systems.

The recent update in slacker, slacker-cluster and link are:

### link

There are two releases in the [link](https://github.com/sunng87/link) library, features include:

* Ported to Netty 4
* WebSocket server handler
* Ability to shutdown server
* Ability to use core.async with link ring adapter

### slacker

The most recent release of [slacker](https://github.com/sunng87/slacker) is 0.11.0:

* New ping-interval option for clients
* Async callback will accept two argument, and can handle exceptions
* Slingshot removed, we are now using clojure built-in ex-info
* All client creation functions and defn-remote are delayed
* Bugfix for issue when several clients started in a process

### slacker-cluster

There's major enhancement in [slacker cluster](https://github.com/sunng87/slacker-cluster) 0.11.0

* Server exception no longer blocks client
* Added options to call functions on multiple servers, control return type and exception handling

All these libraries are available on github. I will write more about how we use them in AVOS Cloud in future.
