Title: Announcing Espresso: the start of a modern Java web ecosystem
Date: 2018-11-17 16:07:19
Tags: java, web

We have seen rapid development of Java itself these days. The
introduction of functional programming elements made Java coding
experience quite sharper.

However, the Java web ecosystem is still with its early days . The
Servlet API is dated, heavy and verbose. Even with Spring boot has
improved Java development, simpler abstraction like Python's WSGI is
still my preference.

So I started [Espresso](https://github.com/espresso4j/), a core web
abstraction and a family of micro libraries to support it. The idea of
Espresso is highly inspired by Clojure's
[Ring](https://github.com/ring-clojure/ring) and Rust's
[Iron](https://github.com/iron/iron). And the name *Espresso* is
specially chosen for *Java*. Imaging the Java web application is a cup
of coffee, the Espresso is the soul of it.

The core of Espresso is quite simple, it's a function takes a
`Request` object and returns a `Response` object. There is also an
asynchronous variant.

A hello world web application with espresso can be simple as:

```java
import io.github.espresso4j.espresso.*;
import io.github.espresso4j.jettino.Jettino;

public class App {

    public static void main(String[] args) throws Exception {
        new Jettino().start((req) -> Response.of(200).body("It works."));
    }

}
```

Basically the application starts a jetty server and hosts a web
application that return HTTP 200 and a body of `It works.`;

To make Espresso usable, the initial family of the system contains:

* [Jettino](https://github.com/espresso4j/jettino): A web server
  adapter to run Espresso with [Jetty](http://www.eclipse.org/jetty/).
* [Latte](https://github.com/espresso4j/latte): A URL router to route
  request to particular espresso function, based on URL pattern. And
  it generates an espresso application itself.
* [Sugar](https://github.com/espresso4j/sugar): A parameter extractor
  for dealing with query string, form and Json body.

A simple demo is available
[here](https://github.com/espresso4j/espresso-examples). You can grab
and run it with `gradle run`. The code is super easy to understand.

The libraries have been published on [Maven
central](https://search.maven.org/search?q=g:io.github.espresso4j).

## Call for collaboration

To grow a ecosystem, it requires quite a lot work on
upstream/downstream libraries, documents and communication. I have
some ideas to be implemented like:

* Extensible session middleware
* Cookie middleware
* Content negotiation
* Spring-boot integration

If you find the library and idea interesting and want to join me in
the development on code, doc, design and communication, feel free
to contact me via the email list on [my github
page](https://github.com/sunng87/).
