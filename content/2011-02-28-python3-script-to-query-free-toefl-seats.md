---
layout: post
title: Python3 script to query free TOEFL seats
categories:
- 手艺
tags:
- python
- web
published: true
comments: true
---
<p>应同学要求，写了一个爬托福考试空闲考位的脚本。这是第一次真正用Python3写。</p>

<p><script src="https://gist.github.com/846150.js?file=gistfile1.py"></script></p>

<p>通过调用get_seat_status，传入省名（如'Jiangsu'），时间（如'201104'），您可以得到这样的数据结构：<br />
[cc lang="python"]<br />
[ [datetime.datetime(2011, 4, 23, 10, 0),  'STN80085B',  '南通市教育装备与勤工俭学管理中心',  '1415',  True],<br />
 [datetime.datetime(2011, 4, 23, 10, 0), 'STN80086A', '扬州大学', '1415', True]]<br />
[/cc]</p>

<p>下面是一个多线程调用的例子：<br />
[cc lang="python"]<br />
#! /usr/bin/python3</p>

<p>import threading<br />
import pprint<br />
from toeflgraber import get_seat_status</p>

<p># 查询江浙沪 2011年3月和4月的考位<br />
locations = ['Jiangsu', 'Shanghai', 'Zhejiang']<br />
months = ['201103', '201104']</p>

<p>def descartes(x, y):<br />
    for i in x:<br />
        for j in y:<br />
            yield (i, j)</p>

<p>lock = threading.RLock()<br />
tasks = len(locations) * len(months)<br />
e = threading.Event()<br />
results = []</p>

<p>def task(location, month):<br />
    global results, tasks<br />
    all_seats = get_seat_status(location, month)<br />
    available_seats = filter(lambda x: x[4], all_seats)<br />
    with lock:<br />
        results.extend(available_seats)<br />
        tasks -= 1<br />
        if tasks == 0:<br />
            e.set()</p>

<p>for t in descartes(locations, months):<br />
    t = threading.Thread(target=task, args=t)<br />
    t.daemon = True<br />
    t.start()</p>

<p>e.wait()<br />
pprint.pprint(results)<br />
[/cc]</p>

<p>Enjoy it.</p>
