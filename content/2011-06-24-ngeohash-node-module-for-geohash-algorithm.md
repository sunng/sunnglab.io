---
layout: post
title: 'ngeohash: node module for geohash algorithm'
categories:
- ANN
tags:
- GIS
- javascript
- nodejs
- opensource
published: true
comments: true
---
<p>ngeohash是一个geohash的javascript实现，之所以叫做ngeohash是因为到了Publish的时候才发现已经有geohash这个module了。这么令人沮丧的事就不多说了。</p>

<p>安装
<font face="monospace">npm install ngeohash
</font></p>

<p>使用<br />
var geohash = require('ngeohash');<br />
sys.puts(geohash.encode(32.1717, 118.2342));</p>

<p>详细<br />
访问github的相关页面：<a href="https://github.com/sunng87/node-geohash">https://github.com/sunng87/node-geohash
</a><br /><br /><div class="zemanta-pixie"><img class="zemanta-pixie-img" alt="" src="http://img.zemanta.com/pixy.gif?x-id=62cf2f06-ce63-87a6-a76b-5899455660e7" /></div></p>
