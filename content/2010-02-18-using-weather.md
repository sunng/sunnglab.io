---
layout: post
title: Using weather
categories:
- 装备
tags:
- linux
- location
- util
published: true
comments: true
---
<p>Ubuntu仓库里有个weather-util包，可以用来查看天气信息。weather工具从weather.noaa.gov网站获得天气信息，对美国的城市可以直接用名字查询，其他国家只能使用id查询。id的规则和weather.com不太相同。</p>

<p>中国的天气站点id可以在<a href="http://weather.noaa.gov/weather/CN_cc.html">这个页面</a>上查到。比如南京ZSNJ，上海浦东ZSPD。使用weather工具查询：
<em>weather -i ZSNJ</em>
<blockquote>Current conditions at China (ZSNJ) 32-00N 118-48E 12M (ZSNJ)<br />
Last updated Feb 18, 2010 - 08:00 AM EST / 2010.02.18 1300 UTC<br />
   Temperature: 35 F (2 C)<br />
   Relative Humidity: 59%<br />
   Wind: from the SSW (210 degrees) at 4 MPH (4 KT)</blockquote></p>

<p>简化操作，可以在$HOME下创建.weatherrc文件，形如<br />
[cc lang="ini"]<br />
[default]<br />
ID = ZSPD<br />
[nj]<br />
ID = ZSNJ<br />
[/cc]</p>

<p>就可以直接使用
<em>weather</em>
<blockquote>
Current conditions at China (ZSPD) 31-07N 121-46E (ZSPD)<br />
Last updated Feb 18, 2010 - 08:00 AM EST / 2010.02.18 1300 UTC<br />
   Temperature: 35 F (2 C)<br />
   Relative Humidity: 47%<br />
   Wind: from the NE (050 degrees) at 7 MPH (6 KT) (direction variable)
</blockquote>
和
<em>weather nj</em>
<blockquote>Current conditions at China (ZSNJ) 32-00N 118-48E 12M (ZSNJ)<br />
Last updated Feb 18, 2010 - 08:00 AM EST / 2010.02.18 1300 UTC<br />
   Temperature: 35 F (2 C)<br />
   Relative Humidity: 59%<br />
   Wind: from the SSW (210 degrees) at 4 MPH (4 KT)</blockquote></p>
