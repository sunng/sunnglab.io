---
layout: post
title: "Smoothing Your Rust Crates Release With Cargo-release"
date: 2016-03-21T22:28:06+08:00
tags: rust, programming
---

Cargo-release is a cargo subcommand that automates your libraries
release process. The idea was inspired by Clojure's leiningen, which
has a `release` command does exact same thing.

Typically, when you decide to release a library to crates.io, you need
to go through following steps at least:

* bump version in Cargo.toml
* create a git tag for the release
* run `cargo publish`
* push tags/commits to github

cargo-release command deals these tasks for you. In addition, it
increases your patch version and add a `pre` extension after `cargo
publish`. The Java/Maven world uses a `SNAPSHOT` version for
pre-release. What's sad, no other package manager follows this great
idea. With this pre-release extension, users are able to tell if your
master branch is on a released version or not.

By default, cargo-release removes pre-release extension as the version
to release. You can override this behavior by a `--level`. The value
for `--level` can be `major`, `minor` or `patch`. As the name
suggests, it increases different part of a semver version.

For multi-crates repository, `cargo-release` reads you sub-directory
name and use it as prefix of git tag to avoid conflicts.

Some more features are planned for this tool, for instance:

* Custom tag prefix
* Build docs and push to gh-pages

I hope this tool can be used as a standard of rust library release
process. You can install this tool by `cargo install
cargo-release`. And the github repo is
[here](https://github.com/sunng87/cargo-release).
