---
layout: post
title: "Delicious 发布"
date: 2013-10-08 21:56
comments: true
categories: 
---

很久不写了，我们最近忙于 Delicious 的发布，终于上个月24号，新版本正式和所有用户见面了。

作为 Web 2.0 的旗帜性产品，Delicious 已经存在了整整十年。我们这次改版，给这个经典的网站换了新的设计，另外增加了一些新的功能，帮助你在收藏之外，从 Delicious 上获取更多的信息。比如可以从[#programming](https://delicious.com/tag/programming)里看到最近流行的关于programming被收藏最多的链接。新的Discover会根据用户的社交网络，推荐热门的链接。

所有这些才仅仅是开始，接下来的一段时间内，Delicious 还有更多的功能和产品会推出。


