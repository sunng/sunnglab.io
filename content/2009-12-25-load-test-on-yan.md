---
layout: post
title: Load Test on Yan
categories:
- ANN
tags:
- captcha
- project
- Yan
published: true
comments: true
---
<p>给Yan的验证码图片服务做了压力测试。测试环境：
<ul>
	<li>Intel Xeon 3.00GHz 4核</li>
	<li>内存2G</li>
	<li>Red Hat Enterprise Linux AS release 4 (Nahant Update 7)</li>
	<li>Jetty 6 / JDK 6</li>
</ul>
Jetty采用默认配置 maxThreads 200。</p>

<p>测试工具：ab (Apache Bench)</p>

<p>分别用10/50/100/200/500/1000并发用户，每个用户请求100次进行测试。结果如下：</p>

<p><!--   		BODY,DIV,TABLE,THEAD,TBODY,TFOOT,TR,TH,TD,P { font-family:"宋体"; font-size:x-small } --></p>

<p><!--   		BODY,DIV,TABLE,THEAD,TBODY,TFOOT,TR,TH,TD,P { font-family:"宋体"; font-size:x-small } -->
<table border="0" cellspacing="0" frame="VOID" rules="NONE"><colgroup> <col width="161" /> <col width="159" /> <col width="167" /> <col width="169" /> <col width="160" /> <col width="145" /> <col width="136" /> </colgroup>
<tbody>
<tr>
<td align="LEFT"></td>
<td align="RIGHT"><span style="font-family: Arial;">10</span></td>
<td align="RIGHT"><span style="font-family: Arial;">50</span></td>
<td align="RIGHT"><span style="font-family: Arial;">100</span></td>
<td align="RIGHT"><span style="font-family: Arial;">200</span></td>
<td align="RIGHT"><span style="font-family: Arial;">500</span></td>
<td align="RIGHT"><span style="font-family: Arial;">1000</span></td>
</tr>
<tr>
<td align="LEFT"><span style="font-family: Arial;">Requests per second</span></td>
<td align="RIGHT"><span style="font-family: Arial;">487.11</span></td>
<td align="RIGHT"><span style="font-family: Arial;">472.09</span></td>
<td align="RIGHT"><span style="font-family: Arial;">442.74</span></td>
<td align="RIGHT"><span style="font-family: Arial;">421.63</span></td>
<td align="RIGHT"><span style="font-family: Arial;">408.11</span></td>
<td align="RIGHT"><span style="font-family: Arial;">326.12</span></td>
</tr>
<tr>
<td align="LEFT"><span style="font-family: Arial;">Time per request</span></td>
<td align="RIGHT"><span style="font-family: Arial;">2.05</span></td>
<td align="RIGHT"><span style="font-family: Arial;">2.12</span></td>
<td align="RIGHT"><span style="font-family: Arial;">2.26</span></td>
<td align="RIGHT"><span style="font-family: Arial;">2.37</span></td>
<td align="RIGHT"><span style="font-family: Arial;">2.45</span></td>
<td align="RIGHT"><span style="font-family: Arial;">3.07</span></td>
</tr>
<tr>
<td align="LEFT"><span style="font-family: Arial;">Transfer rate</span></td>
<td align="RIGHT"><span style="font-family: Arial;">987.91</span></td>
<td align="RIGHT"><span style="font-family: Arial;">955.54</span></td>
<td align="RIGHT"><span style="font-family: Arial;">896.85</span></td>
<td align="RIGHT"><span style="font-family: Arial;">854.31</span></td>
<td align="RIGHT"><span style="font-family: Arial;">826.25</span></td>
<td align="RIGHT"><span style="font-family: Arial;">660.45</span></td>
</tr>
</tbody>
</table>
﻿<img class="alignnone" src="http://farm3.static.flickr.com/2677/4212622846_f6023f9b40_o.png" alt="" width="358" height="334" /></p>

<p>目前对每个请求独立使用JDK的awt实时绘图，吞吐量可以达到400以上，如果稍稍优化一下Jetty的配置，性能还有一定的提升空间。这个结果还是不错的。</p>
