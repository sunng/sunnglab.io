---
layout: post
title: Exaile-doubanfm-plugin for exaile 0.3.2
categories:
- ANN
tags:
- Douban
- exaile
published: true
comments: true
---
<p>Ubuntu 10.04的Exaile还停留在0.3.1，而Fedora 13上已经升级到0.3.2了。为此我更新了豆瓣电台插件，现在这个版本可以在0.3.2上正常工作。</p>

<p>可以到github的下载页面获取 doubanfm-0.0.3a-exaile032.tar.gz
<a href="http://github.com/sunng87/exaile-doubanfm-plugin/downloads">http://github.com/sunng87/exaile-doubanfm-plugin/downloads</a></p>

<p>此外for exaile 0.3.1的版本也有一个优化和bugfix release, Ubuntu用户可以获取<br />
doubanfm-0.0.3-exaile031.tar.gz</p>

<p>接下来三周又有项目了，难度比较大，估计豆瓣插件近期不会有什么新的惊喜了。</p>

<p>The post is brought to you by <a href="http://fedorahosted.org/lekhonee">lekhonee</a> v0.7</p>
