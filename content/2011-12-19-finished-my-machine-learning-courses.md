---
layout: post
title: Finished my machine learning courses
categories:
- 自话
tags:
- course
- ml
published: true
comments: true
---
<p>经过三个月的时间，终于看完了ml-class的所有视频课程，完成了所有review questions，提交了所有programming exercises.感觉不错，之前一直对数据挖掘相关的方面感兴趣，回想一下大学时候一些地统计分析甚至遥感图像数据处理的课都跟机器学习相关，但是毕竟不是这方面的课程，所以介绍的不是很系统。今年秋天斯坦福推出这个在线课程，机器学习作为其中之一真算是弥补了我们民间科学爱好者的遗憾了。</p>

<p>这个课程在有限的篇幅里涵盖了linear regression, logistic regression, ANN, SVM, PCA, K-Means, Anomaly Detection等等知识，基本上算是一个完整实用的导论。Andrew Ng教授的讲解也算是通俗易懂深入浅出，完全感觉不到什么门槛。</p>

<p>对于online course这种形式，今年秋天斯坦福的人工智能、数据库、机器学习也算是首开先河，目前这三门课程都已经结束，网上的反响非常强烈。好消息是明年Q1斯坦福还有更多数量更多方向的课程。今天MIT也宣布了明年的online course计划，他们也将加入提供在线课程的行列。而且，MIT的在线课程还会颁发一个名叫MITx的certification。开放式课程已经成为大势所趋，信息本应自由传播。</p>

<p>对于对机器学习感兴趣的朋友，除了ml-class.org上的资源，你还可以在academic earth上找到ANG教授的授课视频。这套视频涵盖的内容比ml-class上的更详细完整：
<a href="http://academicearth.org/courses/machine-learning" target="_blank">http://academicearth.org/courses/machine-learning</a></p>

<p>课程结束，我在ml-class上所有的编程作业都已经放在bitbucket上，如果有兴趣可以参考这些octave程序：
<a href="https://bitbucket.org/sunng/ml-class" target="_blank">https://bitbucket.org/sunng/ml-class</a></p>

<p>明年一月斯坦福还会开放更多跟机器学习相关的课程，包括：
<ul>
	<li>Probabilistic Graphical Models <a href="http://www.pgm-class.org/" target="_blank">pgm-class.org</a></li>
	<li>Natural Language Processing <a href="http://www.nlp-class.org" target="_blank">nlp-class.org</a></li></ul></p>

<p></p>

<p>Thank you, Professor Ng and your team for this well-prepared, high-quality online course.</p>
