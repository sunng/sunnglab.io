---
layout: post
title: Restore Nautilus to open gnome menu after installed PCManFM
categories:
- 把戏
tags:
- gnome
- linux
- nautilus
published: true
comments: true
---
<p>After installed PCMan File Manager with LXDE,  the gnome "places" menu items were opened by PCManFM as default. To switch back to nautilus, follow these steps:
<ol>
	<li>check <em>/usr/share/applications/nautilus-folder-hanlder.desktop</em>
make sure the line "<em>Exec=nautilus</em>" is not modified. (There are lots of hack article that teach user to change default file manager by modify this)</li>
	<li>check <em>/usr/share/applications/defaults.list</em>
grep the line "x-directory" by
<em>cat /usr/share/applications/defaults.list | grep x-directory </em>
make sure the value was kept as "nautilus-folder-handler.desktop", if not, restore it.<br />
Then grep the line "inode" by
<em>cat /usr/share/applications/defaults.list | grep inode</em>
also be sure about the result "nautilus-folder-handler.desktop". if not, restore it again.</li>
	<li>So it must be your own configuration that has been changed. check <em>~/.local/share/applications/defaults.list</em>
grep the two lines described above, it must be changed to "pcmanfm-folder-handler.desktop", so replace them by "nautilus-folder-handler.desktop"</li>
</ol>
To check the result, click menu item at once (no restart or logout needed), again, you see the slow file manager...</p>
