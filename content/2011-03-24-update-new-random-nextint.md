---
layout: post
title: '"Update ${new Random().nextInt()}"'
categories:
- 当时
tags:
- Life
- Shanghai
published: true
comments: true
---
<p>多少年没写Update体了，看Samson的Update Series都到了53了，咱就只好来这么个标题了。</p>

<p><ol>
<li>头等大事，officially announce 一下艰难决定，经过慎重考虑我决定离开上海了。从09年3月第一次到上海到张江，现在整好两年的时间。一方面是获得了大城市的工作机会，另一方面也被在这里边缘的生活闹够了。于是这个悲喜交加的本命年春天决定告老还乡卸甲归田，过没有追求没有房东的土著生活。</li>
<li>雁过留声踏雪留痕，我在晨晖路1001号半年最大的影响就是让这里又多了两个kindle用户，其他嘛，惭愧了。。。</li>
<li>要说在上海有什么舍不得的话，就是这里的朋友了，同学同事真不少。有一起干活的，有一起吃饭的，有一起听歌的，各种都有，回了南京就没有这种条件了。不过其实大家在上海很忙一个月也聚不上一回，这么想的话没什么区别，赶上什么RubyConf之类的就又过来了，给沪宁线作贡献是咱的宿命。（话说今年的这些会什么时候开啊，我夏天的衣服还没有攒够呢）</li>
<li>新赛季中超3号就揭幕了，今年舜天队的比赛可能连电视转播都没有了，你说不回南京能行吗！！能行吗！！从96年开始，今年就是第16年了。</li>
<li>本来想说最近一两个月荒废得厉害，我回家以后要怎么怎么的，在这还是改为此处省去多少多少字吧。。。</li>
</ol></p>
