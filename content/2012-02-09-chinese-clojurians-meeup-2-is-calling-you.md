---
layout: post
title: 'China Clojurians Meetup #2 is calling you'
categories:
- 当时
tags:
- Beijing
- clojure
published: true
comments: true
---
<p>大家期待已久的第二次Clojure中国用户聚会来了，这次聚会将在三月初的北京举行。感兴趣的朋友请猛击<a href="http://www.diaochapai.com/survey/312738f0-cab0-49b0-9f48-18803f79ddf4" target="_blank">这里</a>报名。如果你有任何关于Clojure的心得，都欢迎加入分享，不要害羞～和上次一样本人会继续抛砖继续不害羞，介绍我的RPC框架Slacker。</p>

<p>For English readers:<br />
If you are interested in Clojure and also living in Beijing, feel free to join our meetup. Please register <a href="http://www.diaochapai.com/survey/312738f0-cab0-49b0-9f48-18803f79ddf4" target="_blank">here</a>. We will see you there!
</p>
