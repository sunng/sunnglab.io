---
layout: post
title: Blackboard Pattern
categories:
- 手艺
tags:
- pattern
published: true
comments: true
---
<p>昨天很虔诚地在图书馆把POSA第四卷里所有的Pattern Languages都抄了一遍。恰好遇到Modeling部分的第9个Pattern， Blackboard，没有感性认识。今天简单地搜索了一下。</p>

<p>POSA上指出Blackboard模式用于解决尚没有绝对解决方案的问题，将几个子系统的知识组合起来提供一个近似的解决方案。在解决这类问题的过程中，几个子系统相互独立，Blackboard作为中央控制对子系统进行控制和反馈。在这个系统中有三个角色：
<ul>
<li>Control Shell: 系统对外的控制接口</li>
<li>Blackboard: 中央的知识管理，接受Specialist运算的反馈，对Specialist进行调度</li>
<li>Specialist: 具有专业知识的子系统，负责独立的运算</li>
</ul></p>

<p>这里有一个描述Blackboard模式的代码例子：
<a href="http://www.andypatterns.com/index.php/blog/blackboard_architectural_pattern/">http://www.andypatterns.com/index.php/blog/blackboard_architectural_pattern/</a></p>

<p>参考：<br />
http://www.vico.org/pages/PatronsDisseny/Pattern%20Blackboard/<br />
http://en.wikipedia.org/wiki/Blackboard_system<br />
http://chat.carleton.ca/~narthorn/project/patterns/BlackboardPattern-display.html</p>
