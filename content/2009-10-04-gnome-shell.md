---
layout: post
title: Gnome Shell
categories:
- 装备
tags:
- gnome
- linux
- ubuntu
published: true
comments: true
---
<p>续昨，好吧，这个无聊的国庆假期就献给karmic了。gnome-shell已经可以在软件仓库里找到了，apt-get安装即可。要使用gnome-shell可以执行
<em>gnome-shell --replace</em></p>

<p>要把gnome-shell作为默认的窗口管理器，打开gnome的配置编辑器，找到/desktop/gnome/session/required_components，把panel和windown-manager全部改成gnome-shell，重新登录即可。</p>

<p>上个图：
<img class="alignnone" src="http://pic.yupoo.com/classicning/0026182bb7d6/medium.jpg" alt="" width="500" height="313" /></p>

<p>当然了，还没有正式release，大家试用尝鲜注意安全第一。</p>
