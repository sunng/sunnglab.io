---
layout: post
title: 'GPS Track of Train K221: Nanjing to Guangzhou'
categories:
- 自话
tags:
- GIS
- gps
published: true
comments: true
---
<p>GPS Logger完成了它有生以来最长的一次旅行，1477分钟，行程1767公里，途径江苏、安徽、江西、湖南、广东，包含了宁芜线、浙赣线、京广线等。有了GPS终端，贴地的旅途就变成一件非常严肃的事情。这周末去西安，这个GPS也是16个小时上铺车程唯一的安慰了。
<a href="http://www.flickr.com/photos/40741608@N08/5657757338/" title="Railway-K221 - Viking by 贝小塔, on Flickr"><img src="http://farm6.static.flickr.com/5305/5657757338_9bbcc6af42.jpg" alt="Railway-K221 - Viking" height="302" width="500" /></a></p>

<p>这个数据分享在OpenStreetMap上，也许你会感兴趣：
<a href="http://www.openstreetmap.org/user/Sunng/traces/996794">http://www.openstreetmap.org/user/Sunng/traces/996794</a><br /><br /><div class="zemanta-pixie"><img class="zemanta-pixie-img" alt="" src="http://img.zemanta.com/pixy.gif?x-id=e4cdedff-8fec-852d-876b-830e16846036" /></div></p>
