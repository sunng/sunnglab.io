---
layout: post
title: '[ANN] Handlebars Clojure API'
categories:
- ANN
tags:
- clojure
published: true
comments: true
---
<p>The ONLY real-world modern clojure templating system. <br />
I just can't believe that the clojure world doesn't build web application with server-side template.<br />
hiccup and enlive are neither **real-world**. So people tends to use single page architecture for a clojure backend. That's PAINFUL.</p>

<p>Available on clojars [hbs "0.4.1"], code and docs on <a href="https://github.com/readwise/hbs" target="_blank">github</a>. We have been using hbs on <a href="http://readwise.net" target="_blank">readwise.net</a> for a long time.</p>

<p></p>
