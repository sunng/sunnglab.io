---
layout: post
title: 从GNOME网站安装exaile-doubanfm-gnome-shell-extension
categories:
- 装备
tags:
- gnome
- linux
- project
published: true
comments: true
---
<p>最近GNOME发布了期待已久的extension.gnome.org，这个网站允许你直接通过浏览器安装和管理gnome-shell扩展，有点类似app store的感觉，混乱的~/.local/share/gnome-shell/extensions/终于有了一个官方的界面。</p>

<p>网站开通的第一时间，我提交了exaile-doubanfm-gnome-shell-extension，经过review和修改，这个扩展也得到了进一步的完善，适配了gnome-shell 3.2的风格。</p>

<p>你可以从这个地址直接安装启用
<a href="https://extensions.gnome.org/extension/24/exaile-doubanfm-control/" target="_blank">https://extensions.gnome.org/extension/24/exaile-doubanfm-control/</a></p>

<p>它会在exaile douban.fm启动后显示一个菜单在gnome-shell上，你可以通过这个菜单进行基本的操作。</p>

<p>如果喜欢，别忘了在extension.gnome.org上vote一下 ：）</p>
