---
layout: post
title: Artificial301 Firefox Addon 1.1
categories:
- 装备
tags:
- firefox
- project
published: true
comments: true
---
<p>由于一些众所周知的原因，一些链接仅仅是因为feedproxy.google.com这样的链接不能打开而无法访问（链接本身是可以访问的）。这个时候你需要Firefox插件：Artificial301。
<a href="http://www.flickr.com/photos/40741608@N08/5191869453/" title="reload with artificial301 by 贝小塔, on Flickr"><img src="http://farm5.static.flickr.com/4090/5191869453_a26474c5d8.jpg" width="500" height="313" alt="reload with artificial301" /></a></p>

<p>本次更新的1.1版本，新增了“Reload With Artificial301”的选项。</p>

<p>下载页面：
<a href="https://github.com/sunng87/Artificial301/downloads">https://github.com/sunng87/Artificial301/downloads</a></p>
