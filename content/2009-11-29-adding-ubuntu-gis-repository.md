---
layout: post
title: Adding Ubuntu-GIS repository
categories:
- 把戏
tags:
- GIS
- ubuntu
published: true
comments: true
---
<p>在Ubuntu上安装GIS软件，可以添加这个PPA源：<em>
deb http://ppa.launchpad.net/ubuntugis/ubuntugis-unstable/ubuntu karmic main</em></p>

<p>导入GPG KEY
<em>gpg --keyserver subkeys.pgp.net --recv 089EBE08314DF160<br />
gpg --export --armor 089EBE08314DF160 | sudo apt-key add -</em></p>

<p>这个源中目前包含了最新版本的GIS软件：
<ul>
	<li>gdal 1.6</li>
	<li>grass</li>
	<li>qgis</li>
</ul></p>
