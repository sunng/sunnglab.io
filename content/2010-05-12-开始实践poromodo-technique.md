---
layout: post
title: 开始实践Poromodo Technique
categories:
- 把戏
tags:
- work
published: true
comments: true
---
<p>看了@juvenxv的博客介绍<a href="http://book.douban.com/subject/4199701/">Poromodo Technique Illustrated</a>一书，结合我现在低下的工作效率，我决定实践一下这个方法。
<ul>
	<li>一张TODO表记录今天的计划</li>
	<li>一张Activity Inventory表用于记录长期的任务，标记已完成的任务</li>
	<li>一张Record表统计每天完成的任务数量</li>
</ul></p>

<p>一个Poromodo持续25分钟，在这25分钟内只专注当前任务。Poromodo间隙用来给大脑做缓冲，清空前一个任务的影响，以便下一个Poromodo开始时快速切换到相应任务。</p>

<p>应对Interruption，对internal的accept, record & continue，一个poromodo作为一个原子单元不可被打断，一旦被打断应该重新开始。<br />
对external的，评估重要性，分别拒绝、延后、安排时间、安排回调。</p>
