---
layout: post
title: Using MySQL on ArchLinux
categories:
- 手艺
- 装备
tags:
- ArchLinux
- linux
- mysql
published: true
comments: true
---
<p><ol>
	<li><em>su -</em></li>
	<li><em>pacman -S mysql mysql-gui-tools
</em></li>
	<li>run <em>mysql_install_db --user=mysql</em> in <em>/var/lib/mysql</em></li>
	<li><em>vim /etc/my.cnf</em>, disable <em>skip_networking </em>to allow tcp/ip connection<em>
</em></li>
	<li><em>vim /etc/hosts.allow</em>, add <em>mysql:all:allow</em></li>
	<li><em>/etc/rc.d/mysql start</em></li>
	<li><em>mysqladmin -u root password #NEW_PASSWORD#</em> to set root password<em>
</em></li>
</ol>
<div id="_mcePaste" style="overflow: hidden; position: absolute; left: -10000px; top: 0px; width: 1px; height: 1px;">
<ol>
	<li></li>
	<li><em>su -</em></li>
</ol>
</div></p>
