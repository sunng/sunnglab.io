---
layout: post
title: RDF, Sparql query with python
categories:
- 手艺
tags:
- python
- rdf
published: true
comments: true
---
<p>A simple query to test <a href="http://en.wikipedia.org/wiki/Resource_Description_Framework">rdf</a> and <a href="http://en.wikipedia.org/wiki/Sparql">sparql</a>.
<pre>[codesyntax lang="python"]
import sys
import rdflib
from rdflib.Graph import ConjunctiveGraph</pre></p>

<p>g = ConjunctiveGraph()</p>

<p>g.parse(sys.argv[1], format="xml")</p>

<p>fbns = rdflib.Namespace("http://rdf.freebase.com/ns/")</p>

<p>player_refs = g.query("""SELECT ?player<br />
        WHERE {<br />
            ?root fb:soccer.football_roster_position.player ?player .<br />
        }""", initNs={'fb':fbns})<br />
players = []<br />
for player in player_refs:<br />
    gp = ConjunctiveGraph()<br />
    gp.parse(player[0], format="xml")<br />
    results = gp.query("""SELECT ?player_name ?player_position<br />
        WHERE {<br />
            ?player fb:type.object.name ?player_name .<br />
            ?player fb:soccer.football_player.position_s ?player_position .<br />
        }""", initNs={'fb':fbns})<br />
    l = list(results)<br />
    for r in l:<br />
        if r[0].language == 'en':<br />
            players.append(r)</p>

<p>for player in players:<br />
    print player</p>

<p>[/codesyntax]
The query will return all player-position tuple from freebase's db for specified resource.<br />
To query for players of AC Milan, execute the script with resource location:
<em>python rdfquery.py http://rdf.freebase.com/rdf/en/ac_milan</em>
got:
<em>(rdflib.Literal('Yoann Gourcuff', language=u'en', datatype=None), rdflib.URIRef('http://rdf.freebase.com/ns/en.midfielder'))<br />
(rdflib.Literal('Andrea Pirlo', language=u'en', datatype=None), rdflib.URIRef('http://rdf.freebase.com/ns/en.midfielder'))<br />
(rdflib.Literal('Paolo Maldini', language=u'en', datatype=None), rdflib.URIRef('http://rdf.freebase.com/ns/en.defender'))<br />
(rdflib.Literal('Gianluca Zambrotta', language=u'en', datatype=None), rdflib.URIRef('http://rdf.freebase.com/ns/en.defender'))<br />
(rdflib.Literal('Davide Facchin', language=u'en', datatype=None), rdflib.URIRef('http://rdf.freebase.com/ns/guid.9202a8c04000641f8000000000c75e75'))<br />
(rdflib.Literal('Marcus Plinio Diniz Paixao', language=u'en', datatype=None), rdflib.URIRef('http://rdf.freebase.com/ns/en.defender'))<br />
...</em></p>

<p>All resources are stored as rdf:resource references. If you are also Milan fan like me, take a quick look at the players, well, the data is not up to date:)</p>

<p>I have also tried to fetch data for my favorite local team.
<em>python rdfquery.py http://rdf.freebase.com/rdf/en/jiangsu_shuntian<br />
(rdflib.Literal('Yao Hanlin', language=u'en', datatype=None), rdflib.URIRef('http://rdf.freebase.com/ns/en.midfielder'))</em></p>
