---
layout: post
title: Slacker slides on cnclojure meetup
categories:
- 手艺
tags:
- slacker
published: true
comments: true
---
<p>从北京回来两天了，稍微有点累，还没来得及总结一下，先把slides上传分享一下： <a href="http://www.box.com/s/k0alcj1p115jq40bdkik" target="_blank">http://www.box.com/s/k0alcj1p115jq40bdkik</a></p>

<p>解压之后 lein deps && lein run 一下即可。</p>

<p>这是一个借助impress.js制作的幻灯片。之所以让它跑在webbit里，是因为我简单hack了impress.js让你可以通过websocket远程控制幻灯片播放。这样，就可以通过手机上的firefox浏览器（目前android上仅有firefox支持websocket）控制播放，把手机变成一款遥控器。</p>

<p></p>
