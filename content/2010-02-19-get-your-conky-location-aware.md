---
layout: post
title: Get your conky location aware
categories:
- 装备
tags:
- conky
- GIS
- linux
- location
published: true
comments: true
---
<p><a href="http://www.flickr.com/photos/40741608@N08/4370628754/" title="2010-02-19-225826_188x44_scrot by 贝小塔, on Flickr"><img src="http://farm5.static.flickr.com/4014/4370628754_5a22522b80_o.png" width="188" height="44" alt="2010-02-19-225826_188x44_scrot" /></a></p>

<p>Add this in your conkyrc<br />
[cc lang="text" nowrap="0"]<br />
${exec curl -s "http://api.hostip.info" | xpath -e "//gml:featureMember/Hostip/gml:name/text()" -q} ${exec curl -s "http://api.hostip.info" | xpath -e "//gml:featureMember/Hostip//gml:coordinates/text()" -q}<br />
[/cc]</p>

<p>Hostip is well known as a service provider of the geoclue framework. It translates IP address to geolocation information. The API we use will return a GML document like<br />
[cc lang="xml" nowrap="0"]
<?xml version="1.0" encoding="ISO-8859-1"?>
<hostiplookupresultset version="1.0.1" xmlns:gml="http://www.opengis.net/gml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nonamespaceschemalocation="http://www.hostip.info/api/hostip-1.0.1.xsd">
 <gml:description>This is the Hostip Lookup Service</gml:description>
 <gml:name>hostip</gml:name>
 <gml:boundedby>
  <gml:null>inapplicable</gml:null>
 </gml:boundedby>
 <gml:featuremember>
  <hostip>
   <ip>58.212.88.212</ip>
   <gml:name>Nanjing</gml:name>
   <countryname>CHINA</countryname>
   <countryabbrev>CN</countryabbrev>
   <!-- Co-ordinates are available as lng,lat -->
   <iplocation>
    <gml:pointproperty>
     <gml:point srsname="http://www.opengis.net/gml/srs/epsg.xml#4326">
      <gml:coordinates>118.883,32.05</gml:coordinates>
     </gml:point>
    </gml:pointproperty>
   </iplocation>
  </hostip>
 </gml:featuremember>
</hostiplookupresultset>
[/cc]</p>

<p>Because it uses ip to lookup your address, you cannot expect higher resolution and precision currently.</p>
