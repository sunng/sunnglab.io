---
layout: post
title: gefr API updates
categories:
- ANN
tags:
- gefr
- java
- jython
- opensource
- project
- python
- wsgi
published: true
comments: true
---
<p>早上收到<a href="http://jythonet.appspot.com">jythonet</a>作者的一封邮件，受到启发，我打算扩展一下gefr这个WSGI adapter。原本gefr是写给soldat用来测试的，结果发现现在出现了买椟还珠的效果，既然大家更加关注这个，我决定多花一些时间在上面。不过话说回来gefr确实是Jython Web程序的新思路，过去大家都在想怎样把Jython放进Servlet中，gefr的思路是把Java的服务器实现放到WSGI后面。未来会有gefr-netty, gefr-mina, gefr-servlet等等出现，如果真的存在一个可靠的backend，也许这种思路也不失为一个办法，毕竟python的web框架更吸引人，而java的基础设施相对可靠，各取所长。</p>

<p>根据这个目的，现在gefr 0.2的API更新为：
<script src="https://bitbucket.org/sunng/gefr/src/8490748055f1/src/examples/example.py?embed=t"></script></p>

<p>即在创建Gefr的时候需要告知backend类型，目前还只有soldat一种后端。未来也可能通过自动检测让这个参数成为可选。</p>

<p>gefr 0.2已经在开发中，您可以通过这里关注：
<a href="https://bitbucket.org/sunng/gefr/overview">https://bitbucket.org/sunng/gefr/overview</a></p>
