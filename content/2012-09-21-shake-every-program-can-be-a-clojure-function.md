---
layout: post
title: 'Shake: Every Program Can Be a Clojure Function'
categories:
- ANN
tags:
- clojure
- github
- linux
published: true
comments: true
---
<p>You might have heard of <a href="http://amoffat.github.com/sh/index.html">sh</a>, which brings python an interface to call subprocesses. The API of sh is pretty cool: Every command can be treated as a python function, and imported from a namespace. Options and arguments are passed in as python string.</p>

<p>But I think in Clojure, things can be even cooler. We dynamically create symbols for every program. We will have a beautiful DSL so you don't have to quote arguments as string. So when you are using this library, it may look like:</p>

<p>[cc lang="clojure"]<br />
(ls)<br />
(uname -a)<br />
(ip -4 addr)<br />
[/cc]</p>

<p>And actually it's just like that! I create this library called <strong><a href="https://github.com/sunng87/shake/" target="_blank">shake</a></strong>. When you load `shake.core`, it indexes all the executables in your path. Then all programs are available to you in a clojure native way.</p>

<p>[cc lang="clojure"]</p>

<p>(use 'shake.core)<br />
(uname -a) ;; returns a java.lang.Process, that you can send data, read data and wait for termination.</p>

<p>;; for those just need output<br />
(alter-var-root *print-outpt* (fn [_] true))<br />
(uname -a)<br />
;; it prints ...<br />
[/cc]</p>

<p>There's a lot of fun in implementing this library. First, to be able to use custom symbol in the DSL, you have to make these executables as macros. Second, find a way to programmably create vars which are named by string. The power of Clojure enables all the ideas and makes it possible. Check out the source code if you are interested in: <a href="https://github.com/sunng87/shake/">https://github.com/sunng87/shake/</a>. </p>

<p></p>
