---
layout: post
title: Lily Studio New Official Site
categories:
- 自话
tags:
- friends
- LilyStudio
published: true
comments: true
---
<p>江山带有人才出啊，自从看到<a href="http://remember.yo2.cn/" target="_parent">Remember2015</a>和<a href="http://www.lightory.net/" target="_parent">Lightory</a>这么伟岸的人才们之后，我都不敢想像后面的人再出来以后我上哪蹲着去了。</p>

<p>隆重推出工作室的全新官网：
<a href="http://www.lilystudio.org">http://www.lilystudio.org/</a>
如果没有搞错的话，美工Remember2015，编辑Lightory，这个组合太强大了，嗯。</p>

<p>Any feedback? Try http://www.lilystudio.org/bbs/</p>

<p>最后引用何勇的一句名言：感谢大家的工作！</p>
