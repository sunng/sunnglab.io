---
layout: post
title: Installing Weather Indicator on Maverick
categories:
- 手艺
tags:
- ubuntu
published: true
comments: true
---
<p>The appindicator has been enhanced again in Ubuntu 10.10 Maverick. The new datetime indicator has been introduced in the replace Gnome datetime applet. However, the datetime indicator is quite simple, which drops support for EDS (Evolution data server), location and weather. So you need an indicator api based weather widget, and <a href="http://www.omgubuntu.co.uk/2010/06/weather-indicator-applet-genesis-of-an-itch-into-an-app-updated-with-ppa/">omgubuntu</a> says there is already such a project of <a href="https://launchpad.net/weather-indicator">weather-indicator</a>, on launchpad.</p>

<p><a href="http://www.flickr.com/photos/40741608@N08/5125055279/" title="indicator-weather by 贝小塔, on Flickr"><img src="http://farm5.static.flickr.com/4145/5125055279_19997d4c15_m.jpg" width="200" height="240" alt="indicator-weather" /></a></p>

<p>However, any attempt to install this package will result in a failure of 404. The ppa is no long maintained (Even the project has been in silent for a long time.) Fortunately, we can still build it from sources.</p>

<p>First, check out the sources with bzr:
<i>bzr branch lp:weather-indicator</i></p>

<p>Then, change directory into <i>weather-indicator</i>, package it with debuild:
<i>debuild binary</i></p>

<p>You will get the result in the parent directory:
<i>indicator-weather_10.07.16_all.deb</i></p>

<p>Then you simply install the debian package with dpkg:
<i>sudo dpkg -i indicator-weather_10.07.16_all.deb</i></p>

<p>Enjoy !</p>
