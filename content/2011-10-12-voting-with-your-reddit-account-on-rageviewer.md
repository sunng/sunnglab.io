---
layout: post
title: Voting with your reddit account on RageViewer
categories:
- ANN
tags:
- clojure
- project
- reddit
published: true
comments: true
---
<p>I just implemented a new feature on <a href="http://rageviewer.cloudfoundry.com/index.html" title="rage viewer" target="_blank">RageViewer</a> which has been requested for a long time. Now you can login with your Reddit account and up-vote/down-vote rages without turning to Reddit website.</p>

<p>Again, I made a rage comic for this feature.
<img src="http://i.imgur.com/Zg9TU.jpg" alt="voting rage" /></p>

<p>To be able to vote items with your Reddit account, a Reddit session is maintained in the memory of RageViewer. Rage Viewer is a pure open source software so it will never store or log your password in any form. You can verify the source code on <a href="https://github.com/sunng87/rageviewer">github</a> if you have concern about the privacy. (Currently, Reddit doesn't offer any authorization like OAuth so we can only access your reddit account with your password. Sorry for that.)</p>

<p>For those still have no idea about RageViewer. RageViewer is for browsing rage comics smoothly. It loads most recent comics from <a href="http://www.reddit.com/r/fffffffuuuuuuuuuuuu/" target="_blank">fffffffuuuuuuuuuuuu</a> and some other subreddits into the database. It gives you a modern and clean UI for your best experience.</p>

<p>Also important, RageViewer is a full-stack clojure web application that written in the JVM functional language: Clojure(server-side) and ClojureScript(broser-side).</p>

<p>BTW, a new version of Reddit.clj (0.3.0) is available in clojars repository. (the new RageViewer is also based on it). In 0.3.0, reddit.clj is compatible with Clojure 1.3.0. And a "enhance" function is removed that you are no long required to "enhance" a reddit client before you write data to Reddit. Now you have read-write permission once you create the client with reddit.clj.core/login .</p>

<p></p>
