---
layout: post
title: HeatCanvas support for Leaflet
categories:
- ANN
tags:
- GIS
- heatcanvas
- javascript
- map
published: true
comments: true
---
<p><a href="http://leaflet.cloudmade.com/" title="leaflet" target="_blank">Leaflet</a> is a light weight web mapping library developed by <a href="http://cloudmade.com/" title="cloudmade" target="_blank">Cloudmade</a>. Leaflet is designed for compatibility with both desktop browser and mobile browser.</p>

<p>HeatCanvas-Leaflet extension enables heat map on Leaflet. You can create heat map layer and add it to Leatlet map:<br />
[cc lang="javascript"]<br />
var heatmap = new L.TileLayer.HeatCanvas("Heat Canvas", map, {},<br />
                        {'step':0.3, 'degree':HeatCanvas.QUAD, 'opacity':0.7});<br />
heatmap.pushData(32.1104, 118.0852, 14);<br />
//push more data ...<br />
map.addLayer(heatmap);<br />
[/cc]</p>

<p>You can find live demo on:
<a href="http://sunng87.github.com/heatcanvas/leaflet.html" title="heatcanvas demo" target="_blank">http://sunng87.github.com/heatcanvas/leaflet.html</a>
Mobile browser is also supported. (Tested on Firefox Android and default Android browser)</p>

<p>Find more about HeatCanvas on <a href="https://github.com/sunng87/heatcanvas">github page</a>.</p>

<p>Leaflet is still buggy for extension. There is a latlng-pixel coordinate conversion <a href="https://github.com/CloudMade/Leaflet/issues/190" target="_blank">issue</a> in low zoom level, affects this demo. Hope it could be fixed soon.</p>
