---
layout: post
title: 'Douban on Gwibber #2'
categories:
- ANN
tags:
- Douban
- gwibber
- ubuntu
published: true
comments: true
---
<p><a href="http://www.flickr.com/photos/40741608@N08/5058425003/" title="Gwibber douban by 贝小塔, on Flickr"><img src="http://farm5.static.flickr.com/4145/5058425003_6bd9c4b6e7.jpg" width="397" height="500" alt="Gwibber douban" /></a></p>

<p>Douban support on gwibber, tested on Gwibber <strong>2.32.0-ubuntu2</strong> or <strong>2.32.0.1-ubuntu1</strong> , Ubuntu Maverick.</p>

<p>You can download it from bitbucket:
<a href="http://bitbucket.org/sunng/gwibber-douban/downloads">http://bitbucket.org/sunng/gwibber-douban/downloads</a></p>

<p>Installation:<br />
Unpack and run install script with super user privilege:
<i>sudo bin/install.sh</i></p>

<p>We will create backup for you. To rollback, just run <i>sudo bin/uninstall.sh</i> .</p>

<p>Any feedback is welcome.</p>
