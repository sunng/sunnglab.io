---
layout: post
title: 给 Raspberry Pi 添加一个显示 IP 的液晶屏
categories:
- 把戏
tags:
- raspberry_pi
published: true
comments: true
---
<p>我的 Raspberry Pi 在家里一直是通过 DHCP 联网的，每次要登录上去都要先进路由管理界面看看他的 IP 到底是什么，很不方便。于是就买了这块液晶屏幕，准备显示一下 IP，另外以后也可以作为一个输出设备。毕竟用这个比外接一个显示器方便多了（现在显示器最小都是21寸）。</p>

<p>液晶屏就是最普通的1602屏幕，淘宝上有很多，基本上都一样。要注意的是需要自备引脚，要自己焊到板上，否则无法连接。我也是第一回焊，感觉还比较简单。(<a href="http://instagram.com/p/bIPSEcBM6R/" target="_blank">如图。</a>)另外为了控制液晶屏的对比度需要有一个电位器，为此我中途还专门跑了一趟中关村。</p>

<p>之后就可以按照 <a href="http://learn.adafruit.com/drive-a-16x2-lcd-directly-with-a-raspberry-pi/overview" target="_blank">Adafruit 上的教程</a>一步一步做了。</p>

<p>最后我简化了一下显示 IP 的脚本，时间就不去刷新了。
<script src="https://gist.github.com/sunng87/5890563.js"></script></p>

<p>把这个脚本作为 systemd 的服务，可以参考<a href="http://blog.sdbarker.com/adding-custom-units-services-to-systemd-on-arch-linux/" target="_blank">这里</a>有关如何创建自定义服务的说明。然后通过systemctl enable ip-display.service设置自启动。</p>

<p>最终效果如下：
<img src="http://i.imgur.com/ZqAc2dh.jpg" alt="my ip display setup" /></p>

<p></p>
