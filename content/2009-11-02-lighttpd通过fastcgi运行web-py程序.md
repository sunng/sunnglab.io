---
layout: post
title: Lighttpd通过FastCGI运行web.py程序
categories:
- 手艺
tags:
- fastcgi
- lighttpd
- python
- web.py
published: true
comments: true
---
<p><a href="http://webpy.org/" target="_blank">web.py</a>是个很小的python框架，特点就是小，连session都没有实现人家就发布了。前几天KungfuRails大会上他们吹牛说<a href="http://www.sinatrarb.com" target="_blank">Sinatra</a>可以写出世界上最小的Webapp，但是有web.py的化，那个最小至少要加上个“之一”。</p>

<p>Web.py在Lighttpd上通过fastcgi运行的配置，可以在web.py的网站上找到文档：
<a href="http://webpy.org/cookbook/fastcgi-lighttpd">http://webpy.org/cookbook/fastcgi-lighttpd</a></p>

<p>实际我用的时候把静态的index.html用作首页，稍改动一下：
<pre class="brush:plain">server.document-root = "/home/sun/projects/sdostatweb"</pre></p>

<p>server.modules   += ( "mod_fastcgi" )<br />
server.modules   += ( "mod_rewrite" )</p>

<p>server.port = 4000</p>

<p>mimetype.assign = (<br />
    ".html" =&gt; "text/html"<br />
)</p>

<p>index-file.names = ( "index.html" )</p>

<p>fastcgi.server = ( "/sdostatweb.py" =&gt;<br />
(( "socket" =&gt; "/tmp/fastcgi.socket",<br />
        "bin-path" =&gt; "/home/sun/projects/sdostatweb/sdostatweb.py",<br />
        "max-procs" =&gt; 5,<br />
        "bin-environment" =&gt; (<br />
        "REAL_SCRIPT_NAME" =&gt; ""<br />
    ),<br />
        "check-local" =&gt; "disable"<br />
    ))<br />
)</p>

<p>url.rewrite-once = (<br />
    "^/$" =&gt; "/static/index.html",<br />
    "^/flotr/(.*)$" =&gt; "/static/flotr/$1",<br />
    "^/static/(.*)$" =&gt; "/static/$1",<br />
    "^/(.*)$" =&gt; "/sdostatweb.py/$1",</p>

<p>)
另外用fastcgi的方式使用web.py，需要安装flup。</p>

<p>详细的事，可以看看新浪的Tim Yang的优化：
<a href="http://timyang.net/python/python-webpy-lighttpd/">http://timyang.net/python/python-webpy-lighttpd/</a></p>

<p>我之前真不敢相信搜狐Mail是跑在web.py上。</p>
