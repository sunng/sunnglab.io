---
layout: post
title: Alexandria with Douban support
categories:
- 装备
tags:
- opensource
published: true
comments: true
---
<p>迟到的新闻了，5月8号，GNOME桌面的书籍管理程序Alexandria发布了0.6.6 Beta2。上个月底我提交的豆瓣支持已经被加入了标准发布的打包里，这里可以看到Release note
<a href="http://alexandria.rubyforge.org/news/2010-05-08--0.6.6beta2-released.html">http://alexandria.rubyforge.org/news/2010-05-08--0.6.6beta2-released.html</a></p>

<p>最近半年忙里偷闲给开源软件做一点贡献，上次<a href="http://live.gnome.org/DeskbarApplet/Extending">gnome deskbar wiki</a>名字是自己加上去的，这次在alexandria里终于算是得到了一些认可，再接再厉，嗯！</p>

<p>各位用开源桌面的读者是不是看在这个份上也装一下这个软件啊。。。</p>
