---
layout: post
title: slacker 0.4.0 released
categories:
- ANN
tags:
- clojure
- project
- slacker
published: true
comments: true
---
<p>Slacker 0.4.0 has been released to clojars.org . There are new features and breaking changes in this release.</p>

<p><h3>Breaking Changes</h3></p>

<p><ul>
	<li>New maven coordinator: [slacker "0.4.0"] (groupId renamed to slakcer)</li>
	<li>defremote renamed to <strong>defn-remote</strong></li>
	<li>SlackerException removed. slacker now uses slingshot for exception handling</li>
	<li>Rename :async option of defn-remote to <strong>:async?</strong></li></ul></p>

<p></p>

<p><h3>What's new in 0.4.0</h3>
<ul>
	<li>Add new serialization type <strong>:clj</strong></li>
	<li>New interceptors: execution time stats, args logger, slow watch dog</li>
	<li>New HTTP interface</li>
	<li>Server inspect commands	</li></ul></p>

<p><li> utility functions/macros <strong>defn-remote-all</strong>, <strong>defn-remote-batch</strong> and <strong>meta-remote</strong></li></p>

<p></p>

<p>Get more information on <a href="https://github.com/sunng87/slacker" target="_blank">github</a>.
</p>
