---
layout: post
title: fixing "libmozjs" missing when using mongodb on Ubuntu lucid
categories:
- 把戏
tags:
- mongodb
- ubuntu
published: true
comments: true
---
<p><strong>Problem</strong>
When running mongod/mongo/mongos, you got message like this:
<i>mongod: error while loading shared libraries: libmozjs.so: cannot open shared object file: No such file or directory</i></p>

<p><strong>Solution</strong>
Make sure you have xulrunner-dev installed:
<em>sudo apt-get install xulrunner-dev</em></p>

<p>then find libmozjs on your filesystem:
<em>sudo locate libmozjs</em></p>

<p>in lucid, it's supposed to locate at:
<em>/usr/lib/xulrunner-1.9.2.6/libmozjs.so</em></p>

<p>(and some other directories, such as firefox / thunderbird / seamonkey)</p>

<p>Just create a symbol link:
<em>sudo ln -s /usr/lib/xulrunner-1.9.2.6/libmozjs.so /usr/lib/</em></p>

<p>try to restart mongodb:
<em>sudo service mongodb start</em></p>

<p>take a look at process list:
<em>ps aux | grep mongo</em></p>

<p>it works.</p>

<p>The post is brought to you by <a href="http://fedorahosted.org/lekhonee">lekhonee</a> v0.7</p>
