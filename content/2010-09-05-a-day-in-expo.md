---
layout: post
title: A day in Expo
categories:
- 留影
tags:
- D60
- Shanghai
published: true
comments: true
---
<p>A day in Expo, Shanghai.</p>

<p><a href="http://www.flickr.com/photos/40741608@N08/4957383122/" title="DSC_0015 by 贝小塔, on Flickr"><img src="http://farm5.static.flickr.com/4133/4957383122_818b9af46c.jpg" width="500" height="335" alt="DSC_0015" /></a></p>

<p><a href="http://www.flickr.com/photos/40741608@N08/4957392860/" title="DSC_0017 by 贝小塔, on Flickr"><img src="http://farm5.static.flickr.com/4076/4957392860_4249a043bc.jpg" width="500" height="335" alt="DSC_0017" /></a></p>

<p><a href="http://www.flickr.com/photos/40741608@N08/4956812599/" title="DSC_0023 by 贝小塔, on Flickr"><img src="http://farm5.static.flickr.com/4110/4956812599_fdd0e0b035.jpg" width="335" height="500" alt="DSC_0023" /></a></p>

<p><a href="http://www.flickr.com/photos/40741608@N08/4957416828/" title="DSC_0025 by 贝小塔, on Flickr"><img src="http://farm5.static.flickr.com/4096/4957416828_7d74399e72.jpg" width="500" height="335" alt="DSC_0025" /></a></p>

<p><a href="http://www.flickr.com/photos/40741608@N08/4957437892/" title="DSC_0026 by 贝小塔, on Flickr"><img src="http://farm5.static.flickr.com/4108/4957437892_5b92d804fd.jpg" width="500" height="335" alt="DSC_0026" /></a></p>

<p><a href="http://www.flickr.com/photos/40741608@N08/4957449690/" title="DSC_0028 by 贝小塔, on Flickr"><img src="http://farm5.static.flickr.com/4091/4957449690_22357b0ffc.jpg" width="500" height="335" alt="DSC_0028" /></a></p>

<p><a href="http://www.flickr.com/photos/40741608@N08/4958948524/" title="DSC_0032 by 贝小塔, on Flickr"><img src="http://farm5.static.flickr.com/4090/4958948524_08095c93cc.jpg" width="335" height="500" alt="DSC_0032" /></a></p>

<p><a href="http://www.flickr.com/photos/40741608@N08/4958953244/" title="DSC_0035 by 贝小塔, on Flickr"><img src="http://farm5.static.flickr.com/4086/4958953244_9d9dfa3fc3.jpg" width="335" height="500" alt="DSC_0035" /></a></p>

<p><a href="http://www.flickr.com/photos/40741608@N08/4958964376/" title="DSC_0053 by 贝小塔, on Flickr"><img src="http://farm5.static.flickr.com/4083/4958964376_b9054d2a3c.jpg" width="500" height="335" alt="DSC_0053" /></a></p>

<p><a href="http://www.flickr.com/photos/40741608@N08/4958390957/" title="DSC_0057 by 贝小塔, on Flickr"><img src="http://farm5.static.flickr.com/4086/4958390957_d62179b219.jpg" width="335" height="500" alt="DSC_0057" /></a></p>

<p><a href="http://www.flickr.com/photos/40741608@N08/4958405807/" title="DSC_0060 by 贝小塔, on Flickr"><img src="http://farm5.static.flickr.com/4094/4958405807_8a0dda2557.jpg" width="500" height="335" alt="DSC_0060" /></a></p>

<p><a href="http://www.flickr.com/photos/40741608@N08/4959010738/" title="DSC_0063 by 贝小塔, on Flickr"><img src="http://farm5.static.flickr.com/4151/4959010738_c5b6da5fe1.jpg" width="500" height="335" alt="DSC_0063" /></a></p>

<p><a href="http://www.flickr.com/photos/40741608@N08/4958468151/" title="DSC_0067 by 贝小塔, on Flickr"><img src="http://farm5.static.flickr.com/4142/4958468151_cf0f270d04.jpg" width="500" height="335" alt="DSC_0067" /></a></p>

<p><a href="http://www.flickr.com/photos/40741608@N08/4958477457/" title="DSC_0069 by 贝小塔, on Flickr"><img src="http://farm5.static.flickr.com/4126/4958477457_3437cbe143.jpg" width="500" height="335" alt="DSC_0069" /></a></p>

<p>早晨6点开始阳光就非常剧烈，不适合拍照，而且大部分时间都用来排队了，只留下这么一点点能看的而已。</p>
