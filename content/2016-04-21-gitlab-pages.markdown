Title: 将网站迁移到 Gitlab Pages
Date: 2016-04-21 22:40
Tags: gitlab, git

前段时间 Github 连续出现负面新闻后，Gitlab 似乎感觉嗅到了机会，作为追
赶者发布了新版本和新功能。[Gitlab Pages](http://pages.gitlab.io/) 就是
他们对应 Github Pages 的产品。

经过一番对比之后，我现在把整个网站从 Github 迁移过来，体验好了不少。

#### HTTPS

首先，Gitlab Pages 原生支持 HTTPS。如果使用 `yourname.gitlab.io` 你会
使用 Gitlab 的 HTTPS 证书。而如果绑定自己的域名你可以使用自己的证书，
比如从 [LetsEncrypt](http://letsencrypt.org/) 获得的证书。而过去在
Github 原生并不支持 HTTPS，过去是通过
[CloudFlare](https://cloudflare.com/) 这样的服务做一个代理。但是
CloudFlare 免费的版本不支持自定义的证书，并且从 CloudFlare 到Github 仍
然是 Plain HTTP。

#### Page Builder

Github Pages 背后是一个默认的 jekyll。在 LetsEncrypt 验证时，要求建立
的 `.well-known` 目录会由于以`.`开头，默认被 jekyll 忽略，需要加上一个
`.nojekyll` 文件告诉 Github 不要用 jekyll 来构建。

相比之下 Gitlab 的构建方式就比较透明，官方的例子包含了市面上几乎所有的
静态网站生成器的例子。而且 Gitlab 是通过他的 Runner 来做构建（和
Github 已经具有完整生态不通，Gitlab 作为 catchup 自己做了一整套周边，
连 badge 都是自己实现的）。Gitlab 共享版本的 Runner 运行在 docker 环境
里，用户几乎可以安装任何工具、指定任意的命令来生成网站，只要保证页面生
成在 `public/` 目录下即可。之后会触发 Gitlab 内部的 builder 将
`public` 目录打包上传到某些神秘的地方，网站就上线了。

Gitlab 的缺点是文档相对差劲一些，比如：

* Runner 的文档比较分散，找 registration token 找了很久。另外比较奇怪
  的是 Runner 的注册必须通过一个本地的命令行工具
  `gitlab-ci-multi-runner` 来注册，而注册之后这个工具基本上就没什么用
  了。
* 设置域名时 Gitlab 对所有域名都提示设置为 CNAME。但是类似 sunng.info
  这样的域名是无法设置为 CNAME 的，这时必须设置为 A 记录，文档上没有说
  明。方法是通过 `dig yourname.gitlab.io` 看一下网站实际的 IP，然后把
  这个 IP 设置为 A 记录的地址。

总而言之瑕不掩瑜，Gitlab 作为后来者一定分析了 Github Pages 使用时的痛
点，想必开发者本身也曾经是 Github 的用户吧。
