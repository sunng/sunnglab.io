---
layout: post
title: OSM Static API with JavaFX
categories:
- 手艺
tags:
- foss
- GIS
- JavaFX
- map
- OpenStreetMap
- RIA
- web
published: true
comments: true
---
<p>OSM(http://www.osm.org) has released their first prototype of static map api. It is aimed to make web mapping easier to refer just like google has done.</p>

<p>All interfaces are listed in following page:
<a href="http://dev.openstreetmap.org/~pafciu17/" target="_blank">http://dev.openstreetmap.org/~pafciu17/</a></p>

<p>With OSM static api, you can:
<ul>
	<li>Show a map centered at a specified point. (using center, height and width)</li>
	<li>Show a map contains specified bound box. (using bbox, zoom/height/width)</li>
	<li>Create point/path/polygon in static map.</li>
	<li>Choose different map render engines.</li>
	<li>Control logo position in output image.</li>
</ul>
I just tested it with a simple javafx applet. For there is no crossdomain.xml on the server of OSM, to get cross-domain images, you will be asked to confirm security. Feel free to change center latitude/longitude and zoom level.</p>

<p><!--more-->
<script src="http://dl.javafx.com/1.2/dtfx.js"></script> <script type="text/javascript">// <![CDATA[<br>
      javafx(         {               archive: "http://www.classicning.com/archives/OSMStaticMap.jar",               width: 500,               height: 500,               code: "OSMStaticMap",               name: "OSMStaticMap"         }     );<br>
// ]]></script></p>

<p>Enjoy all these free data on free applications :)</p>

<p>Steps to create signed javafx applet
<ul>
	<li>javafxc OSMStaticMap.fx</li>
	<li>javafxpackger javafxpackager -src . -appClass OSMStaticMap -appWidth 500 -appHeight 500</li>
	<li>cd dist</li>
	<li>keytool -genkey -keystore classicning.keys -alias  http://www.classicning.com/ -validity 365</li>
	<li>jarsigner --keystore classicning.keys --storepass ******** OSMStaticMap.jar http://www.classicning.com/</li>
</ul></p>
