---
layout: post
title: Lua script for Geany to view manpage of functions
categories:
- 装备
tags:
- c
- foss
- geany
- linux
- lua
published: true
comments: true
---
<p><h3>Requirement</h3>
When editing when Geany, you need some document of system library functions at hand. So you want to browse manpage at any time.</p>

<p><h3>Solution</h3>
Just two lines:<br />
[cc lang="lua"]<br />
sel=geany.selection()<br />
geany.launch('rxvt-unicode', '-e', 'man', sel)<br />
[/cc]
<a href="http://gist.github.com/591279">http://gist.github.com/591279</a></p>

<p><h3>Install</h3>
Make sure you have geany-plugin-lua installed:
<i>sudo apt-get install geany-plugin-lua</i></p>

<p>Create a lua source file named 'ShowMan.lua' in <i>~/.config/geany/plugins/geanylua/</i>  . If the direcotry does not exist, you should create it first.</p>

<p>Copy two lines of lua code into this file, save and close.</p>

<p>Create a file named hotkeys.cfg in <i>~/.config/geany/plugins/geanylua/</i>, add following content:
<i>ShowMan.lua</i></p>

<p>Restart geany, then open Preferences->Keybindings, Lua Script-> ShowMan, bind a key, for example, Alt+m</p>

<p><h3>Usage</h3>
Select the function name, and press Alt+m (or from menu Tools->Lua Scripts->ShowMan), then you see the terminal and the manpage.</p>

<p><a href="http://www.flickr.com/photos/40741608@N08/5017060556/" title="geany-lua-manpage by 贝小塔, on Flickr"><img src="http://farm5.static.flickr.com/4127/5017060556_ca89e90517.jpg" width="500" height="313" alt="geany-lua-manpage" /></a></p>

<p>The post is brought to you by <a href="http://fedorahosted.org/lekhonee">lekhonee</a> v0.7</p>
