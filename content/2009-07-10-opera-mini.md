---
layout: post
title: Opera-mini
categories:
- 装备
tags:
- internet
- mobile
- opera
published: true
comments: true
---
<p>偌大的互联网，却找不到一个带提交按钮的文本框。</p>

<p>昨天为了能看一眼facebook上的留言（汗，其实我对翻墙的要求也够低的）可谓无所不用其极。首先是用tor+privoxy，结果tor的速度实在是太慢，除了tor的测试页面其它根本就打不开。然后看到了ssh -D的方法，这个速度也是很慢，但是不小心打开了twitter.com，然后又海枯石烂地登录成功，看到没有新的direct msg，也就算达到目的了。</p>

<p>再后来就看到opera-mini的在线<a title="Opera Mini Demo" href="http://demo.opera-mini.net/demo.html" target="_blank">DEMO</a>，一个j2me simulator，借助这个applet可以顺顺利利地打开facebook twiiter之类的网站，但是因为Linux上JRE中文字体的问题，看起来多少还有点吃力。然后我就弱弱地睡下来，片刻之后才反应过来这一切多有意思。看看Opera-mini最大的卖点：<br />
http://en.wikipedia.org/wiki/Opera_Mini#Functionality</p>

<p>在Server和终端之间有一个Proxy，这个Proxy整好运行在墙外面，也就不奇怪twitter facebook这样的网站可以轻松地访问了。不仅仅是在线demo，用手机终端也是一样的道理。我之前还一直以为是中移动的疏忽，看来真是错怪他们的工作了，呵呵。
<div id="_mcePaste" style="overflow: hidden; position: absolute; left: -10000px; top: 44px; width: 1px; height: 1px;">http://en.wikipedia.org/wiki/Opera_Mini#Functionality</div></p>
