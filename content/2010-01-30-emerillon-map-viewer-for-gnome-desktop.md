---
layout: post
title: 'Emerillon: map viewer for gnome desktop'
categories:
- 装备
tags:
- GIS
- gnome
- map
- OpenStreetMap
published: true
comments: true
---
<p>自从libchamplain / geoclue等库发布之后，gnome桌面的地理信息工具和支持发展很迅速：例如之前提到过的eye-of-gnome的地理信息插件，根据EXIF信息在地图上显示。现在gnome桌面上终于有一个专门的地图查看器了，仍然是基于libchamplain，名字叫做<a href="http://www.novopia.com/emerillon/" target="_blank">emerillon</a></p>

<p><a title="Emerillon Map Viewer by 贝小塔, on Flickr" href="http://www.flickr.com/photos/40741608@N08/4315412541/"><img src="http://farm5.static.flickr.com/4021/4315412541_d34e08d02b.jpg" alt="Emerillon Map Viewer" width="500" height="286" /></a></p>

<p>仍然是使用open street maps，这两年上海的地图发展的非常不错，连最新的二号线东延都已经被标注出来了。相比之下，南京的地图就还是一片空白。</p>

<p>在Ubuntu上安装emerillon，可以从其网站上下载源码编译安装：
<a href="http://www.novopia.com/emerillon/download.html">http://www.novopia.com/emerillon/download.html</a></p>

<p>emerillon的几个主要依赖：
<ul>
	<li>libchamplain</li>
	<li>librest</li>
	<li>ethos</li>
</ul>
libchamplain在ubuntu 9.10的仓库已经包含</p>

<p>librest也在软件仓库中，不过需要注意的是ubuntu将librest安装在pkg-config里时的名字叫做rest.pc，而emerillon查找的是rest-0.6.pc，所以需要手动建立一个软连接：
<em>sudo ln -s /usr/lib/pkgconfig/rest.pc /usr/lib/pkgconfig/rest-0.6.pc</em></p>

<p>ethos是一个Gtk的插件框架，目前还不在软件仓库中，需要从网站下载代码编译：
<a href="http://git.dronelabs.com/ethos/" target="_blank">http://git.dronelabs.com/ethos/</a>
ethos网站上提到的PPA源中的版本偏旧，不建议使用。</p>

<p><em>实际上也可以直接添加emerillon的PPA源：<br />
deb http://ppa.launchpad.net/mathieu-tl/emerillon/ubuntu karmic main<br />
deb-src http://ppa.launchpad.net/mathieu-tl/emerillon/ubuntu karmic main</em></p>

<p>另外，也可以通过Ubuntu Tweak安装。</p>

<p>作者Blog：
<a href="http://blog.pierlux.com/en/">http://blog.pierlux.com/en/</a></p>
