---
layout: post
title: Yan Captcha Service
categories:
- 把戏
tags:
- captcha
- foss
- project
- Yan
published: true
comments: true
---
<p>I'd like to announce my recent works, a project called Yan Captcha Service written in Java which is aimed to provide whole solutions of captcha for your websites. It will be very easy to use the service because 1. interfaces are based on plain http url; 2. different kinds of usage are supported to fit your requirements; 3. the architecture is open so you can add your own solid implementation of captcha; 4. less coupling with your system. And designed for scalability, currently, it applies JGroups to share sessions (memcached support will be added soon), thus you can setup a cluster for the service.</p>

<p>As you may know, the open-source project is split from my current work-time project because it is more closed to my idea. However, soon we will have the product opened to our thousands (millions ?) of users. I can gain feedback from the challenge and improve the open-source edition.</p>

<p>The code is maintained by open-source scm, mercurial (also known as hg). The project is now hosted on <a href="http://bitbucket.org/sunng/yan/">bitbucket.org</a>. You can clone the code to local via:
<em>$ hg clone https://sunng@bitbucket.org/sunng/yan/</em></p>

<p>To build the project, run this command in root of project directory:
<em>mvn install</em></p>

<p>To run in a develop environment:
<em>mvn jetty:run</em></p>

<p><a href="http://bitbucket.org/sunng/yan/issues/?status=new&amp;status=open" target="_blank">Issue reporting</a> and patches are always welcomed.</p>

<p>Check the wiki pages for more information about the project:
<a href="http://bitbucket.org/sunng/yan/wiki/Home">http://bitbucket.org/sunng/yan/wiki/Home</a></p>
