---
layout: post
title: Extend slacker server with interceptors
categories:
- 装备
tags:
- clojure
- github
- project
- slacker
published: true
comments: true
---
<p>An interceptor framework was introduced in slacker 0.3.0. It's designed to allow user to add custom functionality without hacking into the internal of slacker.</p>

<p>Like many server frameworks, slacker abstracts the request processing as a pipeline. The request object is modified by adding or updating attributes through each node of the pipeline. So it's easy to add your interceptor into the pipeline, with which you can get the data before and after function executed.</p>

<p>To create such an interceptor, you should use the <em>slacker.interceptor/definterceptor</em> macro and <em>slacker.interceptor/definterceptor+</em> macro:</p>

<p><blockquote>(definterceptor name<br />
  :before interceptor-function<br />
  :after interceptor-function)</blockquote></p>

<p><blockquote>(definterceptor+ name [arguments]<br />
  :before interceptor-function<br />
  :after interceptor-function)</blockquote></p>

<p>definterceptor+ can accept arguments so you can configure the interceptor when you use it.</p>

<p>See a simple example:<br />
[cc lang="clojure"]<br />
(definterceptor log-function-call<br />
  :before (fn [req] (println (str "calling " (:fname req))) req))</p>

<p>(definterceptor+ log-function-call-prefixed [prefix]<br />
  :before (fn [req] (println (str <br />
                               (if (fn? prefix) (prefix) prefix) <br />
                               " calling " <br />
                               (:fname req))) <br />
                    req))<br />
[/cc]</p>

<p>Then, add it to your slacker server by<br />
[cc lang="clojure"]<br />
(use '[slacker.interceptor])<br />
(import '[java.util Date])<br />
(start-slacker (the-ns 'slapi) 2104<br />
  :interceptors (interceptors log-function-call<br />
                              (log-function-call-prefixed <br />
                                (fn [] (.toString (Date.)))))<br />
[/cc]</p>

<p>Now you can log every function call of your slacker server.</p>

<p>For more detail about the interceptor framework, especially the request data, please check the <a href="https://github.com/sunng87/slacker/wiki/Interceptors" target="_blank">wiki page</a>.</p>

<p>In slacker 0.3.0, there is a built-in interceptor to stats function calls. You can find it at <em>slacker.interceptors.stats</em>. The stats data is expose via JMX. You can also write monitoring application to retrieve the data. 
<a href="http://imgur.com/vtOoL"><img src="http://i.imgur.com/vtOoL.png" alt="" title="Hosted by imgur.com" /></a></p>

<p>And there will be more built-in interceptors in 0.4.0, includes function call time stats and logging.</p>

<p></p>
