---
layout: post
title: ClojureDocs Android App
categories:
- ANN
tags:
- android
- clojure
- github
- project
published: true
comments: true
---
<p>利用春节的假期写了一个Android应用，可以在<a href="http://clojuredocs.org" target="_blank">ClojureDocs.org</a>上搜索clojure API，浏览文档、源代码和社区贡献的代码实例。ClojureDocs在我学习Clojure的过程中起了很大的作用，所以我想这个网站应该对很多人有用。</p>

<p>无暇去学习Android平台上繁琐的知识，不过好在有Phonegap这样的框架，可以把网页应用转化为本地应用，并且提供访问本地设备的API。通过Phonegap开发的程序还可以直接移植到iphone平台上。ClojureDocs Android就是运行在Phonegap中。</p>

<p>首页：
<img src="http://i.imgur.com/zdMCt.png" alt="" /></p>

<p>搜索界面
<img src="http://i.imgur.com/MVCz4.png" alt="" /></p>

<p>API函数界面
<img src="http://i.imgur.com/M0fEI.png" alt="" /></p>

<p>你可以从github获得代码和签名过的apk：<a href="https://github.com/sunng87/clojuredocs-android" target="_blank">https://github.com/sunng87/clojuredocs-android</a></p>

<p>Known Issue，phonegap程序在屏幕旋转时会崩溃，已经在2.3和3.2上重现，目前还不清楚具体的原因。(Edit 20120127: Fixed in 1.0.4)</p>

<p>欢迎任何的pull request。
</p>
