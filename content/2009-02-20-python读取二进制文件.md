---
layout: post
title: Python读取二进制文件
categories:
- 手艺
tags:
- c
- python
published: true
comments: true
---
<p>Python里虽然可以通过'b'的标记来区别普通文件和二进制文件，但是依然是把二进制文件当作普通文本处理。要读写二进制文件，需要用struct库来作pack和unpack。</p>

<p>比如我最近拿到一个如下数据结构的二进制文件</p>

<p>[codesyntax lang="c" lines="fancy"]<br />
struct DEMTYPE {<br />
	int row;<br />
	int col;<br />
	float xmin;<br />
	float ymin;<br />
	float xmax;<br />
	float ymax;<br />
	float size;<br />
	float vmin;<br />
	float vmax;<br />
	float scale;<br />
	float *data;<br />
};<br />
[/codesyntax]</p>

<p>其中data是一个长度为row*col的数组。要读取这样一个二进制文件，可以用这样的代码<br />
[codesyntax lang="python" lines="fancy"]<br />
row, col, xmin, ymin, xmax, ymax, size, vmin, vmax, scale = \<br />
        struct.unpack('&lt;2i8f', datastring[:(4*2+4*8)])<br />
vdata = struct.unpack('&lt;'+str(row*col)+'f', datastring[(4*2+4*8):])<br />
[/codesyntax]<br />
核心是unpack的第一个参数，用来标识二进制数据的格式，其中&lt;表示little-endian，i表示整型数，f表示单精度浮点数，数字是量词。</p>

<p>更详细的说明可以看manual：
<a href="http://docs.python.org/library/struct.html" target="_blank">http://docs.python.org/library/struct.html</a></p>

<p>好吧，我承认我写这个是来测试highlight插件的。</p>
