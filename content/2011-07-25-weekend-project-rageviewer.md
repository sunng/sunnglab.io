---
layout: post
title: 'Weekend Project: RageViewer'
categories:
- ANN
tags:
- clojure
- project
published: true
comments: true
---
<p><blockquote>"Weekend is for weekend projects."</blockquote></p>

<p>介绍Weekend Project前，先说个事。早上HN上<a href="http://www.romku.com/blog/2011/07/my-weekend-project-what-can-you-really-do-in-a-weekend" target="_blank">有人说</a>：你们这些个人，自个偷偷摸摸搞了半年一年的东西，然后发到HN上说这是两天就搞定的weekend project，你们恶心不恶心。本人郑重声明，下面这个东西确实是标准的weekend project，只用了周六下午和晚上的时间，结果周日还因为元气大伤只改了一点小东西。因为确实只有不到两天的effort，所以就不发到HN上丢人现眼了。</p>

<p>话说，比起HN，我更常看reddit。于是就有了这个weekend project，<strong><a href="http://sunng.info:8080/rageviewer/index.html" target="_blank">Rage Viewer</a></strong>，可以浏览reddit ffffffffuuuuuuu上最新的rage comic。</p>

<p>功能很简单，唯一值得一提的是它是统统用clojure写成的，后端是clojure，前端是最近公布的clojurescript编译的js。之所以这么搞，不为别的，只为cool。这个和每天早晨五点起床化妆是一个道理。</p>

<p>在github上可以找到这个项目，他可以随意跑在任何地方，只要三个步骤：
<ul>
	<li>git clone git://github.com/sunng87/rageviewer.git</li>
	<li>lein deps</li>
	<li>lein ring server</li>
</ul></p>

<p>欢迎feedback。</p>
