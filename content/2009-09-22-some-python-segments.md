---
layout: post
title: Some python segments
categories:
- 把戏
tags:
- python
published: true
comments: true
---
<p><pre class="brush:python">
import MySQLdb
import subprocess
from smtplib import *
from datetime import date, timedelta</pre></p>

<p>class StatEntry(object):<br />
    def __init__(self, name, count):<br />
        self.name = name<br />
        self.count = count<br />
    def __str__(self):<br />
        return "%s\t%d" % (self.name, self.count)</p>

<p>def fetchdb(date):<br />
    conn = MySQLdb.connect(host="localhost", user="root", passwd="acd", db="cls")<br />
    cursor = conn.cursor()<br />
    cursor.execute("SELECT link, count(*) FROM sdocom WHERE DATE(logtime) = DATE('%s') GROUP BY link" % date)<br />
    rows = cursor.fetchall()<br />
    result = map(lambda x: StatEntry(*x), rows)<br />
    return result</p>

<p>def archive(datelist):<br />
    for date in datelist:<br />
        subprocess.call(["gzip", "cls-"+date+".log"])</p>

<p>def sendmail(results):<br />
    mailcontent = reduce(lambda x,y: x+str(y)+"\n", results, "")<br />
    smtp = SMTP("your.smtp.server")<br />
    smtp.login("user","passwd")<br />
    smtp.sendmail("cls-no-reply@smtp.server", ["your@mail.server"], mailcontent)<br />
    smtp.quit()</p>

<p>def dumpall(data):<br />
    content = reduce(lambda x,y: x+str(y)+"\n", data, "")<br />
    print content</p>

<p>def main():<br />
    yesterday = str(date.today()-timedelta(1))<br />
    results = fetchdb(yesterday)<br />
    #dumpall(results)<br />
    sendmail(results)<br />
    archive([yesterday])</p>

<p>if __name__ == '__main__':<br />
    main()
</p>

<p>As log file was imported to database automatically by other process, the script retrieves statistical data from mysql db and sends the result to your mail box via smtp. Also, log file would be compressed by gzip to minimize storage overhead. Add it to crontab, you will get daily statistical report.</p>

<p>This example shows you basic usage of mysql-python lib, smtplib and how to calculate yesterday by datetime.</p>
