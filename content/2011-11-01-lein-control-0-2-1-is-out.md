---
layout: post
title: lein-control 0.2.1 is out
categories:
- ANN
tags:
- clojure
- project
published: true
comments: true
---
<p>I have updated the leiningen plugin for <a href="https://github.com/killme2008/clojure-control" target="_blank">clojure-control</a> 0.2.1. There is no new feature for it as a plugin. But some new features from upstream are supported and should be highlighted:</p>

<p><h3>SSH command DSL</h3>
This feature is committed by me and first announced in clojure-control 0.2.1. With this DSL, you can write your SSH command with some predefined macros which is more readable and enjoyable.</p>

<p>For example, an ssh task can be defined as:<br />
[cc lang="clojure"]<br />
   (deftask :restart-server "restart your jetty server"<br />
          []<br />
          (ssh (cd "/opt/jetty/bin"<br />
                 (run "./jetty.sh restart"))))<br />
[/cc]</p>

<p>which equals to :<br />
[cc lang="clojure"]<br />
   (deftask :restart-server "restart your jetty server"<br />
          []<br />
          (ssh "cd /opt/jetty/bin; ./jetty/sh restart"))<br />
[/cc]</p>

<p>For full document of this DSL, please check the <a href="https://github.com/killme2008/clojure-control/blob/master/README.md" target="_blank">README</a>.</p>

<p><h3>Improved syntax for defcluster</h3>
ssh/scp/rsync options in defcluster could be written as a vector:<br />
[cc lang="clojure"]<br />
(defcluster :mycluster<br />
  :ssh-options "-p 44"<br />
  :scp-options "-v"<br />
  :rsync-options ["-arz" "--delete"]<br />
  :clients [<br />
    { :host "c.domain.com" :user "clogin" :ssh-options "-v -p 43"}<br />
  ]<br />
  :user "login"<br />
  :addresses ["a.domain.com" "b.domain.com"])[/cc]<br />
(code quote from official announcement)</p>

<p><h3>Parallel support</h3>
Tasks could be executed in parallel if you add option <code>:parallel true</code> on defcluster.</p>

<p>Finally, to use this plugin, add dev-dependency to your project.clj:<br />
[cc lang="clojure"]<br />
        :dev-dependencies [[lein-control "0.2.1"]]<br />
[/cc]<br />
More detailed instructions could be found <a href="https://github.com/sunng87/lein-control/blob/master/README" target="_blank">here</a>.</p>

<p>BTW, this is the last release of lein-control as a standalone artifact. I will be working on merging lein-control into clojure-control. Soon you will have a few flexible ways to use clojure-control.
</p>
