---
layout: post
title: Virtual Machine Searcher for Gnome Deskbar Applet
categories:
- ANN
tags:
- gnome
- linux
- python
- ubuntu
published: true
comments: true
---
<p>Very glad to announce another daily-coding work: an extension for gnome deskbar applet to search and launch virtual machine. There is a plugin for gnome-do that does the same job. That's what I create the the plugin for. I switched to deskbar because gnome-do's Do.exe reminds me nightmares when I was a M$ Windows user. The deskbar applet has been a great replacement, however, the virtualbox plugin in Do is really impressive while deskbar doesn't provide me the same functionality.</p>

<p>You can ignore words above and just take a look at the screenshot:
<a title="deskbar-virtualbox by 贝小塔, on Flickr" href="http://www.flickr.com/photos/40741608@N08/4350951202/"><img src="http://farm5.static.flickr.com/4062/4350951202_9ede9c4318_o.png" alt="deskbar-virtualbox" width="560" height="400" /></a>
<h3>Download</h3>
Download the extension from (the highlighted one):
<a href="http://bitbucket.org/sunng/daily-coding/downloads/?highlight=8792">http://bitbucket.org/sunng/daily-coding/downloads/?highlight=5137</a></p>

<p>You can also trace the development at bitbucket project. However, the repository is mixed with other small code snippets. Currently, mercurial doesn't support subdirectory pull. So there is no way to grab the deskbar-applet individually.
<h3>Installation</h3>
copy this file to <em>/usr/lib/deskbar-applet/deskbar-applet/modules-2.20-compatible/</em> (Ubuntu installation for example) with super user privilege. Right click desktbar applet, select Preference, Searchers tab, hit "Reload" button, then check the Virtualbox Deskbar Module.
<a title="deskbar-preference by 贝小塔, on Flickr" href="http://www.flickr.com/photos/40741608@N08/4350219097/"><img src="http://farm5.static.flickr.com/4006/4350219097_126cdb3c8d.jpg" alt="deskbar-preference" width="500" height="346" /></a>
<h3>Issue</h3>
Feel free the report issues on bitbucket:
<a href="http://bitbucket.org/sunng/daily-coding/issues/">http://bitbucket.org/sunng/daily-coding/issues/</a></p>
