---
layout: post
title: 安装Exaile豆瓣电台插件及Gnome-Shell扩展
categories:
- ANN
tags:
- exaile
- gnome-shell
published: true
comments: true
---
<p>介绍一下新版本的Exaile豆瓣电台插件和新的GNOME-Shell扩展的安装。</p>

<p><h3>下载</h3>
从github上下载相应的版本：
<pre>$ wget --no-check-certificate https://github.com/sunng87/exaile-doubanfm-plugin/zipball/0.0.10-dbus</pre>
<pre>$ wget --no-check-certificate https://github.com/sunng87/exaile-doubanfm-gnome-shell-extension/zipball/0.0.1</pre>
<pre>$ ls
sunng87-exaile-doubanfm-gnome-shell-extension-0.0.1-0-g5cc29c6.zip
sunng87-exaile-doubanfm-plugin-0.0.10-dbus-0-g90f7175.zip
</pre></p>

<p><h3>解压安装</h3>
如果你已经安装过exaile豆瓣电台插件，可以先删除旧版本：
<pre>$ rm -rf ~/.local/share/exaile/plugins/doubanfm</pre></p>

<p>解压缩下载的文件：
<pre>$ unzip sunng87-exaile-doubanfm-plugin-0.0.10-dbus-0-g90f7175.zip
$ unzip sunng87-exaile-doubanfm-gnome-shell-extension-0.0.1-0-g5cc29c6.zip</pre></p>

<p>移动到指定的目录：
<pre>$ mv sunng87-exaile-doubanfm-plugin-90f7175/ ~/.local/share/exaile/plugins/doubanfm
$ mv sunng87-exaile-doubanfm-gnome-shell-extension-5cc29c6/ ~/.local/share/gnome-shell/extensions/exaile-doubanfm-gnome-shell-extension</pre></p>

<p><h3>配置</h3>
启动Exaile，通过菜单“编辑-&gt;首选项-&gt;插件”，激活doubanfm，<b>禁用lastfm covers</b>。如图设置插件：
<img src="http://i.imgur.com/TFKU3.png" title="Hosted by imgur.com" /></p>

<p>重启GNOME-Shell，按下alt+f2，输入r，回车。</p>

<p><h3>使用</h3>
启动exaile，可以通过文件菜单打开豆瓣电台频道。可以通过ctrl+d或者视图菜单切换到豆瓣电台视图：
<img src="http://i.imgur.com/1vPpz.png" title="Hosted by imgur.com" /></p>

<p>当然也可以通过gnome-shell扩展的菜单控制豆瓣电台的播放。<br /><br /><div class="zemanta-pixie"><img class="zemanta-pixie-img" alt="" src="http://img.zemanta.com/pixy.gif?x-id=ef5913a1-4c72-827d-837e-15abc25dcc92" /></div></p>
