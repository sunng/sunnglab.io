---
layout: post
title: Bash Color Support for Bti
categories:
- 装备
tags:
- c
- foss
- linux
- opensource
published: true
comments: true
---
<p><a href="http://github.com/gregkh/bti">Bti</a> is well known as a command line twitter/identica/statusnet client, by <a href="http://github.com/gregkh">Greg Kroah-Hartman</a>.</p>

<p>I have been using this tool in CLI environment for a long time, found it difficult for human to parse output by eyes. So I forked it and added this bash color support, to provide some highlight on tweets.
<a href="http://www.flickr.com/photos/40741608@N08/5130529830/" title="bti-bash-color by 贝小塔, on Flickr"><img src="http://farm2.static.flickr.com/1346/5130529830_a1f8eaf793.jpg" width="416" height="500" alt="bti-bash-color" /></a></p>

<p>To enable color support, add an option <i>--auto-color</i> to bti command, or append <i>auto-color=yes</i> to your configuration file($HOME/.bti)</p>

<p>I have sent pull request to Greg, hope the patch can be merged into master. Before approved, you can check out <a href="http://github.com/sunng87/bti">my fork</a> and compile it yourself. (If liboauth not found, find it <a href="http://liboauth.sourceforge.net/">here</a>.)</p>
