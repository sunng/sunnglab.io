---
layout: post
title: Tiny LifeStream widget available on sunng.info
categories:
- 手艺
tags:
- daf
- javascript
- web
published: true
comments: true
---
<p>首先推荐samson的<a href="http://stdout.samsonw.info/">stdout</a>，这是一个LifeStream Web应用（针对Loser的LifeStream称为LoserStream）。你可以从<a href="https://github.com/samsonw/stdout">github</a>上获得代码，参考samson的<a href="http://blog.samsonis.me/2011/03/stdout-deployment/">deployment</a>和<a href="http://blog.samsonis.me/2011/03/stdout-customization/">customization</a>文档，在一二三四五六七八九十十一十二……分钟内搭建一个自己stdout，至于是不是LoserStream你自己看着办。</p>

<p>本来打算在自己的主机上搭一个实例的，怎奈怎奈我那CentOS5的主机。所以我说我们这些CentOS用户最伤不起了！！！源里有Ruby有个毛用！！1.8.4，泥玛gem都装不上！！还要去找1.8.6的代码来编译，有没有！！！好不容易装上gem了，gem 1.6.1说找不到Win32API！！！干掉重装1.4.5！再装ruby-sqlite3，源里的sqlite3版本又不行，装不上，有没有！！！有没有！！！</p>

<p>最后我无奈了，还是用老朋友Yahoo Pipes吧。拖一拖拉一拉神马都有了，页面上整点咱最擅长的document.getElementById就成了。你可以在<a href="http://sunng.info">http://sunng.info</a>找到这个东西，现在这个源里收录了：
<ul>
<li>blog</li>
<li>豆瓣</li>
<li>github</li>
<li>osm editing</li>
<li>twitter</li>
<li>reddit like</li>
</ul></p>

<p>这些也是我主要混迹的地方了，遗憾就是bitbucket居然没有个人的RSS输出。倒是可以通过他们的API获得，不过在Pipes里要多拖点东西，所以我先琢磨一下再说吧。网易八方我说过了，不愧是互联网产品经理驱动的东西，连个RSS输出都木有。饭否嘛现在作为扯淡专用场所就不收录了。</p>

<p>说实在的把这些东西都聚合在一起还是挺吓人的，所以我把他稍微藏了一下。</p>

<p>还有，今天日本地震，祝daf同学平平安安平平安安。</p>
