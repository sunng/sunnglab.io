---
layout: post
title: Exaile-Douban-Covers 0.0.3 Released
categories:
- ANN
tags:
- exaile
- linux
- opensource
published: true
comments: true
---
<p>一眨眼有大半年没有维护这个插件了，Exaile从0.3.0升级到了0.3.2。严重的是这三个版本的插件接口各不相同，0.3.1新增了cover providers管理， 修改了track获得相应字段的api，简化了CoverSearchMethod的职责（去掉了缓存管理，只负责find_convers和get_conver_data两个功能）。而0.3.2至少也修改了配置管理界面的API，所以可以在0.3.1上工作的版本也不能在0.3.2上工作。</p>

<p>这次升级维护把插件升级到0.0.3，兼容目前Ubuntu仓库里的Exaile 0.3.1.1。0.3.1.1新提供了一个cover provider的配置界面，可以调整cover provider的优先级，如果你听中文歌比较多，建议可以把douban cover放在第一位。</p>

<p>下载页面（doubancovers-0.0.3.exz ）：
<a href="http://bitbucket.org/sunng/exailedoubancovers/downloads">http://bitbucket.org/sunng/exailedoubancovers/downloads</a></p>

<p>新版本和0.3.0已经无法兼容，0.3.0的用户可以继续使用0.0.2 。而兼容0.3.2的版本预计要等到10月份之后推出了，用Archlinux和其他自己安装的用户要稍等一阵了。</p>

<p>The post is brought to you by <a href="http://fedorahosted.org/lekhonee">lekhonee</a> v0.7</p>
