---
layout: post
title: I cloned Beanstalkd with Clojure
categories:
- ANN
tags:
- beanstalkd
- clojure
- opensource
- project
published: true
comments: true
---
<p>I just cloned <a href="http://kr.github.com/beanstalkd/">Beanstalkd</a> with clojure and you can find this project on <a href="https://github.com/sunng87/clojalk">github</a>. It's a light weight task queue that "producers" could put tasks in and "workers" are blocked to reserve them and process them. It will help you to split expensive operations (sending email, etc) to background.</p>

<p>The features:
<ul>
	<li>Almost compatible with Beanstalkd protocol</li>
	<li>Full support on Beanstalkd verbs: put/reserve/release/bury/kick</li>
	<li>Persistence with binary write-ahead logs</li>
	<li>Simple JMX monitoring</li></ul></p>

<p></p>

<p>The networking layer is based on @<a href="https://github.com/ztellman">ztellman</a>'s libraries, aleph, lamina and gloss. And I also received great help from him on implementing the protocol with gloss. </p>

<p>This is not my first clojure project but the largest one. I add <a href="https://github.com/fogus/marginalia">marginalia</a> in the dev-dependencies, you can generate well formatted code and docs with `lein marg`.</p>

<p>By the way, this project is not well tested. I'm still working on it to find potential issues and to fix them. Do not use it on any production environment before you have carefully gone through the code base. I just made this announcement here to let people know this project if they are interested in this topic. </p>

<p>And always, Any fork/contribution is welcomed.
</p>
