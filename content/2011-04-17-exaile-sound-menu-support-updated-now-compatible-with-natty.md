---
layout: post
title: Exaile sound menu support updated, now compatible with Natty
categories:
- ANN
tags:
- exaile
- project
- ubuntu
published: true
comments: true
---
<p>Finally it was fixed. I just updated the plugin according to the new <a href="https://wiki.ubuntu.com/SoundMenu#For%20Natty">SoundMenu registration process</a>. Now you can download the head version from github and extract it under <font face="monospace">~/.local/share/exaile/plugins</font>.</p>

<p><img src="http://i.imgur.com/z2YSG.png" title="Hosted by imgur.com" /></p>

<p>You can fire an issue on github if you have problem of using it.<br /><br /><div class="zemanta-pixie"><img class="zemanta-pixie-img" alt="" src="http://img.zemanta.com/pixy.gif?x-id=ebfcb568-71a3-85d5-a381-5a9865617aae" /></div></p>
