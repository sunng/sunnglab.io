---
layout: post
title: YAMB is now hosted on kenai.com
categories:
- 把戏
tags:
- java
- kenai
- web
published: true
comments: true
---
<p>YAMB(Yet Another Microblogging Tool) which is mentioned several days ago is now hosted on Sun's project-kenai licensed with GPL2.
<a href="http://kenai.com/projects/yamb">http://kenai.com/projects/yamb</a></p>

<p>You can grab the source code from following link via mercurial(hg).
<a href="https://kenai.com/hg/yamb~hg-repos ">https://kenai.com/hg/yamb~hg-repos</a></p>

<p>Actually, there is nothing new.</p>
