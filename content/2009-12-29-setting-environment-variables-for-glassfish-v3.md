---
layout: post
title: Setting Environment Variables for Glassfish v3
categories:
- 把戏
tags:
- Glassfish
- java
published: true
comments: true
---
<p>After you test and build your application in NetBeans, you deploy it to Glassfish, but the environment variables are invalid again. In such situation, you should set these variable in glassfish configuration file which is supposed to located at:</p>

<p><em>$GLASSFISH_HOME/glassfish/config/asenv.conf</em></p>

<p>Append your variables to the file, and restart the server. It works.</p>
