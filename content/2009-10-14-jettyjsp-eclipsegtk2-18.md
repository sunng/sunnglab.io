---
layout: post
title: Jetty7/JSP, Eclipse/GTK2.18
categories:
- 装备
tags:
- eclipse
- foss
- gnome
- jetty
published: true
comments: true
---
<p><p>Jetty7.0上周发布了，这是jetty迁进eclipse社区之后的第一个正式版本。但是下载之后发现这个版本居然没有jsp支持。原来，jetty的jsp实现一直使用的是glassfish的实现，嵌入eclipse社区之后，这变成了一个问题。功能完全的版本（hightide）依然可以从codehaus的镜像里下载：<br />
<a href="http://dev.eclipse.org/mhonarc/lists/jetty-dev/msg00198.html">http://dist.codehaus.org/jetty/</a></p>
<p>比较一下大小就能看出，eclipse的版本只有2.1M，hightide的版本15M。为此，社区里有详细的讨论：<br />
<a href="http://dev.eclipse.org/mhonarc/lists/jetty-dev/msg00198.html">http://dev.eclipse.org/mhonarc/lists/jetty-dev/msg00198.html</a></p>
<p>Gnome2.28发布了，但是Eclipse在Gtk2.18上工作时，会发生按钮失灵的问题，主要集中在finish next和ok等关键的按钮。简单的解决方法是焦点在按钮上时通过按回车来执行。比较彻底的方法是在eclipse执行的环境中，设置环境变量<em>GDK_NATIVE_WINDOWS=true</em></p>
<p>这个bug被报告在这里：<br />
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=291257">https://bugs.eclipse.org/bugs/show_bug.cgi?id=291257</a></p></p>
